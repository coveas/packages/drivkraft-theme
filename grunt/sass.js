module.exports = {
  login: {
    options: {
      implementation: 'node-sass',
      style: 'nested'
    },
    files: [
      {
        src: '<%= scssDir %><%= scssLoginFileName %>.scss',
        dest: '<%= cssDir %><%= cssLoginFileName %>.css'
      }
    ]
  },
  sensei: {
    options: {
      implementation: 'node-sass',
      style: 'nested'
    },
    files: [
      {
        src: '<%= scssDir %>sensei.scss',
        dest: '<%= cssDir %>sensei.css'
      }
    ]
  },
  dev: {
    options: {
      implementation: 'node-sass',
      style: 'nested'
    },
    files: [
      {
        src: '<%= scssDir %><%= scssMainFileName %>.scss',
        dest: '<%= cssDir %><%= cssMainFileName %>.css'
      }
    ]
  },
  email: {
    options: {
      implementation: 'node-sass',
      style: 'nested'
    },
    files: [
      {
        src: '<%= scssDir %><%= scssEmailFileName %>.scss',
        dest: '<%= cssDir %><%= cssEmailFileName %>.css'
      }
    ]
  },
  min: {
    options: {
      implementation: 'node-sass',
      style: 'compressed'
    },
    files: [
      {
        expand: true,
        cwd: '<%= scssDir %>',
        src: ['**/*.scss', '!<%= scssMainFileName %>.scss'],
        dest: '<%= cssDir %>',
        ext: '.min.css'
      },
      {
        src: '<%= scssDir %><%= scssMainFileName %>.scss',
        dest: '<%= cssDir %><%= cssMainFileName %>.min.css'
      }
    ]
  }
};