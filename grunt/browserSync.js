/* Live reloading with Browsersync */
module.exports  = {
  files: {
    src : [
      '/**/*.php',
      'assets/{,*/}*.css',
      'assets/{,*/}*.js',
      'assets/images/{,*/}*.{gif,jpeg,jpg,png,svg,webp}'
    ],
  },

  options: {
    watchTask: true,
    proxy: '<%= proxy %>',
  }
};
