/* Image minification */
module.exports = {
  dynamic: {
    files: [{
      expand: true,
      cwd: '<%= imgDir %>',
      src: ['**/*.{png,jpg,gif,svg}'],
      dest: '<%= imgDir %>'
    }]
  }
};