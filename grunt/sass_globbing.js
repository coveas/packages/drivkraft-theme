module.exports = function (grunt) {
  return{
    target: {
      files: {
        '<%= scssPartials %>_components.scss': '<%= scssDir %>components/*.scss',
        '<%= scssPartials %>_mixins.scss': '<%= scssPartials %>mixins/*.scss',
      }
    }
  }
};