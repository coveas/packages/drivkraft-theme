/**
 * Prompts and questions - https://github.com/dylang/grunt-prompt
 * We need to ask a few things from you first,
 * then the project can get underway!
 */
module.exports = {
  browserSync: {
    options: {
      questions: [
        {
          config:  'browserSync.options.proxy', // arbitrary name or config for any other grunt task
          type:    'input', // list, checkbox, confirm, input, password
          message: 'Enter the Vagrant URL', // Question to ask the user, function needs to return a string,
          default: 'drivkraft.dev:8080', // default value if nothing is entered
        }
      ]
    }
  },
};