/* CSSO! Optimisation of styles */
module.exports = {
  login: {
    restructure: {
      options: {
        restructure: true,
      },
      files: {
        '<%= cssDir %><%= cssLoginFileName %>.min.css': ['<%= cssDir %><%= cssLoginFileName %>.min.css']
      }
    },
    compress: {
      files: {
        '<%= cssDir %><%= cssLoginFileName %>.min.css': ['<%= cssDir %><%= cssLoginFileName %>.min.css']
      }
    },
  },
  sensei: {
    restructure: {
      options: {
        restructure: true,
      },
      files: {
        '<%= cssDir %>sensei.css': ['<%= cssDir %>sensei.css']
      }
    },
    compress: {
      options: {
        report: 'gzip'
      },
      files: {
        '<%= cssDir %sensei.min.css': ['<%= cssDir %>*sensei.css']
      }
    },
  },
  email: {
    restructure: {
      options: {
        restructure: true,
      },
      files: {
        '<%= cssDir %><%= cssEmailFileName %>.min.css': ['<%= cssDir %><%= cssEmailFileName %>.min.css']
      }
    },
    compress: {
      files: {
        '<%= cssDir %><%= cssEmailFileName %>.min.css': ['<%= cssDir %><%= cssEmailFileName %>.min.css']
      }
    },
  },
  live: {
    restructure: {
      options: {
        restructure: true,
        report: 'min'
      },
      files: {
        '<%= cssDir %><%= cssMainFileName %>.min.css': ['<%= cssDir %><%= cssMainFileName %>.min.css']
      }
    },
    compress: {
      options: {
        report: 'gzip'
      },
      files: {
        '<%= cssDir %><%= cssMainFileName %>.min.css': ['<%= cssDir %><%= cssMainFileName %>.min.css']
      }
    },
  },
};