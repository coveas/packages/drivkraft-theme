module.exports  = {
  //Viewport options
  options: {
    width: 1600,
    type: 'screen'
  },
  all: {
    files: {
      '<%= cssDir %>ie.css': ['<%= cssDir %><%= cssMainFileName %>.min.css']
    }
  }
};