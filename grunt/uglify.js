module.exports = {
  options: {
    mangle: false
  },
  js: {
    expand: true,
    cwd: '<%= jsDir %>src',
    src: '**/*.js',
    dest: '<%= jsDir %>dist'
  }
};