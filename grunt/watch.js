/* Watching files for changes that may or may not occur... */
module.exports = {
  js: {
    files: [
      '<%= jsDir %>*.js'
    ],
    tasks: [
      'uglify',
      'concat:dist'
    ]
  },
  css: {
    // Watch sass changes, merge mqs & run bs
    files: [
      '<%= scssDir %>*.scss',
      '<%= scssDir %>**/*.scss'
    ],
    tasks: [
    'sass_globbing:target',
    'sass:dev',
    'sass:min',
    'postcss:dist'
    ]
  },
  options: {
    spawn: false // Very important, don't miss this
  }
};