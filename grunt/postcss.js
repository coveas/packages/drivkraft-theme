/* Postcss. Autoprefixing everything */
module.exports = {
  options: {
    map: false,
    processors: [
      require('autoprefixer')({
        browsers: ['> 20%', 'last 10 versions', 'Firefox > 20']
      })
    ],
    remove: false
  },
  login: {
    src: '<%= cssDir %>*<%= cssLoginFileName %>.css'
  },
  sensei: {
    src: '<%= cssDir %>*sensei.css'
  },
  email: {
    src: '<%= cssDir %>*<%= cssEmailFileName %>.css'
  },
  dist: {
    src: '<%= cssDir %>*.css'
  }
};