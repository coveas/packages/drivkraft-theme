<?php

class drivkraft_footer {
  static function setup() {
    // add_action( 'drivkraft_before_footer', __CLASS__ . '::footer_widgets' );
    add_action( 'drivkraft_after_footer_widgets', __CLASS__ . '::affiliated_logos', 10 );

    // The footer after the footer
    add_action( 'drivkraft_after_footer', __CLASS__ . '::post_credits', 10 );

    // Logos with external links
    add_action( 'drivkraft_before_post_credits', __CLASS__ . '::logos_with_external_links', 10 );
  }

  /**
   * A list of logos Logos
   * @return html list of logos
   */
  static function affiliated_logos() {
    if ( ! have_rows( 'affiliated_logos', 'option' ) ) {
      return;
    }

    // Footer icon list as transient
    $footer_icon_list = get_transient( 'footer_affiliated_logos' );
    // If there isn't a transient, capture the html
    if ( ! $footer_icon_list ) {
      ob_start();
      if ( have_rows( 'affiliated_logos', 'option' ) ) :
        echo '<ul class="affiliated--logos cf">';
        while ( have_rows( 'affiliated_logos', 'option' ) ) : the_row();
            $logo = get_sub_field( 'logo' );
            echo '<li>' . wp_get_attachment_image( $logo, 'thumbnail' ) . '</li>';
        endwhile;
        echo '</ul>';
      endif;
      $footer_icon_list = ob_get_clean();

      // Set the transient for next load
      set_transient( 'footer_affiliated_logos' , $footer_icon_list, 12 * YEAR_IN_SECONDS );
    }
    // Always output an image
    echo $footer_icon_list;
  }

  /**
   * The footer that comes after the footer
   * @return html for footer/footer
   */
  static function post_credits() {

    global $wpdb;

    // Footer icon list as transient
    $post_credit_content = get_transient( 'post_footer_content' );
    // If there isn't a transient, capture the html
    if ( ! $post_credit_content ) {
      ob_start();
      ?>
      <div class="post--credits">
        <div class="post--credits--wrapper w cf">

        <?php /* For logos, etc. */
        do_action( 'drivkraft_before_post_credits' ); ?>

          <div class="post--copyright">
            <?php echo sprintf( __( '&copy; %1$s %2$s', 'drivkraft-theme' ), get_bloginfo( 'name' ), get_the_time( 'Y' ) ); ?>
          </div>

          <div class="post--social">
            <?php // Fetch social networks

              $social_urls = [
                'Facebook'  =>'options_facebook_url',
                'Twitter'   =>'options_twitter_url',
                'Instagram' =>'options_instagram_url',
                'Youtube'   =>'options_youtube_url'
              ];

              $wherein = "'".implode( "', '", $social_urls )."'";
              $options = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}options WHERE option_name IN ({$wherein})" );

              foreach ( $options as $option ) {
                if ( ! $option->option_value ) {
                  continue;
                }
                echo '<a href="' . $option->option_value. '">' . array_search( $option->option_name, $social_urls ) . '</a>';
              }
            ?>
          </div>
        </div>
      <?php
      $post_credit_content = ob_get_clean();

      // Set the transient for next load
      set_transient( 'post_footer_content' , $post_credit_content, 12 * HOUR_IN_SECONDS );
    }
    // Always output the footer content
    echo $post_credit_content;
  }


  /**
   * Logos with links that lead to external sites.
   * These can be found in Theme settings > footer
   */
  static function logos_with_external_links() {
    // Footer icon list as transient
    $footer_logos_list = get_transient( 'logos_with_links' );

    // If there isn't a transient, capture the html
    if ( ! $footer_logos_list ) {
      ob_start();

      // check if the repeater field has rows of data
      if ( have_rows( 'logos_with_links', 'option' ) ) :
        echo '<div class="outward--logos cf">';
        while ( have_rows( 'logos_with_links', 'option' ) ) : the_row();
            $logo = get_sub_field( 'logo' );
            $link = get_sub_field( 'link' );
            echo '<a href="' . $link . '">';
            echo wp_get_attachment_image( $logo, 'thumbnail' );
            echo '</a>';
        endwhile;
        echo '</div>';
      endif;

      $footer_logos_list = ob_get_clean();
      // Set the transient for next load
      set_transient( 'logos_with_links' , $footer_logos_list, 12 * HOUR_IN_SECONDS );
    }

    // Always output an image
    echo $footer_logos_list;
  }
}

// Set up the footer
drivkraft_footer::setup();
