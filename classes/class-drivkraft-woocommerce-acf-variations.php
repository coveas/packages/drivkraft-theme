<?php

class woocommerce_acf_variations {

  /**/
  static function setup() {
    /* Add this form to variations */
    // add_action( 'admin_head','acf_form_head', 20 );
    // add_action( 'woocommerce_product_after_variable_attributes', 'woocommerce_acf_variations::variation_field_group', 99, 3 );
    // add_action( 'woocommerce_product_thumbnails', 'woocommerce_acf_variations::display_thumbs', 99, 3 );
  }

  /* @todo – Javascript to pull in the variation images when variation is selected */
  static function display_thumbs() {
    if ( ! is_product() ) {
      return;
    }

    global $product;

    // Only variations
    if ( ! $product->is_type( 'variable' ) ) {
      return;
    }

    // If not a product, stop
    if ( ! $product ) {
      return;
    }

    // get variations
    $variations = $product->get_available_variations();

    if ( empty( $variations ) ) {
      return;
    }

    foreach ( $variations as $variation ) {
      $id = $variation['variation_id'];
      $gallery = get_field( 'variation_gallery', $id );
      if ( ! $gallery ) {
        continue;
      }
      // Print out the variation gallery separated
      echo '<div id="gallery-variation-' . $id . '" class="gallery-variation">';
      foreach ( $gallery as $image ) {
        // Need to link to the correct image and show in viewer
        echo '<a data-rel="prettyPhoto" href="' . wp_get_attachment_image_src( $image['id'], 'large' )[0] . '">';
        echo wp_get_attachment_image( $image['id'], 'thumbnail' );
        echo '</a>';

      }
      echo '</div>';
    }
  }

  static function variation_field_group( $loop, $variation_data, $variation ) {
    if ( ! is_product() ) {
      return;
    }

    add_action( 'admin_head','acf_form_head', 20 );
    $variation_id = $variation->ID; ?>
    <tr>
      <td colspan="2">
        <?php /* Attach the ACF Form to the variation */
        acf_form(
          [
            'id'                    => 'acf_variaton_form',
            'post_id'               => $variation_id,
            'form'                  => true, // Show the form on the variation
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'field_groups'          => [ 336 ], // This is the ID of the ACF Group
            'return'                => add_query_arg(
              [
                'post_id' => $variation->post_parent,
                'updated' => 'true',
              ],
              get_edit_post_link( $variation->post_parent, '' ) // Output the edit link
            ),
          ]
        ); ?>
      </td>
    </tr>
  <?php
  }
}
woocommerce_acf_variations::setup();
