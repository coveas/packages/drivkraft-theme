<?php

class drivkraft_best_sellers_in_category {

  static function setup() {
    // Add shortcode before content
    add_shortcode( 'best_selling_products_in_current_category', __CLASS__ . '::best_selling_products_in_current_category' );
    // add_action( 'woocommerce_before_shop_loop', __CLASS__ . '::best_selling_this_category_shortcode_taxonomy' );

    // To test if 'Best sellers' are working
    // add_action( 'woocommerce_shop_loop_item_title',  __CLASS__ . '::wc_product_sold_count', 11 );
  }

  // To test if 'Best sellers' are working
  static function wc_product_sold_count() {
    global $product;
    $units_sold = get_post_meta( $product->id, 'total_sales', true );
    echo '<p>' . sprintf( __( 'Units Sold: %s', 'drivkraft-theme' ), $units_sold ) . '</p>';
  }

  static function best_selling_products_in_current_category( $atts ) {
    global $woocommerce_loop;

    // Get out the attributes
    extract(
      shortcode_atts(
        [
          'cats'    => '',
          'tax'     => 'product_cat',
          'per_cat' => '6',
          'columns' => '6',
        ],
      $atts)
    );

    if ( empty( $cats ) ) {
      $terms = get_terms( 'product_cat', [
        'hide_empty' => true,
        'fields'     => 'ids',
      ] );
      $cats = implode( ',', $terms );
    }

    $cats = explode( ',', $cats );

    if ( empty( $cats ) ) {
      return '';
    }

    ob_start();

    foreach ( $cats as $cat ) {

      // get the product category
      $term = get_term( $cat, $tax );

      // setup query
      $args = [
        'post_type'           => 'product',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => 1,
        'posts_per_page'      => $per_cat,
        'meta_key'            => 'total_sales',
        'orderby'             => 'meta_value_num',
        'tax_query'           => [
          [
            'taxonomy' => $tax,
            'field'    => 'id',
            'terms'    => $cat,
          ],
        ],
        'meta_query'  => [
          [
            'key'     => '_visibility',
            'value'   => [ 'catalog', 'visible' ],
            'compare' => 'IN',
          ],
        ],
      ];

      // set woocommerce columns
      $woocommerce_loop['columns'] = $columns;
      $products = new WP_Query( $args );
      $woocommerce_loop['columns'] = $columns;
      if ( $products->have_posts() ) :
        woocommerce_product_loop_start();
        while ( $products->have_posts() ) : $products->the_post();
          wc_get_template_part( 'content', 'product' );
        endwhile; // end of the loop.
        woocommerce_product_loop_end();
      endif;
      wp_reset_postdata();
    }
    return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
  }

  /* Get the shortcode into a function */
  static function best_selling_this_category_shortcode_taxonomy() {
    if ( ! is_tax() ) {
      return;
    }
    global $wp_query;
    $term = $wp_query->get_queried_object();
    $title = $term->name;
    echo do_shortcode( "[best_selling_products_in_current_category cats=$title]" );
  }
}

drivkraft_best_sellers_in_category::setup();
