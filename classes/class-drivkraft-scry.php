<?php

/**
 * use drivkraft_srcy
 */

trait drivkraft_srcy {
  /**
   * Scry - determine the css & js uri based on a relative path
   * Also implement cache busting basted on file make time
   */
  static function scry( $path ) {
    $uri = get_template_directory_uri();
    $dir = get_template_directory();

    if ( file_exists( $dir . $path ) ) {
      // If the file exist, lets get the last time it was modified
      $file_uri = $uri . $path .'?'. filemtime( $dir . $path );
    } else {
      // Just give us the file, no query
      $file_uri = $uri . $path;
    }
    return apply_filters( 'drivkraft-scry', $file_uri );
  }
}