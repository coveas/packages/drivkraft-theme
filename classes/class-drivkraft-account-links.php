<?php
class drivkraft_account_links{

  static function setup() {
    add_action( 'account_additional_items', __CLASS__ . '::links' );
    add_action( 'mega_menu_additional_items', __CLASS__ . '::links' );
  }

  static function links() {

    echo apply_filters( 'drivkraft_header_account_ul_before', '<ul class="header--account--list">' );

    /* Logout URL */
    $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
    if ( $myaccount_page_id && is_user_logged_in() ) {
      $logout_url = wp_logout_url( get_permalink( $myaccount_page_id ) );
      if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' ) {
        $logout_url = str_replace( 'http:', 'https:', $logout_url );
      }
      echo '<li class="header--account--item icon-acc menu-item">';
        echo '<a href="' . $logout_url . '">';
          _e( 'Logout', 'drivkraft-theme' );
        echo '</a>';
      echo '</li>';
    }

    /* My Account Page */
    $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
    if ( $myaccount_page_id ) {
      $myaccount_page_url = get_permalink( $myaccount_page_id );
      echo '<li class="header--account--item icon-acc menu-item">';
        echo '<a href="' . $myaccount_page_url . '">';
          _e( 'My account', 'drivkraft-theme' );
        echo '</a>';
      echo '</li>';
    }

    // Contact telephone number from ACF Theme Settings
    // get_field( 'telephone_number', 'option' ) <- If ACF isn't enabled, will fail
    $telephone   = esc_html( get_option( 'options_telephone_number' ) );
    $number_only = preg_replace( '/\D/', '', $telephone );
    if ( $telephone ) {
      echo '<li class="header--account--item icon-tel menu-item"><a href="tel:' . $number_only . '">' . $telephone . '</a></li>';
    }

    echo apply_filters( 'drivkraft_header_account_ul_after', '</ul>' );
  }
}

drivkraft_account_links::setup();
