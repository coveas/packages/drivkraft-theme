<?php

/**
* Filter function to force wordpress to add our custom srcset values
* @param array  $sources {
*     One or more arrays of source data to include in the 'srcset'.
*
*     @type type array $width {
*          @type type string $url        The URL of an image source.
*          @type type string $descriptor The descriptor type used in the image candidate string,
*                                        either 'w' or 'x'.
*          @type type int    $value      The source width, if paired with a 'w' descriptor or a
*                                        pixel density value if paired with an 'x' descriptor.
*     }
* }
* @param array  $size_array    Array of width and height values in pixels (in that order).
* @param string $image_src     The 'src' of the image.
* @param array  $image_meta    The image meta data as returned by 'wp_get_attachment_metadata()'.
* @param int    $attachment_id Image attachment ID.
*/

class craft_pagespeed {

  // Load the new pagespeed ready images into the
  static function setup() {
    add_action( 'init', __CLASS__ . '::images', 30 );
    add_filter( 'wp_calculate_image_srcset', __CLASS__ . '::srcset', 10, 5 );
    add_filter( 'max_srcset_image_width', function( $max_srcset_image_width, $size_array ) { return 2000; }, 10, 2 );
  }

  // Add additional image sizes for mobile devices
  static function images() {
    // Ensure no cropping to keep the ratio with false flag at the end
    add_image_size( 'tiny_catelog_image', 160, 160, false );
    add_image_size( 'tiny_product_image', 200, 250, false );
    add_image_size( 'tiny_banner', 300, 300, false );
  }

  // Load the srcset and addd the new sizes
  static function srcset( $sources, $size_array, $image_src, $image_meta, $attachment_id ) {

    // List the sizes added in images()
    $new_sizes = [
      'tiny_catelog_image',
      'tiny_product_image',
      'tiny_banner'
    ];

    // Image base name
    $image_basename = wp_basename( $image_meta['file'] );

    // Upload directory info array
    if ( function_exists( 'wp_get_upload_dir' ) ) {
      $upload_dir_info_arr = wp_get_upload_dir();
    } else {
      $upload_dir_info_arr = wp_upload_dir();
    }

    // Base url of upload directory
    $baseurl = $upload_dir_info_arr['baseurl'];

    // Uploads are (or have been) in year/month sub-directories.
    if ( $image_basename !== $image_meta['file'] ) {
      $dirname = dirname( $image_meta['file'] );

      if ( '.' !== $dirname) {
        $image_baseurl = trailingslashit( $baseurl ) . $dirname;
      }
    }

    // Trailing slash
    $image_baseurl = trailingslashit( $image_baseurl );

    // Loop through size array & add to srcset
    foreach ( $new_sizes as $size ) {

      // Check whether our custom image size exists in image meta
      if ( array_key_exists( $size, $image_meta['sizes'] ) ) {

        // Add source value to create srcset
        $sources[ $image_meta['sizes'][$size]['width'] ] = array(
         'url'        => $image_baseurl .  $image_meta['sizes'][$size]['file'],
         'descriptor' => 'w',
         'value'      => $image_meta['sizes'][$size]['width'],
         );
      }
    }

    // Return sources with new srcset value
    return $sources;
  }
}

craft_pagespeed::setup();
