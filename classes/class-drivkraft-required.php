<?php

// Lets make sure you can't install craft.
if ( ! class_exists( 'acf' ) || ! class_exists( 'woocommerce' ) ) {

  ob_start(); ?>
  <h1><?php _e( 'Hey, Listen!', 'drivkraft-theme' ); ?></h1>
  <p><?php _e( 'Craft needs ACF Pro and Woocommerce to think about rendering properly. Please download, install and activate them', 'drivkraft-theme' ); ?></p>
  <p><?php echo sprintf( wp_kses( __( 'If you have already installed them, make sure they\'re activated <a href="%s">on the plugin page</a>.', 'drivkraft-theme' ), [ 'a' => [ 'href' => [] ] ] ), esc_url( admin_url( 'plugins.php' ) ) ); ?></p>
  <?php $craft_acf_notice_msg = ob_get_clean();

  /*
  * Admin notice
  */
  add_action( 'admin_notices', 'craft_notice_missing_acf' );
  function craft_notice_missing_acf() {
    global $craft_acf_notice_msg;
    echo '<div class="notice notice-error notice-large"><div class="notice-title">'. $craft_acf_notice_msg .'</div></div>';
  }


  /*
  * Frontend notice
  */
  add_action( 'template_redirect', 'craft_notice_frontend_missing_acf', 0 );
  function craft_notice_frontend_missing_acf() {
    global $craft_acf_notice_msg;
    wp_die( $craft_acf_notice_msg );
  }
}
