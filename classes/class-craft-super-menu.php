<?php

/**
 * Super menu
 * Using ACF Flexible content, the mega menu has been
 * rewritten to cater for the evolving landscape that
 * is the main menu. Now encapsulating everything from
 * the previous menu, but with the ability to be extended
 * and adapted without needing to 'hack' into it.
 *
 * !! Note: static functions must be the same name as the
 * flexible content layout name in order for this to work.
 */

class supermenu {

  /**
   * [$count Calls the correct fields by flexible content]
   * @var integer
   */
  static $count = 0;

  /**
   * [field helper – Allows the menu items to be
   * called without writing this long phrase over
   * and over. Also keeps the count correct.]
   * @param  [type] $item [description]
   * @return [type]       [description]
   */
  static function field( $item ) {
    return get_option( 'options_build_the_menu_' . self::$count . '_' . $item );
  }

  /**
   * Check term links aren't causing errors
   * @param  [type] $term [description]
   * @return [type]       [description]
   */
  static function term_link_failsafe( $term ) {
    if ( empty( $term ) ) {
      return '#';
    }
    $link = get_term_link( $term, 'product_cat' );
    if ( ! is_wp_error( $link ) ) {
      return $link;
    }
    // Nothing working?
    return '#';
  }

  /**
   * [super_menu Builds the menu by looping through
   * all static ]
   * @return [type] [description]
   */
  static function build() {
    $rows = get_option( 'options_build_the_menu' );
    // Double check we've got something to use
    if ( empty( $rows ) ) {
      return;
    }
    // Open up a fish shop
    echo apply_filters( 'drivkraft_super_menu_container_markup', '<div class="super__menu">' ); // Allow this markup to be edited.
      echo apply_filters( 'drivkraft_super_menu_wrap_markup', '<ul id="nav" class="super__menu--wrap">' ); // Allow this markup to be edited.
        foreach( (array) $rows as $count => $row ) {
          if ( ! $row ) {
            continue;
          }
          self::$count = $count;
          call_user_func( __CLASS__ . "::$row" );
        }
        // Close the fish shop down.
      echo apply_filters( 'drivkraft_super_menu_wrap_close_markup', '</ul>' ); // Allow this markup to be edited.
      do_action( 'craft_super_menu_after_build' );
    echo apply_filters( 'drivkraft_super_menu_container_close_markup', '</div>' ); // Allow this markup to be edited.
    self::$count = null;
  }

  /**
   * [A separator can just be a visual mark between links]
   * @return [type] [description]
   */
  static function separator() {
    echo '<li class="separator">&nbsp;</li>';
  }

  /**
   * External link
   * @return [string] [Link]
   */
  static function external_link() {
    $title = esc_html( self::field( 'title' ) );
    $link  = self::field( 'external_link' );
    echo'<li class="super__menu--item super__menu--external">';
    $blank = apply_filters( 'drivkraft_external_links_force_blank', 'target=""' );
    echo sprintf( '<a class="super__menu--item--link" %3$s rel="nooppener" href="%2$s">%1$s</a>', $title, $link, $blank );
    echo '</li>';
  }

  /**
   * [A link that spans the width of the website,
   * containing multiple columns of links]
   * @return [type] [description]
   */
  static function full_width_selection() {
    $title = esc_html( self::field( 'title' ) );
    $type  = self::field( 'link_type' );

    // The type of link will change the type of content it contains
    // If it is a category:
    if ( 'category' === $type ) {
      $category = get_term( self::field( 'title_link_category' ) );

      // If no category, then there'll be no name, so we need to fix this
      if ( ! is_wp_error( $category ) ) {
        // Set link to category
        $link     = self::term_link_failsafe( $category->name );
        // Get the selected IDs for taxonomies
        $columns  = self::field( 'sub_categories' );
      } else {
        // Empty category means empty variables
        $link = '#';
        $columns = null;
      }

    } else {
      // If it is a page or an article
      $link = get_the_permalink( self::field( 'title_link' ) );
      // Get the selected IDs for pages
      $columns = self::field( 'sub_pages_posts' );
    }
  // Build the HTML for this section
  ?>

    <li class="super__menu--item <?php echo ( ! empty( $columns) ) ? 'kids' : 'single'; ?>">

      <a class="super__menu--item--link" href="<?php echo $link ?>" data-count="<?php echo self::$count; ?>"><?php echo $title ?></a>

      <div class="<?php echo apply_filters( 'craft_super_menu_drop_class', 'hidden__dragon' ); ?>"><?php

        do_action( 'drivkraft_before_hidden_dragon_wrap' );

        ?><div class="hidden__dragon--wrap">
          <?php
            // Check that theres a sub-menu that we can loop
            // through. This check allows clients to create
            // very simple menu items without the fancy drop.
            if ( ! empty( $columns ) ) { ?>
              <div class="hidden__dragon--columns">
                <?php
                // Loop through all of the available columns
                for ( $i = 0; $i < $columns; $i++ ) {

                  if ( 'category' === $type ) {
                    // If category to select
                    $column_title = self::field( 'sub_categories_' . $i . '_column_title' );
                    $category     = get_term( self::field( 'sub_categories_' . $i . '_link' ) );
                    if ( is_wp_error( $category ) ) {
                      continue;
                    }
                    $link         = self::term_link_failsafe( $category->name );
                    $ids          = self::field( 'sub_categories_' . $i . '_categories_to_display' );
                  } else {
                    // If pages or posts to select
                    $column_title = self::field( 'sub_pages_posts_' . $i . '_column_title' );
                    $link = get_the_permalink( self::field( 'sub_pages_posts_' . $i . '_link' ) );
                    $ids  = self::field( 'sub_pages_posts_' . $i . '_pages_to_display' );
                  }
                  ?>
                  <?php /* <!-- Column repeaters --> */ ?>
                  <ul class="super__menu--sub-menu">

                    <?php /* <!-- Column Title of the column section--> */
                      do_action( 'craft_super_menu_before_column', $column_title ); ?>
                    <li class="super__menu--item super__menu--title ">
                      <a href="<?php echo $link; ?>"><?php echo $column_title; ?></a>
                    </li>
                    <?php /* <!-- End of title --> */ ?>

                    <?php if ( ! empty( $ids)  ) {
                      foreach ( $ids as $id ) {
                        // Check for ID
                        if ( ! $id ) {
                          continue;
                        }

                        // If there's no type. Fuck it. Just quit.
                        if ( ! $type ) {
                          continue;
                        }

                        // Check if this is a category or a post
                        if ( 'category' === $type ) {
                          // $id is a taxonomy
                          $category = get_term( $id, 'product_cat' );
                          // Check that category exists again
                          if ( empty( $category ) ) {
                            continue;
                          }
                          $title    = $category->name;
                          $link     = get_term_link( $category->slug, 'product_cat' );
                          if ( is_wp_error( $title ) || is_wp_error( $link ) ) {
                            continue;
                          }
                        } else {
                          // $id is a post_id
                          $title = get_the_title( $id );
                          $link  = get_the_permalink( $id );
                        }
                        ?>
                        <?php /* <!-- Loop through the sublinks--> */ ?>
                        <li class="super__menu--item sub-menu--item">
                          <a href="<?php echo $link; ?>"><?php echo $title; ?></a>
                        </li>
                        <?php /* <!-- End of loop sublinks--> */ ?>
                      <?php }
                    }
                    do_action( 'craft_super_menu_after_column', $column_title ); ?>
                  </ul>
                  <?php /* <!-- End of Column repeaters--> */
                } // $i++
                ?>
              </div>
              <?php
            } /* End of ! empty( columns ) */ ?>


            <?php
              /**
               * Related content
               * products : Selected products
               * best_sellers : Related products
               * banner : Banner image
               * posts : Articles
               */
              $title          = self::field( 'related_content_title' );
              $display        = self::field( 'related_display' ); // Multiple choice

              // Looping through available options
              if ( 'products' === $display ) {
                $selected_ids = self::field( 'selected_products' );
                if ( empty( $selected_ids ) ) {
                  return;
                }
                ?>
                  <ul class="super__menu--related super__menu--products">
                    <?php
                      do_action( 'craft_super_menu_before_products', $title );
                      $args = [
                        'post_type' => 'product',
                        'post__in'  => $selected_ids
                        ];
                      $loop = new WP_Query( $args );
                      if ( $loop->have_posts() ) {
                        while ( $loop->have_posts() ) : $loop->the_post();
                          wc_get_template_part( 'content', 'product' );
                        endwhile;
                      }
                      wp_reset_postdata();

                      do_action( 'craft_super_menu_after_products', $title );
                    ?>
                  </ul><?php /* <!--/.products--> */ ?>
                <?php

              } elseif ( 'best_sellers' === $display ) {
                $related_cat = self::field( 'related_products' );
                $term = get_term( $related_cat, 'product_cat' );

                // setup query for best selling within a category
                $args = [
                  'post_type'           => 'product',
                  'post_status'         => 'publish',
                  'ignore_sticky_posts' => 1,
                  'posts_per_page'      => $per_cat,
                  'meta_key'            => 'total_sales',
                  'orderby'             => 'meta_value_num',
                  'tax_query'           => [
                    [
                      'taxonomy' => $tax,
                      'field'    => 'id',
                      'terms'    => $cat,
                    ],
                  ],
                  'meta_query'  => [
                    [
                      'key'     => '_visibility',
                      'value'   => [ 'catalog', 'visible' ],
                      'compare' => 'IN',
                    ],
                  ],
                ];

              } elseif ( 'banner' === $display ) {
                $banners = self::field( 'select_banner' );
                var_dump( $banners );
                echo '<script>console.log( "Banners not available just yet" )</script>';

              } else {
                $articles = self::field( 'selected_article__post' );
                var_dump( $articles );
                echo '<script>console.log( "Articles not available just yet" )</script>';
              }

            ?>
        </div><?php

        do_action( 'drivkraft_after_hidden_dragon_wrap' );

      ?></div>

    </li>
  <?php
  }

}
