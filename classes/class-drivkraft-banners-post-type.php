<?php


class driv_banners_post_type {

  static function setup() {
    add_action( 'init', __CLASS__ . '::banners' );
    add_action( 'init', __CLASS__ . '::banner_groups' );
    add_action( 'admin_head', __CLASS__ . '::style_banner_layout_list' );
    add_action( 'admin_menu' , __CLASS__ . '::remove_banner_group_metabox' );

    add_filter( 'manage_taxonomies_for_banner_columns', __CLASS__ . '::banner_columns' );
    add_action( 'restrict_manage_posts', __CLASS__ . '::banner_group_filter_list' );
    // add_filter( 'parse_query', __CLASS__ . '::perform_filtering' );

  }

  static function banners() {

    $labels = array(
      'name'                  => _x( 'Banners', 'Post Type General Name', 'drivkraft-theme' ),
      'singular_name'         => _x( 'Banner', 'Post Type Singular Name', 'drivkraft-theme' ),
      'menu_name'             => __( 'Banners', 'drivkraft-theme' ),
      'name_admin_bar'        => __( 'Banner', 'drivkraft-theme' ),
      'archives'              => __( 'Banner Archives', 'drivkraft-theme' ),
      'parent_item_colon'     => __( 'Parent Banner:', 'drivkraft-theme' ),
      'all_items'             => __( 'All Banners', 'drivkraft-theme' ),
      'add_new_item'          => __( 'Add New Banner', 'drivkraft-theme' ),
      'add_new'               => __( 'Add New', 'drivkraft-theme' ),
      'new_item'              => __( 'New Banner', 'drivkraft-theme' ),
      'edit_item'             => __( 'Edit Banner', 'drivkraft-theme' ),
      'update_item'           => __( 'Update Banner', 'drivkraft-theme' ),
      'view_item'             => __( 'View Banner', 'drivkraft-theme' ),
      'search_items'          => __( 'Search Banners', 'drivkraft-theme' ),
      'not_found'             => __( 'Not found', 'drivkraft-theme' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'drivkraft-theme' ),
      'featured_image'        => __( 'Featured Image', 'drivkraft-theme' ),
      'set_featured_image'    => __( 'Set featured image', 'drivkraft-theme' ),
      'remove_featured_image' => __( 'Remove featured image', 'drivkraft-theme' ),
      'use_featured_image'    => __( 'Use as featured image', 'drivkraft-theme' ),
      'insert_into_item'      => __( 'Insert into Banner', 'drivkraft-theme' ),
      'uploaded_to_this_item' => __( 'Uploaded to this Banner', 'drivkraft-theme' ),
      'items_list'            => __( 'Banner list', 'drivkraft-theme' ),
      'items_list_navigation' => __( 'Banner list navigation', 'drivkraft-theme' ),
      'filter_items_list'     => __( 'Filter Banner list', 'drivkraft-theme' ),
      );

    $args = array(
      'label'                 => __( 'Banner', 'drivkraft-theme' ),
      'description'           => __( 'Different instances of the Banner', 'drivkraft-theme' ),
      'labels'                => $labels,
      'supports'              => [ 'title' ],
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 10,
      'menu_icon'             => 'dashicons-feedback',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,
      'exclude_from_search'   => true,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
      );

    register_post_type( 'banners', $args );
  }

  /* Custom Banner groups */
  static function banner_groups() {
    $labels = array(
      'name'                       => _x( 'Banner groups', 'Taxonomy General Name', 'drivkraft-theme' ),
      'singular_name'              => _x( 'Banner group', 'Taxonomy Singular Name', 'drivkraft-theme' ),
      'menu_name'                  => __( 'Banner group', 'drivkraft-theme' ),
      'all_items'                  => __( 'All Banner groups', 'drivkraft-theme' ),
      'parent_item'                => __( 'Parent Banner group', 'drivkraft-theme' ),
      'parent_item_colon'          => __( 'Parent Banner group:', 'drivkraft-theme' ),
      'new_item_name'              => __( 'New Banner group Name', 'drivkraft-theme' ),
      'add_new_item'               => __( 'Add New Banner group', 'drivkraft-theme' ),
      'edit_item'                  => __( 'Edit Banner group', 'drivkraft-theme' ),
      'update_item'                => __( 'Update Banner group', 'drivkraft-theme' ),
      'view_item'                  => __( 'View Banner group', 'drivkraft-theme' ),
      'separate_items_with_commas' => __( 'Separate Banner groups with commas', 'drivkraft-theme' ),
      'add_or_remove_items'        => __( 'Add or remove Banner groups', 'drivkraft-theme' ),
      'choose_from_most_used'      => __( 'Choose from the most used', 'drivkraft-theme' ),
      'popular_items'              => __( 'Popular Banner groups', 'drivkraft-theme' ),
      'search_items'               => __( 'Search Banner groups', 'drivkraft-theme' ),
      'not_found'                  => __( 'Not Found', 'drivkraft-theme' ),
    );

    $rewrite = array(
      'slug'                       => 'banner_group',
      'with_front'                 => true,
      'hierarchical'               => false,
    );

    $capabilities = array(
      'manage_terms'               => 'manage_product_terms',
      'edit_terms'                 => 'edit_product_terms',
      'delete_terms'               => 'delete_product_terms',
      'assign_terms'               => 'assign_product_terms',
    );

    $args = array(
      'labels'                     => $labels,
      'hierarchical'               => true,
      'public'                     => true,
      'show_ui'                    => true,
      'show_admin_column'          => true,
      'show_in_nav_menus'          => true,
      'show_tagcloud'              => true,
      'rewrite'                    => $rewrite,
      'capabilities'               => $capabilities,
    );

    register_taxonomy( 'banner_group', array( 'banners' ), $args );
  }

  /* Remove the taxonomy metabox, instead,
  we're going to create our own with ACF */
  static function remove_banner_group_metabox() {
    remove_meta_box( 'banner_groupdiv', 'banners', 'side' );
  }

  /* Sorting and Ordering of Banner Groups */
  static function banner_columns( $taxonomies ) {
    $taxonomies[] = 'banner_group';
    return $taxonomies;
  }

  static function banner_group_filter_list() {
    global $wp_query;
    // If we're not on the banners post type
    $screen = get_current_screen();
    if ( 'banners' != $screen->post_type ) {
      return;
    }

    wp_dropdown_categories( [
      'show_option_all' => _x( 'Show all banner groups', 'Select groups of banners', 'drivkraft-theme' ),
      'taxonomy'        => 'banner_group',
      'name'            => 'banner_group',
      'orderby'         => 'name',
      'selected'        => ( isset( $wp_query->query['banner_group'] ) ? $wp_query->query['banner_group'] : '' ),
      'hierarchical'    => false,
      'depth'           => 3,
      'show_count'      => false,
      'hide_empty'      => true,
    ] );
  }

  static function perform_filtering( $query ) {
    if ( ! is_admin() ) {
      return $query;
    }

    $qv = &$query->query_vars;
    if ( ( @$qv['banner_group'] ) && is_numeric( @$qv['banner_group'] ) ) {
      $term = get_term_by( 'id', $qv['banner_group'], 'banner_group' );
      $qv['banner_group'] = $term->slug;
    }
  }



  /* Allowing only one grade to be selected,
  I have created an ACF metabox in place of
  the one removed above. This means we can
  style these radio buttons to look better
  than a bunch of checkboxes. Both functional
  and stylish. Swagger, brah. */
  static function style_banner_layout_list() { ?>
    <style>
      [data-name="layout_style"] li {
          position: relative;
          width: 20%;
          margin: 0 !important;
          text-align: center;
          padding-top: 7rem;
      }

      [data-name="layout_style"] input[type="radio"]:after {
        content: "";
        width: 100px;
        height: 100px;
        display: block;
        background-size: 100px auto;
        position: absolute;
        top: 0;
        left: 50%;
        margin-left: -50px;
        border: 2px solid white;
      }

      [data-name="layout_style"] input[type="radio"]:checked:after{
        border: 2px solid #5b9dd9;
      }

      [data-name="layout_style"] input[type="radio"][value="1"]:after {
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/single.png' );
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/single.svg' );
      }
      [data-name="layout_style"] input[type="radio"][value="2"]:after {
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/double.png' );
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/double.svg' );
      }
      [data-name="layout_style"] input[type="radio"][value="2-5"]:after {
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/triple.png' );
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/triple.svg' );
      }
      [data-name="layout_style"] input[type="radio"][value="3"]:after {
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/stacked.png' );
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/stacked.svg' );
      }
      [data-name="layout_style"] input[type="radio"][value="4"]:after {
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/party.png' );
        background-image: url( '/wp-content/themes/drivkraft-theme/assets/images/party.svg' );
      }
    </style>
  <?php }


}

driv_banners_post_type::setup();