<?php

class drivkraft__customiser {

  static function setup() {

    // Craft customiser class
    add_filter( 'body_class', __CLASS__ . '::customiser_class', 1, 10 );

    // Add scripts first, load the styles up height
    add_action( 'wp_head', __CLASS__ . '::drivkraft_customize_css', 100 );
    add_action( 'customize_register', __CLASS__ . '::drivkraft_custom_editor' );

    // Add analytics scripts to the site
    add_action( 'wp_head', __CLASS__ . '::drivkraft_analytic_scripts_head', 50 );
    add_action( 'drivkraft_after_body_tag', __CLASS__ . '::drivkraft_analytic_scripts_body', 10 );
    add_action( 'wp_footer', __CLASS__ . '::drivkraft_analytic_scripts_footer', 50 );

    // Add the global product notice to product titles
    add_action( 'woocommerce_single_product_summary', __CLASS__ . '::global_product_notice', 6 ); // Title is 5

  }

  /**
   * Add classes to the body to allow custom styles
   * @param  [type] $classes [description]
   * @return [type]          [description]
   */
  static function customiser_class( $classes ) {
    $classes[] = 'customize-support'; // Always wanted
    $classes[] = 'craft-is-customised'; // Craft related customisations
    return $classes;
  }

  static function drivkraft_custom_editor( $wp_customize ) {

    /****************************** Sections **********************************/

    // Branding section
    $wp_customize->add_section('drivkraft_color_scheme', array(
        'title'       => __( 'Branding', 'drivkraft-theme' ),
        'description' => __( 'Branding', 'drivkraft-theme' ),
        'priority'    => 120,
        'transport'   => 'postMessage',
    ));

    // Google fonts section
    $wp_customize->add_section('driv_kraft_font_selection', array(
        'title'       => __( 'Typography', 'drivkraft-theme' ),
        'description' => __( 'Change the fonts used throughout the site using a service like google fonts', 'drivkraft-theme' ),
        'priority'    => 120,
        'transport'   => 'postMessage',
    ));

    // Woocommerce section
    $wp_customize->add_section('drivkraft_woocommerce', array(
        'title'       => __( 'Woocommerce', 'drivkraft-theme' ),
        'description' => __( 'Change how woocommerce handles default settings', 'drivkraft-theme' ),
        'priority'    => 150,
        'transport'   => 'postMessage',
    ));

    // Woocommerce Emails section
    // $wp_customize->add_section('drivkraft_woocommerce_emails_section', array(
    //     'title'       => __( 'Woocommerce Emails', 'drivkraft-theme' ),
    //     'description' => __( 'Add content to the emails woocommerce sends out', 'drivkraft-theme' ),
    //     'priority'    => 155,
    //     'transport'   => 'postMessage',
    // ));

    // Analytics section
    $wp_customize->add_section('drivkraft_analytics_section', array(
        'title'       => __( 'Analytics', 'drivkraft-theme' ),
        'description' => __( 'Enable tracking and analytics by placing your script tags in the correct location. <small><strong>Note:</strong> All scripts must be self-containing &lt;script&gt;&lt;/script&gt;, otherwise they will just print the code onto the website.</small>', 'drivkraft-theme' ),
        'priority'    => 160,
        'transport'   => 'postMessage',
    ));

    /***********************************************************************************/

    // Pasted link from google fonts
    $wp_customize->add_setting('google_fonts_link', array(
        'default'    => "<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700' rel='stylesheet' type='text/css'>",
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('google_fonts_link_control', array(
        'label'       => __( 'Custom google fonts', 'drivkraft-theme' ),
        'description' => __( 'Paste your &lt;link&gt; from Google fonts', 'drivkraft-theme' ),
        'section'     => 'driv_kraft_font_selection',
        'settings'    => 'google_fonts_link',
    ));

    // Pasted link from google fonts
    $wp_customize->add_setting('google_fonts_css', array(
        'default'    => 'h1{ font-family: "source-sans-pro", sans-serif; }',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('google_fonts_css', array(
        'label'       => __( 'Custom CSS', 'drivkraft-theme' ),
        'description' => __( 'Custom CSS for using your fonts', 'drivkraft-theme' ),
        'type'        => 'textarea',
        'section'     => 'driv_kraft_font_selection',
        'settings'    => 'google_fonts_css',
    ));

    /***********************************************************************************/

    // drivkraft_analytics_section

    // head scripts
    $wp_customize->add_setting('drivkraft_analytics_head', array(
        'default'    => '',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('drivkraft_analytics_head', array(
        'label'       => __( 'Scripts in the &lt;head&gt;', 'drivkraft-theme' ),
        'description' => __( 'Place script tags in the &lt;head&gt;', 'drivkraft-theme' ),
        'type'        => 'textarea',
        'input_attrs' => [
          'data-name' => __( 'Head', 'drivkraft-theme' ),
          'placeholder' => __( '&lt;script&gt;google.analytic.manager...', 'drivkraft-theme' ),
        ],
        'section'     => 'drivkraft_analytics_section',
        'settings'    => 'drivkraft_analytics_head',
    ));

    // After body scripts
    $wp_customize->add_setting('drivkraft_analytics_after_body', array(
        'default'    => '',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('drivkraft_analytics_after_body', array(
        'label'       => __( 'Place scripts just after the &lt;body&gt; tag', 'drivkraft-theme' ),
        'description' => __( 'Insert script tags  for using your fonts', 'drivkraft-theme' ),
        'type'        => 'textarea',
        'input_attrs' => [
          'data-name' => __( 'Body', 'drivkraft-theme' ),
          'placeholder' => __( '&lt;script&gt;google.tag.manager...', 'drivkraft-theme' ),
        ],
        'section'     => 'drivkraft_analytics_section',
        'settings'    => 'drivkraft_analytics_after_body',
    ));

    // Footer scripts
    $wp_customize->add_setting('drivkraft_analytics_footer', array(
        'default'    => '',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control('drivkraft_analytics_footer', array(
        'label'       => __( 'Scripts in the &lt;footer&gt;', 'drivkraft-theme' ),
        'description' => __( 'Place scripts in the footer', 'drivkraft-theme' ),
        'type'        => 'textarea',
        'input_attrs' => [
          'data-name' => __( 'Footer', 'drivkraft-theme' ),
          'placeholder' => __( '&lt;script&gt;RUM.manager...', 'drivkraft-theme' ),
        ],
        'section'     => 'drivkraft_analytics_section',
        'settings'    => 'drivkraft_analytics_footer',
    ));

    /***********************************************************************************/

    // Woocommerce

    // Does shop / category pages have a sidebar?
    $wp_customize->add_setting('shop_has_sidebar', array(
        'default'    => false,
        'type'       => 'option',
    ));
    $wp_customize->add_control( 'shop_has_sidebar', array(
        'label'       => __( 'Category sidebar', 'drivkraft-theme' ),
        'description' => __( 'Does the category page need a sidebar?', 'drivkraft-theme' ),
        'section'     => 'drivkraft_woocommerce',
        'settings'    => 'shop_has_sidebar',
        'type'        => 'checkbox',
    ));

    // Display price for free shipping
    $wp_customize->add_setting('show_rate_price', array(
        'default'    => true,
        'type'       => 'option',
    ));
    $wp_customize->add_control( 'show_rate_price', array(
        'label'       => __( 'Always show shipping rate price', 'drivkraft-theme' ),
        'description' => __( 'Show the price even if it is zero', 'drivkraft-theme' ),
        'section'     => 'drivkraft_woocommerce',
        'settings'    => 'show_rate_price',
        'type'        => 'checkbox',
    ));

    // How many products per row?
    $wp_customize->add_setting('product_row_count', array(
        'default'    => '3',
    ));

    $wp_customize->add_control( 'product_row_count', array(
      'section'     => 'drivkraft_woocommerce',
      'type'        => 'number',
      'label'       => __( 'Products per row', 'drivkraft-theme' ),
      'description' => __( 'Control how many products are in a row', 'drivkraft-theme' ),
      'transport'   => 'postMessage',
      'input_attrs' => [
        'min'  => 2,
        'max'  => 6,
        'step' => 1,
      ],
    ) );


    // Notice under product titles on single product page
    //

    /*****************/

    $wp_customize->add_setting('enable_global_product_notice', array(
        'default'    => false,
    ));
    $wp_customize->add_control( 'enable_global_product_notice', array(
        'label'       => __( 'Enable global product notice', 'drivkraft-theme' ),
        'section'     => 'drivkraft_woocommerce',
        'settings'    => 'enable_global_product_notice',
        'type'        => 'checkbox'
    ));

    $wp_customize->add_setting( 'global_product_notice', array(
      'default'     => '',
      'placeholder' => __( 'Free shipping for all sale items', 'drivkraft-theme' ),
      'capability'  => 'edit_theme_options'
    ));

    $wp_customize->add_control( 'global_product_notice', array(
      'label'       => __( 'Gloabl product notice', 'drivkraft-theme' ),
      'description' => __( 'This notice will show up under your ', 'drivkraft-theme' ),
      'type'        => 'textarea',
      'section'     => 'drivkraft_woocommerce',
      'settings'    => 'global_product_notice'
    ));

    /*****************/

    /* SVG Colour loading */
    $wp_customize->add_setting(
      'svg_before_image_colour', array(
        'default'           => '#f4f4f4',
        'sanitize_callback' => 'sanitize_hex_color',
        'transport'         => 'postMessage',
        'priority'          => 150,
      )
    );
    $wp_customize->add_control (
      new WP_Customize_Color_Control(
        $wp_customize, 'svg_before_image_colour', array(
          'label'       => __( 'Pre-loaded image colour', 'drivkraft-theme' ),
          'description' => __( 'Before your banners are loaded, we block out the image with colour. So while the image is loading, the page can load the layout', 'drivkraft-theme' ),
          'section'     => 'drivkraft_woocommerce',
          'settings'    => 'svg_before_image_colour'
        )
      )
    );

    // Woocommerce has a default image.
    // Lets update that image
    $wp_customize->add_setting('no_image_available', array(
        'default'    => '',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'woocommerce_no_image', array(
        'label'    => __( 'Missing Image Upload', 'drivkraft-theme' ),
        'section'  => 'drivkraft_woocommerce',
        'settings' => 'no_image_available',
    )));

    // Payment icon on single product
    // page, next to the price.
    $wp_customize->add_setting('single_product_payment_icon', array(
        'default'    => '',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'payment_icon', array(
        'label'       => __( 'Payment Icon Upload', 'drivkraft-theme' ),
        'description' => __( 'This payment icon will show next to the price on the product page.', 'drivkraft-theme' ),
        'section'     => 'drivkraft_woocommerce',
        'settings'    => 'single_product_payment_icon',
    )));



    /***********************************************************************************/

    // Branding section

    // Adding logo for the site
    $wp_customize->add_setting('site_logo', array(
        'default'    => '',
    ));

    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'site_logo_image', array(
        'label'    => __( 'Logo Image Upload', 'drivkraft-theme' ),
        'section'  => 'drivkraft_color_scheme',
        'settings' => 'site_logo',
    )));

    // Colour of all the links throughout the site, this is the main
    // brand colour, so it's used for titles, buttons etc.
    $wp_customize->add_setting('link_color', array(
        'default'           => '#ed3030',
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport'         => 'postMessage',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'link_color', array(
        'label'       => __( 'Brand Colour', 'drivkraft-theme' ),
        'description' => __( 'This colour is used throughout the site to highlight everything related to your brand.', 'drivkraft-theme' ),
        'section'     => 'drivkraft_color_scheme',
        'settings'    => 'link_color',
    )));

    // Price colour
    $wp_customize->add_setting('price_colour', array(
        'default'           => '#222222',
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport'         => 'postMessage',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'price_colour', array(
        'label'       => __( 'Price colour', 'drivkraft-theme' ),
        'description' => __( 'Change the colour of the price on the category list', 'drivkraft-theme' ),
        'section'     => 'drivkraft_color_scheme',
        'settings'    => 'price_colour',
    )));

    // Checkout flow colour
    // This colour will colour anything that leads to the checkout or cart
    $wp_customize->add_setting('checkout_leading_colour', array(
        'default'           => '#479AD9',
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport'         => 'postMessage',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'checkout_leading_colour', array(
        'label'       => __( 'Checkout flow colour', 'drivkraft-theme' ),
        'description' => __( 'Anything that helps the customers get to the checkout can be coloured specifically to enforce the users decision to buy something. Change this colour to something that helps that mentallity.', 'drivkraft-theme' ),
        'section'     => 'drivkraft_color_scheme',
        'settings'    => 'checkout_leading_colour',
    )));

  }

  /***********************************************************************************/

  // Add scripts to the <head>
  static function drivkraft_analytic_scripts_head() {
    echo get_theme_mod( 'drivkraft_analytics_head', '' );
  }

  // Add scripts to the <body>
  static function drivkraft_analytic_scripts_body() {
    echo get_theme_mod( 'drivkraft_analytics_after_body', '' );
  }

  // Add scripts to <footer>
  static function drivkraft_analytic_scripts_footer() {
    echo get_theme_mod( 'drivkraft_analytics_footer', '' );
  }

  /* Everything that can be customised will be output here.
  @Thomas These options should come from the fonts at the top of this page */
  static function drivkraft_customize_css() {
    echo get_theme_mod( 'google_fonts_link' );
    $brand_colour         = get_theme_mod( 'link_color', '#f67f67' );
    $price_colour         = get_theme_mod( 'price_colour', '#222222' );
    $checkout_flow_colour = get_theme_mod( 'checkout_leading_colour', '#479AD9' );
    ?>
    <style type="text/css">
      <?= get_theme_mod( 'google_fonts_css', '' ); ?>

      /* Background colour */
      .unslider-arrow,
      .button,
      .mailchimp--newsletter [type="submit"],
      .wpcf7-submit,
      .pagination a.page-numbers,
      .pagination span.page-numbers,
      .woocommerce-pagination a.page-numbers,
      .woocommerce-pagination span.page-numbers,
      .featured-categories--button,
      .widget_price_filter .ui-slider .ui-slider-handle,
      .widget_price_filter .ui-slider .ui-slider-range,
      .variations .radio-select .radio-option:checked ~ label {
        background-color: <?php echo $brand_colour; ?>;
        color: white;
      }

      .stars a.star-1:before,
      .stars a.star-2:before,
      .stars a.star-3:before,
      .stars a.star-4:before,
      .stars a.star-5:before,
      .stars a.star-1:after,
      .stars a.star-2:after,
      .stars a.star-3:after,
      .stars a.star-4:after,
      .stars a.star-5:after {
        background-color: <?php echo $brand_colour; ?>;
      }

      /* Colour */
      a,
      a:hover,
      .header--cart--link a,
      .page--sidebar .menu-item-has-children.current_page_ancestor > a,
      .page--sidebar .menu-item-has-children.current-menu-item > a,
      .page--sidebar .menu-item-has-children.current_page_ancestor > a:hover,
      .page--sidebar .menu-item-has-children.current-menu-item > a:hover{
        color: <?php echo $brand_colour ?>;
      }

      /* Price colour */
      .driv-product-set-order-options .driv_selected_product_price,
      .driv-add-to-cart-box p.price ins,
      ul.products li.product .price,
      ul.products li.product ins,
      ul.products li.product .price > span.amount,
      ul.products li.type-product .price,
      ul.products li.type-product ins,
      ul.products li.type-product .price > span.amount,
      .summary [itemprop="offers"] .price,
      .summary [itemprop="offers"] ins {
        color: <?php echo $price_colour; ?>
      }

      @media ( max-width: 47.9em ) {
        .header--navigation li.menu-item.is-active > a,
        .header--navigation li.menu-item.is-active.mega-menu--item-has-children > a:after {
          color: <?php echo $brand_colour ?>;
        }
      }

      @media ( min-width: 47.9em ) {
        .header--cart--link a { color: <?php echo $brand_colour ?>; }
      }

      /* Border colour */
      #mc_embed_signup input.mce_inline_error,
      .variations div.swatch-wrapper.selected,
      .variations .radio-select .radio-option:checked ~ label {
        border-color: <?php echo $brand_colour ?>;
      }

      <?php do_action( 'drivkraft_customiser_css', $brand_colour, $price_colour, $checkout_flow_colour ); ?>

    </style>

    <!-- Banner Placeholders -->
    <style type="text/css">

      picture ~ svg.svg-placeholder rect,
      picture ~ svg.svg-placeholder {
        vertical-align: bottom;
        background-color: <?= get_theme_mod( 'svg_before_image_colour', '#F7F7F7' );?>;
        fill: <?= get_theme_mod( 'svg_before_image_colour', '#F7F7F7' );?>;
      }

      @media screen and (max-width: 30em) {
        .placeholder-mobile { display: block; }
        .placeholder-tablet { display: none; }
        .placeholder-desktop { display: block; }
        .placeholder-mobile ~ .placeholder-desktop { display: none; }
      }

      @media screen and (min-width: 30em) and (max-width: 58em) {
        .placeholder-tablet { display: block; }
        .placeholder-mobile { display: block; }
        .placeholder-desktop { display: block; }

        .placeholder-tablet ~ .placeholder-mobile { display: none; }
        .placeholder-mobile ~ .placeholder-desktop { display: none; }
        .placeholder-tablet ~ .placeholder-desktop { display: none; }
      }

      @media screen and (min-width: 58em) {
        .placeholder-mobile { display: none; }
        .placeholder-tablet { display: none; }
        .placeholder-desktop { display: block; }
      }

    </style>

  <?php }

  // If global notice is available & enabled, show it
  static function global_product_notice() {

    $check = get_theme_mod( 'enable_global_product_notice', '' );
    if ( ! $check ) {
      return '';
    }
    $notice = get_theme_mod( 'global_product_notice', '' );
    if ( ! $notice ) {
      return '';
    }

    return '<div class="global-product-notice">' . $notice . '</div>';
  }

}

drivkraft__customiser::setup();
