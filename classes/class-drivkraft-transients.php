<?php
/**
 * Transient Cleaner
 */
class drivkraft_transient_cleanup_crew {

  static function clean() {

    // When posts are saved
    add_action( 'save_post', __CLASS__ . '::query_cleaner' );

    // ACF Cleaner
    add_action( 'acf/save_post', __CLASS__ . '::query_cleaner' );

    // When posts are created
    add_action( 'transition_post_status', __CLASS__ . '::query_cleaner' );
  }

  /**
   * Remove transients that
   * mention craft / drivkraft
   * @return [type] [description]
   */
  static function query_cleaner() {
    global $wpdb;
    $wpdb->query( "DELETE FROM $wpdb->options WHERE `option_name` LIKE ('_transient%_craft_%')
                                                 OR `option_name` LIKE ('_transient_timeout%_craft_%')
                                                 OR `option_name` LIKE ('_transient%_drivkraft_%')
                                                 OR `option_name` LIKE ('_transient_timeout%_drivkraft_%')" );
  }
}

drivkraft_transient_cleanup_crew::clean();
