<?php

/**
 * drivkraft theme - Woocommerce class
 */
class drivkraft__Woocommerce {
	/**
	 * Define support for the woocommerce plugin
	 * http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
	 */
	static function woocommerce_support() {
		// Add support
		add_theme_support( 'woocommerce' );
		add_theme_support( 'sensei' );

		// Remove WooCommerce Updater notice
		remove_action( 'admin_notices', 'woothemes_updater_notice' );

		// Best sellers in a category
		require_once( 'class-drivkraft-woocommerce-best-sellers.php' );

		// Customise woocommerce emails
		require_once( 'class-drivkraft-woocommerce-emails.php' );

		// Remove main woocommerce actions
		remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
		remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

		// Add theme woocommerce actions
		// Main content
		add_action( 'woocommerce_before_main_content', __CLASS__ . '::wrapper_start', 10 );
		add_action( 'woocommerce_after_main_content', __CLASS__ . '::wrapper_end', 10 );
		add_action( 'get_footer', __CLASS__ . '::wrapper_page_end' );

		add_action( 'woocommerce_after_shop_loop_item_title', __CLASS__ . '::show_shop_inventory' );
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' ); // Remove woo CSS
		add_action( 'template_redirect', __CLASS__ . '::remove_sidebar_shop' ); // Remove sidebar on product page

		/****************** Site wide notice ***************************/
		add_action( 'wp_footer', __CLASS__ . '::site_wide_message_script' );

		/****************** Better shipping layout on cart ***************************/
		// add_action( 'wp_footer', __CLASS__.'::better_shipping_on_cart', 10 );

		/****************** Update woocommerce default image sizes ***************************/
		add_action( 'after_switch_theme', __CLASS__ . '::craft_set_woocommerce_image_sizes', 10 );

		/***************************************************************/
		/************************ Breadcrumbs **************************/
		/***************************************************************/

		// Remove the breadcrumbs from the main loop & use Yoast
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
		add_action( 'drivcraft_after_header', __CLASS__ . '::breadcrumb_parent', 20 ); // Wrap breadcrumbs
		add_action( 'drivcraft_after_header', 'woocommerce_breadcrumb', 21 );
		add_action( 'drivcraft_before_content_begins', __CLASS__ . '::wrapper_end', 22 ); // Close breadcrumbs

		/***************************************************************/
		/************************ Account pages ************************/
		/***************************************************************/

		add_action( 'woocommerce_before_customer_login_form', __CLASS__ . '::wrap_single_login_form' );
		add_action( 'woocommerce_after_customer_login_form', __CLASS__ . '::close_wrap_single_login_form' );

		/***************************************************************/
		/********************** Shop / Category ************************/
		/***************************************************************/

		// Add image to the category header

		remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
		remove_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );
		// add_action( 'woocommerce_before_main_content', 'woocommerce_taxonomy_archive_description', 9 );
		// add_action( 'woocommerce_before_main_content', 'woocommerce_product_archive_description', 9 );
		add_action( 'woocommerce_before_main_content', __CLASS__ . '::changing_the_head_of_archive_category', 5 );

		// Remove title
		add_filter( 'woocommerce_show_page_title' , __CLASS__ . '::remove_shop_title' );

		// Wrap the results in a dev
		// add_action( 'woocommerce_before_shop_loop', __CLASS__ . '::wrapper_loop_start', 10 );
		// add_action( 'woocommerce_after_shop_loop', __CLASS__ . '::wrapper_loop_end', 80 );

		// Move the filters above all the content
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
		add_action( 'woocommerce_before_main_content', __CLASS__ . '::drivkraft__filter_bar', 6 );

		// Extended filter widget above products
		add_action( 'drivkraft__extended-filter', __CLASS__ . '::drivkraft__extended_filter', 6 );

		// Custom woocommerce placeholder image
		add_filter( 'woocommerce_placeholder_img_src', __CLASS__ . '::missing_image_replacement' );

		/***************************************************************/
		/********************** Single Product *************************/
		/***************************************************************/

		// Remove the title and ratings from the summary section

		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

		// Sale icon around the image
		add_action( 'woocommerce_before_single_product_summary', __CLASS__ . '::image_container', 9 ); // before sale
		add_action( 'woocommerce_before_single_product_summary', __CLASS__ . '::wrapper_loop_end', 21 ); // after images

		// Output the variation images into a list of clickable product images
		// add_action( 'woocommerce_before_variations_form', __CLASS__ . '::variation_images_on_single_product_selection', 10 ); // after images

		// Place title, rating and price above the content
		// add_action( 'woocommerce_before_single_product_summary', __CLASS__ . '::changing_the_head_of_single_products', 5 );
		add_action( 'woocommerce_before_add_to_cart_button', 'woocommerce_template_single_price', 5 );

		// Move the SKU and meta to the top of the summary box
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 20 );

		add_filter( 'woocommerce_product_tabs', __CLASS__ . '::woo_rename_tabs', 1, 100 );
		add_filter( 'woocommerce_product_description_heading', __CLASS__ . '::update_description_tab_titles', 1, 10 );

		// grey out variations not in stock
		add_filter( 'woocommerce_variation_is_active', __CLASS__ . '::grey_out_variations_when_out_of_stock', 10, 2 );

		/***************************************************************/
		/************************* Cart Table **************************/
		/***************************************************************/

		// Update cart with AJAX when products are added
		add_filter( 'woocommerce_add_to_cart_fragments', __CLASS__ . '::woocommerce_header_add_to_cart_fragment' );
		add_action( 'woocommerce_before_cart_table', __CLASS__ . '::before_cart_table', 10 );

		// Checkout page needs some wrapping.
		add_action( 'woocommerce_checkout_after_customer_details', __CLASS__ . '::checkout_review_wrapper' ); // wrap
		add_action( 'woocommerce_checkout_after_order_review', __CLASS__ . '::wrapper_loop_end' ); // close

		// Remove the terms and conditions from the cart and say you agree anyway.
		add_filter( 'woocommerce_checkout_show_terms', __CLASS__ . '::filter_woocommerce_checkout_show_terms', 10, 1 );
		add_action( 'woocommerce_review_order_before_submit', __CLASS__ . '::new_term_agreement', 10 );

		add_filter( 'woocommerce_cart_item_name', __CLASS__ . '::checkout_item_names_with_variations', 10, 3 );

		// Force products to show their variations in the cart
		add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
		add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );

		// Check for empty-cart get param to clear the cart
		add_action( 'wp_ajax_empty_cart', __CLASS__ .'::woocommerce_clear_cart_url' );
		add_action( 'wp_ajax_nopriv_empty_cart', __CLASS__ .'::woocommerce_clear_cart_url' );
		add_action( 'wp_footer', __CLASS__ .'::ajax_clear_cart' );
		// add_action( 'drivkraft_header_cart_buttons', __CLASS__ . '::add_clear_cart_button', 20 );
	}

	static function ajax_clear_cart() {
	 echo'
	 <script>
			jQuery( function( $ ) {
				$( ".button--clear" ).click( function( e ) {
					e.preventDefault();
					$.post( "' . admin_url( 'admin-ajax.php' ) . '", { action: "empty_cart" }, function() {
						$( ".counted" ).html( "0" );
						location.reload();
					} );
				} );
			} );
		</script>
		';
	}
	static function woocommerce_clear_cart_url() {
		WC()->cart->empty_cart();
		wp_send_json( 'ok' );
	}

	static function add_clear_cart_button() {
			echo "<a class='header--cart--btn button button--clear' href='?startover=true'>" . __( 'Empty Cart', 'drivkraft-theme' ) . "</a>";
	}

	/**
	 * Make klarna show the get_name instead of the get_title - EXT-427
	 * @param  string $name
	 * @param  array $item
	 * @param  string $key
	 * @return string
	 */
	static function checkout_item_names_with_variations( $name, $item, $key ) {
		$_product = $item['data'];
		if ( ! $_product->is_visible() ) {
			return $_product->get_name();
		}
		return sprintf( '<a href="%s">%s </a>', $_product->get_permalink( $item ), $_product->get_name() );
	}

	// Set the woocommerce images to these sizes whenever you activate Craft Theme
	static function craft_set_woocommerce_image_sizes() {
		global $pagenow;

		if ( ! isset( $_GET['activated'] ) || 'themes.php' != $pagenow ) {
			return;
		}

		$catalog = [
			'width'   => '350', // px
			'height'  => '350', // px
			'crop'    => 0,      // false
		];

		$single = [
			'width'   => '1024',// px
			'height'  => '768', // px
			'crop'    => 0,      // false
		];

		$thumbnail = [
			'width'   => '240', // px
			'height'  => '240', // px
			'crop'    => 0,      // false
		];

		// Image sizes
		update_option( 'shop_catalog_image_size', $catalog );     // Product category thumbs
		update_option( 'shop_single_image_size', $single );       // Single product image
		update_option( 'shop_thumbnail_image_size', $thumbnail ); // Image gallery thumbs
	}


	// Remove terms and conditions
	static function filter_woocommerce_checkout_show_terms( $true ) {
		return false;
	}

	// Replace terms
	static function new_term_agreement() {
		if ( ! wc_get_page_id( 'terms' ) > 0 ) {
			return;
		}

		do_action( 'woocommerce_checkout_before_terms_and_conditions' ); ?>
		<p class="form-row terms wc-terms-and-conditions">
			<?php printf( __( 'by placing an order at %s, you agree to our <a href="%s" target="_blank">terms &amp; conditions</a>', 'drivkraft-theme' ), get_bloginfo( 'name' ), esc_url( wc_get_page_permalink( 'terms' ) ) ); ?>
			<input type="hidden" name="terms-field" value="0" />
		</p>
		<?php do_action( 'woocommerce_checkout_after_terms_and_conditions' );
	}


	static function image_container() {
		echo '<div class="image--container">';
	}

	static function changing_the_head_of_single_products() {
		get_template_part( 'includes/woocommerce', 'title' );
	}

	static function changing_the_head_of_archive_category() {
		if ( ! is_product_category() ) {
			return;
		}
		global $wp_query;
		$parent_name    = $wp_query->queried_object;
		$product_cat_id = $parent_name->term_id;
		$args = [
			'hierarchical'     => 1,
			'show_option_none' => '',
			'hide_empty'       => 0,
			'parent'           => $product_cat_id,
			'taxonomy'         => 'product_cat',
		];
		$children = get_categories( $args );
		if ( ! $children ) {
			get_template_part( 'includes/woocommerce', 'archive-description' );
		} else {
			include( locate_template( 'includes/woocommerce-parent-archive-description.php' ) );
		}
	}

	static function drivkraft__filter_bar() {
		if ( ! is_product_category() ) {
			return;
		}
		get_template_part( 'includes/woocommerce', 'filter-bar' );
	}

	static function drivkraft__extended_filter() {
		if ( ! is_product_category() ) {
			return;
		}
		get_template_part( 'includes/woocommerce', 'extended-filter' );
	}


	/* Before the table on the cart page, we need a title and a cart button */
	static function before_cart_table() {
		global $woocommerce; ?>
		<div class="cart--header">
			<h1 class="cart--header--title"><span class="cart--count"><?php echo WC()->cart->get_cart_contents_count() ?></span><?php the_title(); ?></h1>
			<div class="wc-proceed-to-checkout">
				<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
			</div>
		</div>
	<?php }

	/* When you add a product to the cart, we update the value of the cart in the header */
	static function woocommerce_header_add_to_cart_fragment( $fragments ) {
		global $woocommerce;
		ob_start();
			echo '<div class="header--cart--subtotal">';
			echo '<span class="header--cart--sub">' . __( 'Subtotal', 'drivkraft-theme' ) . '</span>'; // ? Linting error...
			echo '<span class="header--cart--sub">' . $woocommerce->cart->get_cart_total() . '</span>';
			echo '</div>';
		$fragments['.header--cart--subtotal'] = ob_get_clean();

		ob_start();
		get_template_part( 'includes/header-cart--contents' );
		$fragments['.header--cart--contents'] = ob_get_clean();
		return $fragments;
	}

	// Get missing image file from tempalte customisation
	static function missing_image_replacement( $src ) {
		$image = get_theme_mod( 'no_image_available' );
		if ( empty( $image ) ) {
			return $src;
		}
		$src = $image;
		return $src;
	}

	/* Before woocommerce content */
	static function wrapper_start() {
		echo '<div id="content" class="content--container cf w">';
			echo '<div id="main" class="content--woo cf">';
	}

	/* After woocommerce content */
	static function wrapper_end() {
		echo '</div>';
	}

	static function breadcrumb_parent() {
		echo '<div id="trail" class="breadcrumbs__parent">';
	}

	/* Before woocommerce content */
	static function wrapper_loop_start() {
		// Class based on content
		$class = ( is_product_category() || is_shop() ) ? 'filter--container' : 'content--before';

		// Do we allow sidebars through customiser?
		$product_count    = get_theme_mod( 'product_row_count', 4 ); // Default of 4 before changed
		$shop_has_sidebar = get_option( 'shop_has_sidebar' );

		// If sidebars are active for theme
		if ( $shop_has_sidebar ) {
			$class = $class . ' there-is-a-sidebar';
		} else {
			$class = $class . ' there-is-no-sidebar';
		}

		// If product count is active for theme
		if ( $product_count ) {
			$class = $class . ' product-columns-' . $product_count;
		}

		// Output the container with classes
		echo '<div class="' . $class . '">';
	}

	/* After woocommerce content */
	static function wrapper_loop_end() {
		self::wrapper_end();
	}

	/* After woocommerce content */
	static function wrapper_page_end( $name ) {
		if ( 'shop' !== $name ) {
			return;
		}
		echo '</div>';
	}

	/* Before woocommerce content */
	static function checkout_review_wrapper() {
		echo '<div id="review-items-in-cart" class="review--container cf">';
	}

	/* Show the stock of the products */
	static function show_shop_inventory() {
		global $product;
		if ( $product->is_in_stock() ) {
				echo '<span class="stock in-stock">' . $product->get_stock_quantity() . ' ' . __( 'in stock', 'drivkraft-theme' ) . '</span>';
		} else {
				echo '<span class="stock out-stock">' . __( 'Out of stock', 'drivkraft-theme' ) . '</span>';
		}
	}

	// remove the title on the shop archive, but leave the normal titles
	static function remove_shop_title( $page_title ) {
		if ( 'Shop' === $page_title ) {
			return false;
		}
	}

	// Remove the sidebar on the shop
	static function remove_sidebar_shop() {
		if ( ! is_product() ) {
			return;
		}
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar' );
	}

	/* Payment icon next to the single product price */
	static function drivkraft_payment_information() {
		if ( ! is_product() ) {
			return;
		}

		$logo = get_theme_mod( 'single_product_payment_icon' );
		if ( ! $logo ) {
			return;
		}
		echo '<div class="single-product__header--payment" >';
			echo '<img src="'. $logo . '" title="' . get_bloginfo( 'name' ) . '" alt="' . get_bloginfo( 'name' ) . '"/>';
		echo '</div>';
	}

	/*****************************************************************************/
	/************************ Single Product Changes *****************************/
	/*****************************************************************************/

	// Places script in footer for Magnify on single product pages
	// use this in child theme: add_action( 'wp_footer', 'drivkraft__Woocommerce::maginify_product_image' );
	static function maginify_product_image() {
		if ( ! is_product() ) {
			return;
		}
	?>
			<script>
				jQuery( document ).ready( function( $ ) {
					$( window ).on( 'load', function(){
						// Get the main image and apply magnify
						var image = $( '.size-shop_single' );
						image.magnify();
						// When the variation changes though, so do we...
						$( '.variations_form' )
							// When varation is selected
							.on( 'woocommerce_variation_has_changed', function(){
								image.magnify();
							})
							// When we reset it all
							.on( 'click', '.reset_variations', function( ) {
								image.magnify();
							});
					});
				});
			</script>
		<?php
	}


	// Rename tabs with conditionals
	static function woo_rename_tabs( $tabs ) {
		global $product, $post;

		// Description tab
		if ( $post->post_content ) {
			$tabs['description']['title'] = __( 'Details', 'drivkraft-theme' );   // Rename the description tab
		}

		// If there are reviews, we can rename the tab
		$id = $product->get_id();
		if ( comments_open( $id ) ) {
			$tabs['reviews']['title'] = __( 'Reviews', 'drivkraft-theme' );        // Rename the reviews tab
		}

		// If there's an attribute available, we can rename the attributes tab
		if ( $product->has_attributes() || $product->has_dimensions() || $product->has_weight() ) { // Check if product has attributes, dimensions or weight
			$tabs['additional_information']['title'] = __( 'Specifications', 'drivkraft-theme' );  // Rename the additional information tab
		}

		// Return tabs
		return $tabs;
	}

	/* Change the tabs*/
	static function update_description_tab_titles( $title ) {
		// woocommerce_product_description_heading::update_description_tab_titles
		return;
	}

	/* Output the category image wrapped*/
	static function woocommerce_category_image( $size = 'large' ) {
		if ( ! is_product_category() ) {
			return;
		}

		global $wp_query;
		$cat = $wp_query->get_queried_object();
		$thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
		$image = wp_get_attachment_image_url( $thumbnail_id, $size );
		if ( $image ) {
			// Return image rather than echo, we need to be able to use this as a background
			return $image;
		} else {
			// No image? Nothing to output...
			return false;
		}
	}

	// Grey out variations
	static function grey_out_variations_when_out_of_stock( $grey_out, $variation ) {
		if ( ! $variation->is_in_stock() ) {
			return false;
		}
		return true;
	}

	/* Variations to Radio / Images */
	static function get_any_matching_variation( $variations, $attribute, $attribute_value ) {
		$grouped_variations = self::group_variations( $variations );
		$variation = false;
		$key = 'attribute_'. strtolower( $attribute );
		if ( ! isset( $grouped_variations[ $key ] ) ) {
			return false;
		}
		if ( ! isset( $grouped_variations[ $key ][ $attribute_value ] ) ) {
			return false;
		}
		// Get the first match
		$variation = reset( $grouped_variations[ $key ][ $attribute_value ] );
		return $variation;
	}

	static function group_variations( $variations ) {
		$grouped_variations = [];
		foreach ( $variations as $variation ) {
			foreach ( $variation['attributes'] as $attribute => $attribute_value ) {
				// Group first by the attribute, and then the attribute value
				// Eg. [Colour][Red] = $variation
				if ( ! isset( $grouped_variations[ $attribute ] ) ) {
					$grouped_variations[ $attribute ] = [];
				}
				if ( ! isset( $grouped_variations[ $attribute ][ $attribute_value ] ) ) {
					$grouped_variations[ $attribute ][ $attribute_value ] = [];
				}
				$grouped_variations[ $attribute ][ $attribute_value ][] = $variation;
			}
		}
		return $grouped_variations;
	}


	// Show percentage instead of sale
	//add_filter( 'woocommerce_sale_flash', 'drivkraft__Woocommerce::percentage_sales_flash', 10, 2 );
	static function percentage_sales_flash() {
		global $product;
		/* Simple product */
		if ( $product->is_type( 'simple' ) ) {
			$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
		} /* Variable product */
		elseif ( $product->is_type( 'variable' ) ) {
			// Regular Price
			$prices = [
				$product->get_variation_price( 'min', true ),
				$product->get_variation_price( 'max', true ),
			];
			$saleprice = $prices[0] ;

			// Sale Price
			$prices = [
				$product->get_variation_regular_price( 'min', true ),
				$product->get_variation_regular_price( 'max', true ),
			];
			sort( $prices );
			$price = $prices[0];
			$percentage = round( ( ( $price - $saleprice ) / $price ) * 100 );
		}
		return '<span class="onsale percentage">'. sprintf( __( ' Save %s', 'drivkraft-theme' ), $percentage . '%' ).'</span>';
	}

	/* Output the vairation images */
	static function variation_images_on_single_product_selection() {
		if ( ! is_product() ) {
			return;
		}

		global $product;
		$attributes = $product->get_variation_attributes();

		$variations = $product->get_available_variations();
		if ( ! $variations ) {
			return;
		}
		echo '<div class="visual--variation--container">';
		$attribute = 'Color';
		$attribute_values = $attributes[ $attribute ];
		foreach ( $attribute_values as $attribute_value ) {
			$variation = self::get_any_matching_variation( $variations, $attribute, $attribute_value );
			$key = 'attribute_'. strtolower( $attribute );
			?>
				<div class="visual--variation <?php echo $key; ?>">

					<label  for="variation_<?php echo $variation['variation_id']; ?>"
									class="visual--variation--label"
									data-variation-id="<?php echo $variation['variation_id']; ?>">

						<input  type="radio"
										class="visual--variation--radio"
										data-variation-id="<?php echo $variation['variation_id']; ?>"/>

						<img  width="70"
									data-attribute_name="<?php echo $key; ?>"
									data-attribute_value="<?php echo $attribute_value; ?>"
									alt="<?php echo $attribute_value; ?>"
									title="<?php echo $variation['image_title']; ?>"
									src="<?php echo $variation['image_src']; ?>" />
					</label>
				</div>
			<?php
		}
		echo '</div>';
		/* Trigger the change with javascript. Alternatively, use a href to the variation */
		?>
		<script type="text/javascript">
			jQuery( document ).ready( function( $ ) {
				$( '.visual--variation' ).each( function() {
					// Get attributes
					var attr = $( this ).find( 'img' ).attr( "data-attribute_name" );
					var value = $( this ).find( 'img' ).attr( "data-attribute_value" );
					// If we have elements that related
					// to a select, hide that select box
					$( 'select[name="' + attr + '"]' )
						.closest( 'tr' )
						.addClass( 'visual--enabled' );
						// .hide();
					// When we click the new images, we should
					// automatically select that option in the select
					$( this ).on( "click", function() {
						$( 'select[name="' + attr + '"] option[value="' + value + '"]' )
							.attr( 'selected', 'selected')
							.trigger( 'change' );
					});
				});
			});
		</script>
		<?php
	}

	/* LOGIN FORM PAGE*/

	static function wrap_single_login_form() {
		if ( 'no' === get_option( 'woocommerce_enable_myaccount_registration' ) ) :
			echo '<div class="login-form--container">';
		endif;
	}
	// Close the login form if no registration form
	static function close_wrap_single_login_form() {
		if ( 'no' === get_option( 'woocommerce_enable_myaccount_registration' ) ) :
			echo '</div>';
		endif;
	}


	// Shipping table cells need to be merged
	static function better_shipping_on_cart() {
		if ( is_cart() || is_checkout() ) {
		?>
		<script>
			jQuery( function( $ ) {

				function move_shipping_title(){
					var shipping_title = $( 'tr.shipping th' );
					var clone = shipping_title.clone();
					$( 'tr.shipping th' ).remove();
					clone.addClass( 'shipping-title' );
					$( '#shipping_method' ).prepend( clone );
				}

				function merge_shipping_cells(){
					$( 'tr.shipping td' ).attr( 'colspan', '2' );
				}

				// Move the title once
				move_shipping_title();
				// Merge the cells
				merge_shipping_cells();

				// Remerge the cells every time shipping changes
				$( document ).on( 'updated_shipping_method updated_checkout', function() {
					merge_shipping_cells();
				} );
			});
		</script>
	<?php } }



	static function site_wide_message_script() {
		if ( ! is_store_notice_showing() ) {
			return;
		}
	?>
	<script>
		// There is a notice, we must push the head down!
		jQuery( function( $ ) {
			var h = $( '.demo_store' ).outerHeight();
			$('body.woocommerce-demo-store').css({
				'padding-top' : h,
				'position' : 'relative'
			});
		});
	</script>
	<?php }
}

/* Plugable woocommerce functions */

// // Woocommerce checkout login form. Needs to be wrapped.
// function woocommerce_checkout_login_form() {
//   echo '<div class="wrapped--login">';
//   wc_get_template( 'checkout/form-login.php', array( 'checkout' => WC()->checkout() ) );
//   echo '</div>';
// }

// // Woocommerce checkout coupon codes. Needs to be wrapped.
// function woocommerce_checkout_coupon_form() {
//   echo '<div class="wrapped--coupon">';
//   wc_get_template( 'checkout/form-coupon.php', array( 'checkout' => WC()->checkout() ) );
//   echo '</div>';
// }

// Call the woocommerce class

drivkraft__Woocommerce::woocommerce_support();
