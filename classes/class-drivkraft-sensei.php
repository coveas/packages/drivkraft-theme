<?php


class drivkraft_sensei {

  use drivkraft_srcy;

  static function setup() {
    add_theme_support( 'sensei' );
    add_action( 'wp_enqueue_scripts', __CLASS__ .'::style', 100 );
  }

  static function style() {
    if ( is_admin() ) {
      return;
    }
    $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
    wp_register_style( 'drivkraft-sensei', self::scry( "/assets/css/sensei$suffix.css" ), array(), '', 'all' );
    wp_enqueue_style( 'drivkraft-sensei' );
  }

}

drivkraft_sensei::setup();
