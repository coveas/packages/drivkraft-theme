<?php
class drivkraft_woocommerce_emails {

  static function setup() {
    add_filter( 'woocommerce_email_styles', __CLASS__ . '::drivkraft_woocommerce_email_styles' );
    add_action( 'woocommerce_email_before_order_table', __CLASS__ . '::before_order_email', 10, 4 );
    add_action( 'woocommerce_email_after_order_table', __CLASS__ . '::after_order_email', 10, 4 );
  }

  /* Add css to your emails */
  static function drivkraft_woocommerce_email_styles( $css ) {
    // Remove CSS set by woocommerce
    // $css = '';
    // Pull in our stylesheet ompiled with `grunt email`
    ob_start();

    $parent_styles = get_template_directory() . '/assets/css/email.css';
    $child_styles  = get_stylesheet_directory() . '/assets/css/email.css';

    if ( is_file( $parent_styles ) ) { include( $parent_styles ); }
    if ( is_file( $child_styles ) ) { include( $child_styles ); }

    // Load css back into variable
    $css .= ob_get_clean();

    // Return css for inlining
    return $css;
  }

  /* Change the content before & after your order table */

  // Add content before the order table
  static function before_order_email( $order, $sent_to_admin, $plain_text, $email ) {
    // If the emails links to the dashboard
    if ( $sent_to_admin ) {
      // email is plain text, not html
      if ( ! $plain_text ) {
        // HTML content here
      } else {
        // Plain text only
      }
    }
  }

  // Add content after the order table
  static function after_order_email(  $order, $sent_to_admin, $plain_text, $email ) {
    // If the emails links to the dashboard
    if ( $sent_to_admin ) {
      // email is plain text, not html
      if ( ! $plain_text ) {
        // HTML content here
      } else {
        // Plain text only
      }
    }
  }
}

drivkraft_woocommerce_emails::setup();