<?php
/* Canvas elements need wrapping in
order to play with the body element.

You can't move the body and overflow
it at the same damn time. */

class drivkraft_canvas_element {

  static function setup() {
    add_action( 'drivkraft_after_body_tag', __CLASS__ . '::canvas_wrapper_open' );
    add_action( 'drivkraft_before_body_tag_close', __CLASS__ . '::canvas_wrapper_close' );
  }

  static function canvas_wrapper_open() {
    echo '<!-- fn.canvas.open--><div class="canvas cf">';
  }
  static function canvas_wrapper_close() {
    echo '</div><!-- fn.canvas.close -->';
  }
}

drivkraft_canvas_element::setup();
