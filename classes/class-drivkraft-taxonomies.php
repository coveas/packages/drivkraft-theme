<?php
class drivkraft_taxonomies {
  // Nope. brand is still a taxonomy
  // aha
  static function setup() {
    add_action( 'init', __CLASS__ . '::product_brand', 15 );
    add_shortcode( 'brands', __CLASS__ . '::list_brands_by_letter', 1 );
  }

  /**
   * Product Brand
   * hooked on 'init'
   */
  static function product_brand() {
    $labels = array(
      'name'                       => _x( 'Brands', 'Taxonomy General Name', 'drivkraft-theme' ),
      'singular_name'              => _x( 'Brand', 'Taxonomy Singular Name', 'drivkraft-theme' ),
      'menu_name'                  => __( 'Brand', 'drivkraft-theme' ),
      'all_items'                  => __( 'All Brands', 'drivkraft-theme' ),
      'parent_item'                => __( 'Parent Brand', 'drivkraft-theme' ),
      'parent_item_colon'          => __( 'Parent Brand:', 'drivkraft-theme' ),
      'new_item_name'              => __( 'New Brand Name', 'drivkraft-theme' ),
      'add_new_item'               => __( 'Add New Brand', 'drivkraft-theme' ),
      'edit_item'                  => __( 'Edit Brand', 'drivkraft-theme' ),
      'update_item'                => __( 'Update Brand', 'drivkraft-theme' ),
      'view_item'                  => __( 'View Brand', 'drivkraft-theme' ),
      'separate_items_with_commas' => __( 'Separate brands with commas', 'drivkraft-theme' ),
      'add_or_remove_items'        => __( 'Add or remove brands', 'drivkraft-theme' ),
      'choose_from_most_used'      => __( 'Choose from the most used', 'drivkraft-theme' ),
      'popular_items'              => __( 'Popular Brands', 'drivkraft-theme' ),
      'search_items'               => __( 'Search Brands', 'drivkraft-theme' ),
      'not_found'                  => __( 'Not Found', 'drivkraft-theme' ),
    );

    $rewrite = array(
      'slug'         => _x( 'brand', 'Taxonomy slug', 'drivkraft-theme' ),
      'with_front'   => true,
      'hierarchical' => false,
    );

    $capabilities = array(
      'manage_terms' => 'manage_product_terms',
      'edit_terms'   => 'edit_product_terms',
      'delete_terms' => 'delete_product_terms',
      'assign_terms' => 'assign_product_terms',
    );

    $args = array(
      'labels'            => $labels,
      'hierarchical'      => true,
      'public'            => true,
      'show_ui'           => true,
      'query_var'         => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'show_tagcloud'     => true,
      'rewrite'           => $rewrite,
      'capabilities'      => $capabilities,
    );

    register_taxonomy( 'brand', array( 'product' ), $args );
  }

  // Shortcode function to output a list of the brands
  static function list_brands_by_letter( $atts ) {

    $atts = shortcode_atts(
      [ 'title' => __( 'Our Brands', 'drivkraft-theme' ) ],
    $atts, 'brands' );

    $args     = [ 'hide_empty' => false ]; // Hide brands with no products
    $taxonomy = 'brand';
    $tags     = get_terms( $taxonomy, $args );
    $count    = wp_count_terms( $taxonomy, $args );
    $list     = '';
    $groups   = [];

    if ( $tags && is_array( $tags ) ) {
      // Group tags by their first letter
      foreach ( $tags as $tag ) {
        $first_letter = mb_substr( $tag->name, 0, 1, 'utf-8' );
        $first_letter = mb_strtoupper( $first_letter, 'UTF-8' );
        $groups[ $first_letter ][] = $tag;
      }

      // If we have groups available...
      if ( ! empty( $groups ) ) {
        $list .= '<div class="brand__aphabet cf">';
          $list .= '<div class="brand__aphabet--heading cf">';
            $list .= '<h2 class="brand--title">' . $atts['title'] . '</h2>';
            $list .= '<span class="brand--count">' . sprintf( __( 'Showing %d brands', 'drivkraft-theme' ) , $count ) . '</span>';
          $list .= '</div>';
        // List each letter and their tags
        foreach ( $groups as $letter => $tags ) {
          $list .= '<ul class="brand__group">';
          $title = apply_filters( 'the_title', $letter );
          if ( is_numeric( $title ) ) { $title = '123'; }
          $list .= '<li class="brand__group--letter letter__is__' . $title . '">' . $title . '</li>';
          foreach ( $tags as $tag ) {
            $name = apply_filters( 'the_title', $tag->name );
            $link = get_term_link( $tag, 'brand' );
            $list .= '<li class="brand__group--item">';
              $list .= '<a href="' . $link . '" class="term-name">' . $name . '</a>';
            $list .= '</li>';
          }
          $list .= '</ul>';
        }
        $list .= '';
        $list .= '</div>';
        $list .= '<script>jQuery(function($){ $( ".brand__group" ).equalise(); });</script>';
      }
    } else {
      // If there are no brands, we need to output a message saying sorry
      $list .= '<div class="brand--empty">' . __( 'No brands are available', 'Drivkraft_theme' ) . '</div>';
    }

    return $list;
  }
}

drivkraft_taxonomies::setup();
