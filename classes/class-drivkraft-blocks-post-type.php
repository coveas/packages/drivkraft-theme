<?php


class drivkraft_blocks_post_type {

  static function setup() {
    add_action( 'init', __CLASS__ . '::blocks' );
  }

  static function blocks() {
    $labels = array(
      'name'                  => _x( 'Blocks', 'Post Type General Name', 'drivkraft-theme' ),
      'singular_name'         => _x( 'Block', 'Post Type Singular Name', 'drivkraft-theme' ),
      'menu_name'             => __( 'Blocks', 'drivkraft-theme' ),
      'name_admin_bar'        => __( 'Block', 'drivkraft-theme' ),
      'archives'              => __( 'Block Archives', 'drivkraft-theme' ),
      'parent_item_colon'     => __( 'Parent Block:', 'drivkraft-theme' ),
      'all_items'             => __( 'All Blocks', 'drivkraft-theme' ),
      'add_new_item'          => __( 'Add New Block', 'drivkraft-theme' ),
      'add_new'               => __( 'Add New', 'drivkraft-theme' ),
      'new_item'              => __( 'New Block', 'drivkraft-theme' ),
      'edit_item'             => __( 'Edit Block', 'drivkraft-theme' ),
      'update_item'           => __( 'Update Block', 'drivkraft-theme' ),
      'view_item'             => __( 'View Block', 'drivkraft-theme' ),
      'search_items'          => __( 'Search Blocks', 'drivkraft-theme' ),
      'not_found'             => __( 'Not found', 'drivkraft-theme' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'drivkraft-theme' ),
      'featured_image'        => __( 'Featured Image', 'drivkraft-theme' ),
      'set_featured_image'    => __( 'Set featured image', 'drivkraft-theme' ),
      'remove_featured_image' => __( 'Remove featured image', 'drivkraft-theme' ),
      'use_featured_image'    => __( 'Use as featured image', 'drivkraft-theme' ),
      'insert_into_item'      => __( 'Insert into Block', 'drivkraft-theme' ),
      'uploaded_to_this_item' => __( 'Uploaded to this Block', 'drivkraft-theme' ),
      'items_list'            => __( 'Block list', 'drivkraft-theme' ),
      'items_list_navigation' => __( 'Block list navigation', 'drivkraft-theme' ),
      'filter_items_list'     => __( 'Filter Block list', 'drivkraft-theme' ),
      );

    $args = array(
      'label'                 => __( 'Block', 'drivkraft-theme' ),
      'description'           => __( 'Repeatable content blocks', 'drivkraft-theme' ),
      'labels'                => $labels,
      'supports'              => [ 'title' ],
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 10,
      'menu_icon'             => 'dashicons-clipboard',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,
      'exclude_from_search'   => true,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
      );

    register_post_type( 'blocks', $args );
  }
}

drivkraft_blocks_post_type::setup();