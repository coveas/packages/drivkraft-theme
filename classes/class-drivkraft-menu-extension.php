<?php

/**
 * The navigation in craft can be extended.
 * This will extend it to the point where
 * you're able to include products and banners
 * within the header.
 * Known bug: It duplicates the menu items.
 */

class craft_menu_extension {

  /**
   * Setup the hooks needed to create content
   * & assign it to the correct menu item
   */
  static function setup() {
    add_filter( 'acf/load_field/name=which_menu_item', __CLASS__ . '::load_menu_items_into_select', 1, 10 );
    add_action( 'mega_menu_sub_menu_compile', __CLASS__ . '::sub_menu_builder', 20 );
    add_action( 'mega_menu_after_sub_menu', __CLASS__ . '::sub_menu_output', 20 );
  }

  /**
   * Match menu items to sub menu items to display content
   * @param  [type] $name [Menu title / name of the menu selected]
   * @return [type]       [Output the HTML for this sub secton]
   */
  static function sub_menu_output( $name ) {
    global $craft_sub_menus;

    if ( isset( $craft_sub_menus[ $name ] ) ) {
      // Wrap this[ in a <div>
      echo '<div class="sub-menu--container sub-menu--extension">';
      echo $craft_sub_menus[ $name ];
      echo '</div>';
    }
  }


  /**
   * Load Organised menus into the subcategory builder
   * @param  [type] $field [Select box created in ACF admin]
   * @return [type]        [List of menu items assigned to organised menu items]
   */
  static function load_menu_items_into_select( $field ) {
    $field['choices'] = [];

    if ( have_rows( 'menu_item', 'option' ) ) {
      while ( have_rows( 'menu_item', 'option' ) ) {
        the_row();
        if ( get_row_layout() == 'organised_menu' ) {
          $menu_title = get_sub_field( 'menu_title', false );
          $field['choices'][ $menu_title ] = '- ' . $menu_title;
        }

        // *********************************************************
        // Only organised categories have enough room for this
        // kind of experimental feature. Please be concious
        // that enabling more than this option will cause styling
        // issues and weeks of headaches.
        // *********************************************************
        // /* Load the menu items name so we can match it */
        // else if ( get_row_layout() == 'product_category' ) {
        //   $product_category = get_sub_field( 'menu_item_name', false );
        //   $field['choices'][ $product_category ] = '- ' . $product_category;
        // }
        //
        // /* Load the menu items name so we can match it */
        // else if ( get_row_layout() == 'sale_items' ) {
        //   $sale_name = get_sub_field( 'sale_name', false );
        //   $field['choices'][ $sale_name ] = '- ' . $sale_name;
        // }
        //
        // /* Load the menu items name so we can match it */
        // else if ( get_row_layout() == 'page' ) {
        //   $page_name = get_sub_field( 'page_name', false );
        //   $field['choices'][ $page_name ] = '- ' . $page_name;
        // } /* Load the menu items name so we can match it */
      }
    }

    // Put the choices into the menu loader
    return $field;
  }

  /**
   * Create an array of sub menus to assign to
   * @param  [type] $name [Menu title]
   * @return [type]       [Assign HTML of selected content to array]
   */
  static function sub_menu_builder( $name ) {
    global $craft_sub_menus;
    $craft_sub_menus = [];

    // Start looping through additional sub menu items
    if ( have_rows( 'show_addition_content', 'option' ) ) {
      while ( have_rows( 'show_addition_content', 'option' ) ) {
        the_row();

        // Get the name field selected from a select box of the menu names
        $selected_menu_item = get_sub_field( 'which_menu_item' );

        // If there's no selected menu name, or the menu name doesn't match
        // what we're trying to target, then return;
        if ( ! $selected_menu_item ) {
          continue;
        }

        // If products is selected
        if ( 'products' === get_row_layout() ) {

          $selected_products = get_sub_field( 'select_products_to_show' );

          if ( empty( $selected_products ) ) {
            continue;
          }

          // Nice old loop for two products.
          $query_args = [
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'post__in'            => $selected_products,
            'ignore_sticky_posts' => 1,
            'posts_per_page'      => -1,
            'meta_query'          => WC()->query->get_meta_query(),
          ];
          $products = new WP_Query( $query_args );
          ob_start();

          // Title
          echo '<h3 class="sub-menu--extension--title">' . __( 'Featured products', 'drivkraft-theme' ) . '</h3>';

          if ( $products->have_posts() ) {
            woocommerce_product_loop_start();
            while ( $products->have_posts() ) : $products->the_post();
              wc_get_template_part( 'content', 'product' );
            endwhile; // end of the loop.
            woocommerce_product_loop_end();
          }
          woocommerce_reset_loop();
          wp_reset_postdata();

          $craft_sub_menus[ $selected_menu_item ] = '<div class="sub-menu--products products">' . ob_get_clean() . '</div>';
        } // -->

        /*****/

        // If banner is selected
        if ( 'banner' === get_row_layout() ) {
          $banner_id = get_sub_field( 'select_banner' );

          if ( ! $banner_id ) {
            continue;
          }

          // Set post

          ob_start();

          if ( $banner_id ) {
            $unique_id = 'bnr_' . uniqid();
          ?>
          <div class="billboard billboard-singles cf">
            <div id="<?php echo $unique_id; ?>" class="billboard--container w cf">
              <ul>
                <?php
                  $banner      = $banner_id;
                  $identifier  = 'banner-' . $banner;
                  $banner_link = '#';
                  $acf_fields  = [
                    'banner_link',
                    'link_to_category',
                    'link_to_external_link',
                    'banner_link_category',
                    'external_url',
                    'desktop_image',
                    'desktop_line_1',
                    'desktop_line_2',
                  ];
                  foreach ( $acf_fields as $key ) {
                    @$$key = get_field( $key, $banner );
                  }

                  if ( $banner_link ) {
                    $link = $banner_link;
                  } else {
                    $link = '#';
                  }

                  // If there's a category instead of a link
                  // we need to loop through the array to get the
                  // term information
                  if ( $link_to_category && $banner_link_category ) {
                    foreach ( $banner_link_category as $term ) :
                      $link = get_term_link( $term );
                    endforeach;
                  }

                  // If we're going for an external link though, lets do that.
                  if ( $link_to_external_link && $external_url ){
                    $link = $external_url;
                  }

                  ?>
                <li class="cf banner--object <?php echo $identifier?>">
                  <object>
                    <a class="banner--object--container" href="<?php echo $link; ?>">
                        <?php
                        // If there's a desktop image
                        $media__desktop = wp_get_attachment_image_src( $desktop_image, 'medium_large' );
                        if ( $media__desktop ) {
                          echo '<img src="' . $media__desktop[0] . '">';
                        }
                        if ( $desktop_line_1 || $desktop_line_2 ) { ?>
                        <div class="banner--object--d--title">
                          <span class="banner--object--d--title--top"><?php echo $desktop_line_1; ?></span>
                          <span class="banner--object--d--title--bottom"><?php echo $desktop_line_2; ?></span>
                          <?php do_action( 'drivkraft_banners_after_title', $identifier ); ?>
                        </div>
                      <?php } ?>
                    </a>
                  </object>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
          <?php
          $craft_sub_menus[ $selected_menu_item ] = '<div class="sub-menu--banner">' . ob_get_clean() . '</div>';
          wp_reset_postdata();
        } // -->

      }
    }
  }
}

craft_menu_extension::setup();
