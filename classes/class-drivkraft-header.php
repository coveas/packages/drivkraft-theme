<?php

/* How about everything in the header */

class drivkraft_header {

  static function setup() {

    // Top line of navigation
    add_action( 'drivkraft_header_top_left', __CLASS__ . '::header_top_left', 10 );
    add_action( 'drivkraft_header_top_center', __CLASS__ . '::header_top_center', 10 );
    add_action( 'drivkraft_header_top_right', __CLASS__ . '::header_top_right', 10 );
    add_action( 'drivkraft_header_logo', __CLASS__ . '::site_logo', 10 );

    // Bottom line of navigation
    add_action( 'drivkraft_header_bottom_left', __CLASS__ . '::header_bottom_left', 10 );

    // Navigation usually
    add_action( 'drivkraft_header_bottom_center', __CLASS__ . '::header_bottom_center', 10 );
    add_action( 'wp_footer', __CLASS__ . '::navigation_script_loading', 10 );

    // Cart usually
    add_action( 'drivkraft_header_bottom_right', __CLASS__ . '::header_bottom_right', 10 );
    add_action( 'wp_footer', __CLASS__ . '::header_cart_script', 10 );

    // header cart message
    add_action( 'drivkraft_empty_cart_message', __CLASS__ . '::empty_cart_message', 10 );
  }

  /* Corrected layout namespaces with functions as fallback */
  static function header_top_left() {
    self::header_usp_items();
  }

  static function header_top_center() {
    self::site_logo();
  }

  static function header_top_right() {
    do_action( 'account_additional_items' );
  }

  static function header_bottom_left() {
    do_action( 'drivkraft_header_search' );
    self::header_search_form();
  }

  /* The main navigation */
  static function header_bottom_center() {
    ?>
   <div class="menu-bar"><div class="mto button-menu"><?php _e( 'Menu', 'drivkraft-theme' );?></div></div>
    <nav class="header--navigation nav cf">
      <div class="top-nav--inner">
        <?php get_template_part( 'includes/mega', 'menu' ); ?>
      </div>
    </nav>
  <?php }

  static function navigation_script_loading() {
    // Include menu script
    get_template_part( 'includes/menu', 'script' );
  }

  /* Header cart, Menu activation and hover scripts */
  static function header_bottom_right() {
    get_template_part( 'includes/header', 'cart' );
  }

  static function header_cart_script() {
    ?>
    <script>
      jQuery( function( $ ) {
        <?php $cart_contains = WC()->cart->get_cart_contents_count();
        if ( 1 > $cart_contains ) { ?>
        $( 'body' ).addClass( 'cart-is-empty' );
        <?php } ?>
        $( '.counted' ).html( '<?php echo $cart_contains;  ?>');

        $( '.header--cart--container' ).on( 'driv_add_to_cart_updated', function() {
          $( 'body' ).removeClass( 'cart-is-empty' );
        } );
      } );
    </script>
  <?php
  }

  /* USP items from Theme Settings > Options */
  static function header_usp_items() {
    get_template_part( 'includes/header', 'usp-items' );
  }

  /* Site logo from Theme Customiser */
  static function site_logo() {
    $logo = get_theme_mod( 'site_logo' );
    if ( ! $logo ) {
      echo '<a class="site-title" href="' . esc_url( home_url( '/' ) ). '" rel="home">' . get_bloginfo( 'name' ) . '</a>';
    } else {
      // If the client has uploaded a logo, we're going to display it here:
      echo '<a class="site-title" href="' . esc_url( home_url( '/' ) ). '" rel="home"><img src="'. $logo . '" title="' . get_bloginfo( 'name' ) . '" alt="' . get_bloginfo( 'name' ) . '"/></a>';
    }
  }

  /* Header search form */
  static function header_search_form() {
    $placeholder = apply_filters( 'craft_search_placeholder', _x( 'Search', 'The placeholder of the search', 'drivkraft-theme' ) ); ?>
    <a id="search_for" class="search--trigger" href="#search_for"><?php _e( 'Search', 'drivkraft-theme' ); ?></a>
    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
        <div class="searchform--inner cf">
          <label for="s" class="screen-reader-text"><?php _e( 'Search for:', 'drivkraft-theme' ); ?></label>
          <input type="search" id="s" name="s" data-swplive="true" value="" placeholder="<?php echo $placeholder; ?>" />
          <?php

          // If relevanssi exists, post_types, else post_type
          if ( class_exists( 'drivAlgolia' ) ) {
              echo apply_filters( 'drivkraft_search_type', '<input type="hidden" name="post_type" value="product" />' );
          } else {
              echo apply_filters( 'drivkraft_search_type', '<input type="hidden" name="post_types" value="product" />' );
          }
          ?>
          <input type="submit" id="searchsubmit" value="<?php _e( 'Search', 'drivkraft-theme' ); ?>">
        </div>
    </form>
    <?php
  }

  static function empty_cart_message() {
  ?>
    <tr class="empty-cart">
      <td>
        <p><?php _e( 'Your cart is empty', 'drivkraft-theme' ); ?></p>
      </td>
    </tr>

    <tr class="empty-cart">
      <td>
        <a class="button empty-cart-button" href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>"><?php _e( 'Shop now', 'drivkraft-theme' ); ?></a>
      </td>
    </tr>

  <?php }
}

drivkraft_header::setup();
