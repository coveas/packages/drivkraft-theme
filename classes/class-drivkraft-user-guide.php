<?php

// add_action( 'wp_dashboard_setup', 'drivkraft_dashboard_widgets::drivkraft_dashboard_widgets' );
class drivkraft_user_guide {
  static function drivkraft_dashboard_widgets() {
      wp_add_dashboard_widget( 'wptutsplus_dashboard_welcome', 'Welcome', 'drivkraft_user_guide::drivkraft_getting_started_guide' );
  }
  static function drivkraft_getting_started_guide() {
    get_template_part( 'includes/user', 'guide' );
  }
}
