<?php

/**
 * drivkraft theme - Post class
 */
class drivkraft__Post {
  static function print_time( $classes ) {
    $format = '<time class="%s" datetime="%s" itemprop="datePublished">%s</time>';
    $datetime = get_the_time('Y-m-d');
    $time = get_the_time( get_option( 'date_format' ) );

    printf( $format, $classes, $datetime, $time );
  }
}

class drivkraft_load {

  /**
   * Output featured image with SVG placeholder
   * @return html
   */
  static function featured_image( $post_id = null, $size = 'medium', $colour = '#f3f3f3' ) {
    global $post;

    // Fallback id if there isn't one
    if ( ! $post_id ) {
      $post_id = $post->id;
    }

    // Last check to fail
    if ( ! $post_id ) {
      return;
    }

    // Fetch the thumbnail ID
    $thumbnail = get_post_thumbnail_id( $post_id );


    // If no thumbnail, load SVG to keep alignment
    if ( ! $thumbnail ) {
      self::fallback_svg_image();
      return;
    }

    // Get the meta for the thumbnail
    $meta = wp_get_attachment_metadata( $thumbnail );
    // Last check to fail
    if ( empty( $meta ) ) {
      self::fallback_svg_image();
      return;
    }

    // Breakdown the attachment to get the ratio, fallback to 66.6666%
    $ratio     = ( $meta ?  100 / ( $meta['width'] / $meta['height'] ) : '66.666' );
    $thumbnail = get_the_post_thumbnail( $post_id, $size );
    ?>
    <div class="featured--thumbnail floating--image">
      <a href="<?= the_permalink( $post_id ); ?>">
        <?= $thumbnail; ?>
        <svg class="svg-placeholder" style="width: 100%; height: 0; padding-bottom: <?= $ratio; ?>%; background-color: <?= $colour; ?>;" viewBox="0 0 100 <?= $ratio; ?>"></svg>
      </a>
    </div>
    <?php
  }

  static function fallback_svg_image() {
    ?>
      <div class="featured--thumbnail no-thumbnail-image">
        <a href="<?= the_permalink(); ?>">
          <svg class="svg-placeholder" style="width: 100%; height: 0; padding-bottom: 66.666%; background-color: <?= $colour; ?>;" viewBox="0 0 100 66.666"></svg>
        </a>
      </div>
    <?php
  }

}