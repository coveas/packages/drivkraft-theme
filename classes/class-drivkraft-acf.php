<?php

class drivkraft_theme_acf_loader {

  static function setup() {
    // add_filter( 'acf/settings/load_json', __CLASS__ . '::drivkraft_theme_acf_load' );
    // add_filter( 'acf/settings/save_json', __CLASS__ . '::drivkraft_theme_acf_save' );
    add_action( 'acf/init', __CLASS__ . '::craft_acf_add_features', 10 );
    add_action( 'admin_bar_menu', __CLASS__ . '::drivkraft_theme_settings_admin_menu', 999 );
    add_filter( 'acf/settings/show_admin', __CLASS__ . '::hide_acf_edit_for_non_admins' );
    add_filter( 'acf/fields/post_object/query/key=field_57ac72ae2f6fd',  __CLASS__ . '::exclude_id', 10, 3 );

    add_filter( 'acf/load_field/name=flex_layout', __CLASS__ . '::craft_content_layouts' );

    // Add content after the navigation{}
    // add_action( 'mega_menu_after_sub_menu', __CLASS__ . '::add_content_to_sub_menu', 10 );
    // add_filter( 'acf/load_field/name=which_menu_item', __CLASS__ . '::load_menu_items_into_select', 1, 10 );

    // To edit the ACF fields, you need to be local: define( 'LOCAL_INSTALLATION', true );
    if ( ! defined( 'LOCAL_INSTALLATION' ) ) {
      add_filter( 'acf/settings/show_admin', '__return_false' );
    }
  }

  /**
   * [craft_acf_parent_features list all features in parent theme]
   * @return [array] [list of filenames for acf-json to use]
   *
   * Available, but not by standard
   * - menu-builder-extension
   */
  static function craft_acf_parent_features() {
    // A list of the file names and their features
    $features = [
      'content-builder',      // → Layout and content builder
      'menu-builder',         // → Main / mega menu builder
      'theme-settings',       // → Theme settings
      'banner-builder',       // → Banner builder for banner CPT
      'banner-layout',        // → Banner layouts for banner CPT
      'banner-groups',        // → Banner Groups for banner group taxonomy
      'brand-logo',           // → Brand logo on Brand Taxonomy
      'variation-gallery',    // → Variation gallery on single product variations
    ];
    // Give me that list
    return apply_filters( 'craft_acf_parent_features', $features );
  }


  /**
   * [craft_acf_add_features Add all the features to the current theme]
   * @return [acf-json] [Add the ACF group to the website]
   */
  static function craft_acf_add_features() {
    // is ACF loaded? Does it even exist?!
    if ( ! function_exists( 'acf_add_local_field_group' ) ) {
      return;
    }

    // Get all the keys loaded into the site
    $keys = array_column( acf_get_field_groups(), 'key' );

    // Craft path for acf json
    $child_path  = untrailingslashit( get_stylesheet_directory() . '/acf-json' );
    $parent_path = untrailingslashit( get_template_directory() . '/acf-json' );

    // A list of the file names and their features
    $features = self::craft_acf_parent_features();

    // For all these features, load it all in.
    foreach ( $features as $feature ) {

      // Create the file paths + add .json to it
      $file = $child_path . '/' . $feature . '.json';

      // If the file doesn't exist in the child
      if ( ! file_exists( $file ) ) {
        $file = $parent_path . '/' . $feature . '.json';
      }

      // Check the parent, skip if not found
      if ( ! file_exists( $file ) ) {
        continue;
      }

      // Is this a json? Lets double check.
      $json = file_get_contents( $file );
      if ( empty( $json ) ) {
        continue;
      }

      // Load the JSON into the theme
      $json = json_decode( $json, true );
      $json['local'] = 'json';

      // If the JSON doesn't have a key, skip it
      if ( ! isset( $json['key'] ) ) {
        continue;
      }

      // Check if we haven't loaded this already
      // if ( acf_get_field_group( $json['key'] ) ) {
      //   continue;
      // }
      if ( in_array( $json['key'], $keys ) ) {
        continue;
      }

      // Add the key to the loaded array
      array_push( $keys, $json['key'] );

      // Create ACF field group based on json
      acf_add_local_field_group( $json );
    } // end foreach
  }

  // Pull all the menu items into a select for picking the right
  static function load_menu_items_into_select( $field ) {

    $field['choices'] = [];

    if( have_rows( 'menu_item', 'option' ) ) {
      while( have_rows( 'menu_item', 'option' ) ) {
        the_row();

        /* Load the menu items name so we can match it */
        if ( get_row_layout() == 'product_category' ) {
          $product_category = get_sub_field( 'menu_item_name', false );
          $field['choices'][$product_category] = '- ' . $product_category;
        }

        /* Load the menu items name so we can match it */
        else if ( get_row_layout() == 'sale_items' ) {
          $sale_name = get_sub_field( 'sale_name', false );
          $field['choices'][$sale_name] = '- ' . $sale_name;
        }

        /* Load the menu items name so we can match it */
        else if ( get_row_layout() == 'page' ) {
          $page_name = get_sub_field( 'page_name', false );
          $field['choices'][$page_name] = '- ' . $page_name;
        }

        /* Load the menu items name so we can match it */
        else if ( get_row_layout() == 'organised_menu' ) {
          $menu_title = get_sub_field( 'menu_title', false );
          $field['choices'][$menu_title] = '- ' . $menu_title;
        }
      }
    }

    // Put the choices into the menu loader
    return $field;
  }

  // Custom content in the sub menus
  static function add_content_to_sub_menu( $name ) {
    if ( have_rows( 'show_addition_content', 'option' ) ) {
      while ( have_rows( 'show_addition_content', 'option' ) ) {
        the_row();

        $selected_menu_item = get_sub_field( 'which_menu_item' );

        // If there's no selected menu, or the menu doesn't match
        // what we're trying to target, then return;
        if ( ! $selected_menu_item || $name != $selected_menu_item ) {
          return;
        }

        // Wrap this in a <div>
        echo '<div class="sub-menu-extension">';

        // If products is selected
        if ( get_row_layout() == 'products' ) {
          $selected_products = get_sub_field( 'select_products_to_show' );

          if ( empty( $selected_products ) ) {
            return;
          }

          // var_dump( $selected_products );
          echo '<ul class="sub-menu-products products">';
            global $post;
            foreach ( $selected_products as $post ) {
              setup_postdata( $post );
              wc_get_template_part( 'content', 'product' );
            }
            wp_reset_postdata();
          echo '</ul>';
        } // -->

        /*****/

        // If banner is selected
        if ( get_row_layout() == 'banner' ) {
          $selected_banner = get_sub_field( 'select_banner' );

          if ( empty( $selected_banner ) ) {
            return;
          }

          // var_dump( $selected_banner );
          foreach ( $selected_banner as $post ) {
            setup_postdata( $post );
            get_template_part( 'acf-fields/flex', 'banners' );
          }
          wp_reset_postdata();
        } // -->

        echo '</div>'; // div -->
      }
    }
  }

  /**
   * Extending the flexable field for craft content
   */
  static function craft_content_layouts( $field ) {

    // Remove the layouts
    // that are named in this list
    $remove_list = [
      // 'paragraph',
      // 'banner',
    ];
    foreach ( $field['layouts'] as $i => $layout ) {
      if ( in_array( $layout['name'], $remove_list ) ) {
        unset( $field['layouts'][ $i ] );
      }
    }

    /**
     * Load another AC Flex field into this layout mix
     */
    $add_layouts = [];
    $additional_fields = apply_filters( 'craft_extend_flex_content', $add_layouts );

    if ( empty( $additional_fields ) ) {
      return $field;
    }

    foreach ( $additional_fields as $fields ) {
      $extended_layouts = acf_get_fields( $fields );
      foreach ($extended_layouts['0']['layouts'] as $layout) {
        $field['layouts'][] = $layout;
      }
    }

    return $field;
  }

  /**
   * Exclude current post/page from post object field results
   */
  static function exclude_id( $args, $field, $post ) {
    $args['post__not_in'] = array( $post );
    return $args;
  }

  static function hide_acf_edit_for_non_admins( $show ) {
    return current_user_can( 'manage_options' );
  }

  /* Load ACF fields needed for the scheduling and for the custom front pages */
  static function drivkraft_theme_acf_load( $paths ) {
      unset( $paths[0] );
      $paths[] = get_template_directory() . '/acf-json';
      return $paths;
  }

  /* Save JSON when we edit them */
  static function drivkraft_theme_acf_save( $path ) {
      // update path
      $paths[] = get_template_directory() . '/acf-json';
      // return
      return $path;
  }

  static function drivkraft_theme_settings_admin_menu( $wp_admin_bar ) {
    // Theme settings | Options
    $args = [
      'id'    => 'theme_settings',
      'title' => __( 'Site settings', 'drivkraft-theme' ),
      'href'  => admin_url( 'admin.php?page=theme-options' ),
      'meta'  => [
        'class' => 'theme_settings',
        'title' => __( 'Change the site contents', 'drivkraft-theme' ),
      ],
    ];
    $wp_admin_bar->add_node( $args );

    // Main menu editor
    $args = [
      'id'     => 'theme_settings_menu',
      'title'  => __( 'Main menu', 'drivkraft-theme' ),
      'href'   => admin_url( 'admin.php?page=acf-options-main-menu' ),
      'parent' => 'theme_settings',
      'meta'   => [
        'class' => 'theme_settings-menu',
        'title' => __( 'Change the main menu', 'drivkraft-theme' ),
        ],
    ];
    $wp_admin_bar->add_node( $args );
  }
}

/* Theme options */

if ( function_exists( 'acf_add_options_page' ) ) {

  $theme_option = 'theme-options';

  acf_add_options_page(
    [
      'page_title' => __( 'Company details', 'drivkraft-theme' ),
      'menu_title' => __( 'Company details', 'drivkraft-theme' ),
      'menu_slug'  => $theme_option,
      'capability' => 'edit_posts',
      'icon_url'   => 'dashicons-admin-settings',
      'position'   => 2,
      'redirect'   => false,
      'autoload'   => false,
    ]
  );

  acf_add_options_sub_page(
    [
      'page_title'  => __( 'Main menu', 'drivkraft-theme' ),
      'menu_title'  => __( 'Main menu', 'drivkraft-theme' ),
      'menu_slug'  => 'acf-options-main-menu',
      'parent_slug' => $theme_option,
    ]
  );
}

drivkraft_theme_acf_loader::setup();
