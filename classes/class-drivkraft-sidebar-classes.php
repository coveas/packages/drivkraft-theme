<?php

class drivkraft_sidebar_detection {

  static function drivkraft_sidebar_class( $name = '' ) {
    static $class = 'active-sidebar';
    if ( ! empty( $name ) ) {
        $class .= ' sidebar-shop';
    };
    // ADD SOME CLASSES!
    drivkraft_sidebar_detection::drivkraft_sidebar_class_replace( $class );
  }

  static function drivkraft_sidebar_class_replace( $c = '' ) {
    static $class = '';
    if ( ! empty( $c ) ) {
      $class = $c;
    } else {
      echo str_replace( '<body class="', '<body class="' . $class . ' ', ob_get_clean() );
      ob_start();
    }
  }
}

// Create sidebar class in body for Woocommerce
// products to be aligned with a sidebar.
// add_action( 'wp_head', create_function( '', 'ob_start();' ) );
// add_action( 'get_sidebar', 'drivkraft_sidebar_detection::drivkraft_sidebar_class' );
// add_action( 'wp_footer', 'drivkraft_sidebar_detection::drivkraft_sidebar_class_replace' );
