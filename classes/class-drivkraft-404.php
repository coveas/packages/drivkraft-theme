<?php /* 404 Page content */

class drivkraft_404 {

  static function setup() {
    add_action( 'drivkraft_theme_404', __CLASS__ . '::error_404' );
  }

  // 404 content
  static function error_404() {
    do_action( 'drivkraft_theme_before_404' ); ?>
    <div class="e-404 cf">
      <h1 class="error-404--title">
        <?php _e( '404! Sorry, this page doesn\'t exist', 'drivkraft-theme' );?>
      </h1>
      <div class="e-404--inner cf">
        <a class="button button-alt" href="<?php echo bloginfo( 'url' )?>/">
          <?php _e( 'Return home', 'drivkraft-theme' );?>
        </a>
      </div>
    </div>
    <?php do_action( 'drivkraft_theme_after_404' );
  }
}

drivkraft_404::setup();
