<?php

class drivkraft_pagination {

  static function setup() {
    add_action( 'drivkraft_maybe_pagination', __CLASS__ . '::drivkraft_archive_pagination' );
  }

  static function drivkraft_archive_pagination() {
    // Pagination used throughout the site
    the_posts_pagination( array(
      'prev_text'          => __( 'Previous page', 'drivkraft-theme' ),
      'next_text'          => __( 'Next page', 'drivkraft-theme' ),
      'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'drivkraft-theme' ) . ' </span>',
    ) );
  }
}

drivkraft_pagination::setup();
