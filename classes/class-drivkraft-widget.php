<?php

/**
 * drivkraft_theme theme - Widget class
 */
class drivkraft_theme_Widget {

  static function setup() {
    add_action( 'widgets_init', __CLASS__ . '::widget_area' );
  }

  /**
   * Register all widget areas used in the theme
   */
  static function widget_area() {
    // Register sidebars
    register_sidebar( array(
      'name'          => __( 'Page sidebar', 'drivkraft-theme' ),
      'id'            => 'aside',
      'description'   => __( 'Add widgets here to appear in your sidebar.', 'drivkraft-theme' ),
      'before_widget' => '<aside id="%1$s" class="widget--sidebar %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h4 class="widget-title">',
      'after_title'   => '</h4>',
    ) );

    // Register sidebars
    register_sidebar( array(
      'name'          => __( 'Blog sidebar', 'drivkraft-theme' ),
      'id'            => 'blog',
      'description'   => __( 'Add widgets here to appear in your blog sidebar.', 'drivkraft-theme' ),
      'before_widget' => '<aside id="%1$s" class="widget--blog %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h4 class="widget-title--blog">',
      'after_title'   => '</h4>',
    ) );

    // Register sidebars
    register_sidebar( array(
      'name'          => __( 'Product filter', 'drivkraft-theme' ),
      'id'            => 'filter',
      'description'   => __( 'These widgets will appear on product archive pages above the products', 'drivkraft-theme' ),
      'before_title'  => '<strong class="extended--filter--item--title">',
      'before_widget' => '<div id="%1$s" class="extended--filter--item %2$s">',
      'after_widget'  => '</div>',
      'after_title'   => '</strong>',
    ) );

    register_sidebar( array(
      'name'          => __( 'Footer', 'drivkraft-theme' ),
      'id'            => 'footer',
      'description'   => __( 'Add widgets here to appear in your footer.', 'drivkraft-theme' ),
      'before_widget' => '<div id="%1$s" class="footer__widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4 class="footer__widget--title">',
      'after_title'   => '</h4>',
    ) );
  }
}

drivkraft_theme_Widget::setup();
