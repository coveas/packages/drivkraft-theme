<?php get_header(); ?>

<div id="content" class="cf">

  <?php do_action( 'drivkraft_before_front_content' );

  /* What? Where's the front page? I thought I was going to
  just do a simple change to the front page, but it seems
  that everything is missing!!

  Well young wipper-snapper, hold you horses, it's all here,
  it's just separated into tiny files for easier management.

  All sections of the website are split into a folder called
  acf-fields. In here, you're going to find all the sections
  with a prefix of flex-

  When you edit these files, you also have a scss file that
  is of the same naming convention to edit. Play around, Have
  some flex fun. If you find any issues, email andrew@drivkraft.no
  */

  // If this page is anything related to woocommerce, then we can't
  // do a flex area, because the main content comes from the_content

  // No woocommerce? Skip this check.
  if ( ! class_exists( 'woocommerce' ) ) {
        // Load the flex, it fallsback without ACF to the content
    get_template_part( 'acf-fields/flex', 'layout' );

  } else {

    // Ok, so we have woocommerce active, lets make sure these pages
    // only load the content and not the flex.
    if ( is_checkout() || is_cart() || is_account_page() || is_shop() ) {
      if ( have_posts() ) :
        echo '<div class="simple--content front-simple--content w cf">';
        while ( have_posts() ) : the_post();
          the_content();
        endwhile;
        echo '</div>';
      endif;

    } else {

      // Not on those pages? Ok, move to the flex layout
      get_template_part( 'acf-fields/flex', 'layout' );
    }
  }

  do_action( 'drivkraft_after_front_content' ); ?>

</div>

<?php get_footer(); ?>
