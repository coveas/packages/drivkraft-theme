<?php
  // Fetch these flex layout sub-fields:
  $acf_fields = [
    'paragraph'
  ];
  foreach ( $acf_fields as $key ) {
    @$$key = get_sub_field( $key );
  }
?>

<div class="paragraph cf">
  <div class="paragraph--container ">
    <?php echo $paragraph; ?>
  </div>
</div>
