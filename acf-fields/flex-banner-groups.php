<?php
$banner_group_id   = get_sub_field( 'banner_group' ); // Taxonomy ID
$banner_group_term = get_term( $banner_group_id );
$style             = get_field( 'layout_style', $banner_group_term->taxonomy . '_' . $banner_group_term->term_id ); // radio
$slider            = get_sub_field( 'slider' ); // post objects
$i                 = 0;
$full_width        = get_sub_field( 'full_width_banner' ); // post objects


$case                 = false;
$overwrite_svg_height = false;

// If taxonomy, get the posts related to it
if ( $banner_group_id ) {

  $args = [
    'posts_per_page' => -1,
    'post_type'      => 'banners',
    'order'          => 'ASC', // 1, 2, 3, 4...
    'meta_key'       => 'priority', // Order by Priority
    'orderby'        => 'meta_value_num', // Value is numerical
    'tax_query'      => [
        [
          'taxonomy'         => $banner_group_term->taxonomy,
          'field'            => 'term_id',
          'terms'            => $banner_group_term->term_id,
          'include_children' => false,
        ],
      ],
  ];
  $banner_ids = get_posts( $args );
}

// If not taxonomy and there are banners, then we
// do those posts instead.
if ( $banner_ids ) {
  $unique_id  = 'bnr_' . uniqid();
  if ( '1' == $style && $slider ) {
  ?>
    <script>
      // Loading the slider
      jQuery( document ).ready( function( $ ) {
        $( '#<?php echo $unique_id; ?>' ).unslider( {
          infinite: true,
          autoplay: true,
          nav: true,
          speed: 500,
          delay: 6000,
          arrows: {
            prev: '<a class="unslider-arrow prev"><img src="<?php echo get_template_directory_uri() . '/assets/images/drivicons/banner-arrow-left.svg'; ?>" alt="&#8249;" /></a>',
            next: '<a class="unslider-arrow next"><img src="<?php echo get_template_directory_uri() . '/assets/images/drivicons/banner-arrow-right.svg'; ?>" alt="&#8250;" /></a>',
          }
        } );
      });
    </script>
  <?php } ?>
  <div class="billboard billboard-group cf <?php echo ( $full_width) ? 'full-width-enabled' : 'completely-wrapped'; ?>">
    <div id="<?php echo $unique_id; ?>" class="billboard--container <?php echo ( $full_width) ? 'full-width-enabled' : 'w';?> cf">
      <ul>
        <?php foreach ( $banner_ids as $post ) {
          setup_postdata( $post );
          $banner = $post->id;
          $identifier  = 'banner-' . get_the_ID();
          $banner_link = '#';
          $acf_fields = [
            'banner_link',
            'link_to_category',
            'link_to_external_link',
            'banner_link_category',
            'external_url',
            'banner_creation',
            'desktop_image',
            'desktop_line_1',
            'desktop_line_2',
            'tablet_image',
            'tablet_line_1',
            'tablet_line_2',
            'mobile_image',
            'mobile_line_1',
          ];
          foreach ( $acf_fields as $key ) {
            @$$key = get_field( $key, $banner );
          }

          if ( $banner_link ) {
            $link = $banner_link;
          } else {
            $link = '#';
          }

          // If there's a category instead of a link
          // we need to loop through the array to get the
          // term information
          if ( $link_to_category && $banner_link_category ) {
            foreach ( $banner_link_category as $term ) :
              $link = get_term_link( $term );
            endforeach;
          }

          // If we're going for an external link though, lets do that.
          if ( $link_to_external_link && $external_url ) {
            $link = $external_url;
          }

          // Set widths & heights for layout rendering issues
          $media__desktop = wp_get_attachment_image_src( $desktop_image, 'large' );
          $desktop_width  = ( wp_get_attachment_metadata( $desktop_image )?  wp_get_attachment_metadata( $desktop_image )['width'] : '' );
          $desktop_height = ( wp_get_attachment_metadata( $desktop_image ) ?  wp_get_attachment_metadata( $desktop_image )['height'] : '' );
          $desktop_ratio  = ( wp_get_attachment_metadata( $desktop_image ) ?  100 / ($desktop_width / $desktop_height) : '100' );

          $media__tablet  = wp_get_attachment_image_src( $tablet_image, 'large' );
          $tablet_width   = ( wp_get_attachment_metadata( $tablet_image ) ?  wp_get_attachment_metadata( $tablet_image )['width'] : '' );
          $tablet_height  = ( wp_get_attachment_metadata( $tablet_image ) ?  wp_get_attachment_metadata( $tablet_image )['height'] : '' );
          $tablet_ratio  = ( wp_get_attachment_metadata( $tablet_image ) ?  100 / ($tablet_width / $tablet_height) : '100' );

          $media__mobile  = wp_get_attachment_image_src( $mobile_image, 'medium' );
          $mobile_width   = ( wp_get_attachment_metadata( $mobile_image ) ?  wp_get_attachment_metadata( $mobile_image )['width'] : '' );
          $mobile_height  = ( wp_get_attachment_metadata( $mobile_image ) ?  wp_get_attachment_metadata( $mobile_image )['height'] : '' );
          $mobile_ratio  = ( wp_get_attachment_metadata( $mobile_image ) ?  100 / ($mobile_width / $mobile_height) : '100' );

          $triptych = ( '3' === $style ) ? true : false;
          if ( $triptych && ! $overwrite_svg_height ) {
            $overwrite_svg_height = $desktop_ratio;
            $case                 = $identifier;
          }
          ?>
        <li class="cf banner--object <?= $identifier?> showcase-<?= $style; ?>">

          <a class="banner--object--container" style="position: relative; display: block;" href="<?= $link; ?>">

            <?php /* Utilising Picturefill.js, we're going to get some responsive images working for banners */ ?>
            <picture style="position: absolute; top: 0; left: 0; width: 100%; height: auto; " class="banner--object--picture">

              <!--[if IE 9]><video style="display: none;"><![endif]-->

              <?php
              // If there's a desktop image
              if ( $media__desktop ) {
                echo '<source height="' . $desktop_height . '" width="' . $desktop_width . '" srcset="' . $media__desktop[0] . '" media="(min-width: 58em)">';
              }

              // If there's a tablet image
              if ( $media__tablet ) {
                echo '<source height="' . $tablet_height . '" width="' . $tablet_width . '" srcset="' . $media__tablet[0] . '" media="(min-width: 30em)">';
              }

              // If there's a mobile image
              if ( $media__mobile ) {
                echo '<source height="' . $mobile_height . '" width="' . $mobile_width . '" srcset="' . $media__mobile[0] . '" media="(min-width: 1em)" alt="">';
              } ?>

              <!--[if IE 9]></video><![endif]-->

              <?php
              if ( $media__desktop ) {
                echo '<img height="' . $desktop_height . '" width="' . $desktop_width . '" srcset="' . $media__desktop[0] . '" media="(min-width: 58em)">';
              } ?>

              <!--[if IE 9]>
              <?php
              if ( $media__desktop ) {
                echo '<img height="' . $desktop_height . '" width="' . $desktop_width . '" src="' . $media__desktop[0] . '" media="(min-width: 58em)">';
              } ?>
              <![endif]-->
            </picture>

            <?php if ( $media__tablet ) { ?>
            <svg class="svg-placeholder placeholder-tablet" style="width: 100%; height: 0; padding-bottom: <?= $tablet_ratio; ?>%" viewBox="0 0 100 <?= $tablet_ratio; ?>" preserveAspectRatio="none" shape-rendering="crispEdges" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <rect width="100%" height="100%"></rect>
            </svg>
            <?php } if ( $media__mobile ) { ?>
            <svg class="svg-placeholder placeholder-mobile" style="width: 100%; height: 0; padding-bottom: <?= $mobile_ratio; ?>%" viewBox="0 0 100 <?= $mobile_ratio; ?>" preserveAspectRatio="none" shape-rendering="crispEdges" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <rect width="100%" height="100%"></rect>
            </svg>
            <?php } if ( $media__desktop ) { ?>
            <svg class="svg-placeholder placeholder-desktop" style="width: 100%; height: 0; padding-bottom: <?= $desktop_ratio; ?>%" viewBox="0 0 100 <?= $desktop_ratio; ?>" preserveAspectRatio="none" shape-rendering="crispEdges" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <rect  x="0" y="0" width="100%" height="100%"></rect>
            </svg>
            <?php } ?>

            <?php if ( $desktop_line_1 || $desktop_line_2 ) { ?>
              <div class="banner--object--d--title">
                <span class="banner--object--d--title--top"><?php echo $desktop_line_1; ?></span>
                <span class="banner--object--d--title--bottom"><?php echo $desktop_line_2; ?></span>
                <?php do_action( 'drivkraft_banners_after_title', $identifier ); ?>
              </div>
            <?php } ?>
            <?php if ( $tablet_line_1 || $tablet_line_2 ) { ?>
              <div class="banner--object--t--title">
                <span class="banner--object--t--title--top"><?php echo $tablet_line_1; ?></span>
                <span class="banner--object--t--title--bottom"><?php echo $tablet_line_2; ?></span>
                <?php do_action( 'drivkraft_banners_after_title', $identifier ); ?>
              </div>
            <?php } else { ?>
              <?php if ( $desktop_line_1 || $desktop_line_2 ) { ?>
                <div class="banner--object--t--title">
                  <span class="banner--object--t--title--top"><?php echo $desktop_line_1; ?></span>
                  <span class="banner--object--t--title--bottom"><?php echo $desktop_line_2; ?></span>
                  <?php do_action( 'drivkraft_banners_after_title', $identifier ); ?>
                </div>
              <?php } ?>
            <?php } ?>
            <?php if ( $mobile_line_1 || $mobile_line_1 ) { ?>
              <div class="banner--object--m--title">
                <span class="banner--object--m--title--top"><?php echo $mobile_line_1; ?></span>
                <?php do_action( 'drivkraft_banners_after_title', $identifier ); ?>
              </div>
            <?php } else { ?>
              <?php if ( $desktop_line_1 || $desktop_line_2 ) { ?>
                <div class="banner--object--m--title">
                  <span class="banner--object--m--title--top"><?php echo $desktop_line_1; ?></span>
                  <span class="banner--object--m--title--bottom"><?php echo $desktop_line_2; ?></span>
                  <?php do_action( 'drivkraft_banners_after_title', $identifier ); ?>
                </div>
              <?php } ?>
            <?php } ?>
          </a>
        </li>
        <?php } ?>
        <?php // Get the ratio sorted for the SVG when we're using the first image as a guide
        if ( $overwrite_svg_height ) { ?>
         <style>
          #<?= $unique_id; ?> li.showcase-3 .banner--object--container { overflow:hidden; }
          #<?= $unique_id; ?> li.showcase-3 .banner--object--picture { height:100% !important; }
          #<?= $unique_id; ?> li.showcase-3 .banner--object--picture source,
          #<?= $unique_id; ?> li.showcase-3 .banner--object--picture img { object-fit: cover; }
          #<?= $unique_id; ?> li.showcase-3 svg.placeholder-desktop { padding-bottom: <?= $overwrite_svg_height?>% !important }
        </style>
        <?php } ?>
      </ul>
    </div>
  </div>
<?php }
wp_reset_postdata();
