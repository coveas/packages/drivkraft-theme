<?php

  // Fetch these flex layout sub-fields:
  $acf_fields = [
    'popular_categories_title',
    'categories',
  ];

  // We assign all the acf fields to a variable
  // for use throughout this template file.
  foreach ( $acf_fields as $key ) {
    @$$key = get_sub_field( $key );
  }

  /*
  Selected categories that are worth raving about
  */

 if ( !empty( $categories ) ): ?>

  <div id="categories" class="featured-categories">

    <div class="featured-categories--inner w cf">

      <?php if ( get_sub_field( 'popular_categories_title' ) ) { ?>
        <div class="section__title">
          <h2 class="section__title--heading">
            <?php the_sub_field( 'popular_categories_title' ); ?>
          </h2>
        </div>
      <?php } ?>

      <?php
      foreach ( $categories as $cat ) {
        $term = get_term( $cat , 'product_cat' );
        $category_id = $term->term_id;
        echo '<a class="button featured-categories--button" href="'. get_term_link( $term->slug, 'product_cat' ) .'">'. $term->name .'</a>';
      }
      ?>

    </div>

  </div>
<?php endif; ?>
