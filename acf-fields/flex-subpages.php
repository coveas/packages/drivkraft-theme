<?php

/*
  How this works.

  You have the option to list the sub-pages
  of this page/post. You can either manually
  pick those pages, or you can show all of them.
  In fact, if you pick nothing, you get nothing.
  You dig?
*/

global $post;
$current_page_id  = $post->ID;
$show_thumbnails  = get_sub_field( 'display_thumbnails' );
$show_excerpt     = get_sub_field( 'display_excerpts' );
$show_all_pages   = get_sub_field( 'show_all_posts' );
$custom_selection = get_sub_field( 'sub_pages' );

if ( ! $custom_selection && ! $show_all_pages ) {
  return;
}

if ( $show_all_pages ) {

  $args = [
    'posts_per_page' => -1,
    'sort_order'     => 'rand',
    'child_of'       => $current_page_id,
    'post_parent'    => $current_page_id, // Needed for get_posts instead of get_pages
    'parent'         => $current_page_id,
    'post_type'      => 'page',
    'post_status'    => 'publish',
    'fields'         => 'ids',
    // 'content-type' => 'parentr',
  ];

  $pages = get_posts( $args );

} else {

  $pages = $custom_selection;

}

if ( ! $pages ) {
  return;
}
?>

<div class="parent-sub-pages--container cf">

  <ul class="parent-sub-pages cf">
    <?php foreach ( $pages as $page ) { ?>
      <li class="parent-sub-page cf">
        <a class="parent-sub-page--link" href="<?php echo get_permalink( $page ); ?>">

          <?php
          /* If we can show thumbnails */
          if ( $show_thumbnails ) {
            drivkraft_load::featured_image( $page, 'thumbnail' );
          } ?>

          <h4 class="parent-sub-page--title"><?php echo get_the_title( $page );?></h4>

          <?php

          if ( $show_excerpt ) {
            /* Get the paragraph field on the child
            page, strip it down to 20 words */
            $limit = 20;
            $rows = get_post_meta( $page, 'flex_layout', true );

            if ( $rows ) {
              foreach ( (array) $rows as $count => $row ) {
                switch ( $row ) {
                  case 'paragraph':
                    $content = get_post_meta( $page, 'flex_layout_' . $count . '_paragraph', true );
                    if ( $content ) {
                      echo '<div class="parent-sub-page--content">';
                      echo limit_word_count( strip_tags( apply_filters( 'the_content', $content ) ), $limit );
                      echo '</div>';
                    }
                    break;
                }
              }
            }
          } // End show excerpt ?>
        </a>
      </li>
    <?php } ?>
  </ul>

  <?php /* Optional content after pages. Perhaps a button? */
  do_action( 'drivkraft_flex_sub_pages_after' ); ?>

</div>

