<?php

/* Pull in the fields! */
$acf_fields = [
  'type',
  'product_list_title',
  'product_list_link_text',
  'selected_products',
  'product_cat',
  'product_featured_cat',
  'product_list_link',
  'product_count',
  'products_per_row',
];
// Assign them to a variable
foreach ( $acf_fields as $key ) {
  @$$key = get_sub_field( $key );
}

// If there are no columns set,
// set the default to 4
if ( ! $products_per_row ) {
  $products_per_row = 4;
}

$meta_query = WC()->query->get_meta_query();
echo '<div class="flex-product-list type-' . $type . ' cf w">';

  // if we're featured or cateogry
if ( 'featured' === $type || 'category' === $type ) {

  $args       = [
    'post_type'           => 'product',
    'post_status'         => 'publish',
    'posts_per_page'      => $product_count,
    'ignore_sticky_posts' => 1,
    // 'orderby'             => 'rand',
    // 'order'               => 'DESC',
    'meta_query'          => $meta_query,
    'no_found_rows'       => true

  ];

  $link = '';

  // When featured is selected,
  if ( 'featured' == $type ) {
    // Filter based on _featured
    $args['meta_query'][] = [
      [
        'key'   => '_featured',
        'value' => 'yes',
      ],
    ];

    if ( $product_featured_cat ) {
      $args['tax_query'] = [
        [
          'taxonomy' => 'product_cat',
          'terms'    => $product_featured_cat,
        ],
      ];
    }
  }

  // When a product category is selected
  if ( 'category' == $type && $product_cat ) {
    // Filter based on the chosen category

    $args['meta_query'][] = [
      [
        'key'   => '_stock_status',
        'value' => 'instock',
      ],
    ];

    $args['tax_query'] = [
      [
        'taxonomy' => 'product_cat',
        'fields'   => 'id',
        'terms'    => $product_cat,
      ],
    ];
    // Prepare a link
    $link = get_term_link( $product_cat, 'product_cat' );
  }
  if ( $product_list_link ) {
    $link = $product_list_link;
  }

  // Cache this query
  $loop = get_transient( 'craft_featured_products_query_' . get_row_index() );
  if ( false === $loop ) {
    $loop = new WP_Query( $args );
    set_transient( 'craft_featured_products_query_' . get_row_index(), $loop, 10 * MINUTE_IN_SECONDS );
  }

  if ( $loop->have_posts() ) : ?>
    <div class="featured columns-<?php echo $products_per_row ?>">

      <?php if ( $product_list_title ) { ?>
        <div class="section__title">
            <h2 class="section__title--heading" data-type="p-l-t"><?php echo $product_list_title; ?></h2>
          <?php if ( $link && $product_list_link_text ) : ?>
            <a class="section__title--link" href="<?php echo $link; ?>"><?php echo $product_list_link_text; ?></a>
          <?php endif?>
        </div>
      <?php } ?>

      <ul class="products">
      <?php
      do_action( 'craft_before_product_list_loop' );
      while ( $loop->have_posts() ) : $loop->the_post();
        wc_get_template_part( 'content', 'product' );
      endwhile;
      do_action( 'craft_after_product_list_loop' );
      ?>
      </ul>
    </div>
  <?php endif;
  wp_reset_query();
  wp_reset_postdata();

  // If we're using best sellers
} elseif ( 'best-sellers' === $type ) {

  $args = array(
    'post_type'           => 'product',
    'post_status'         => 'publish',
    'ignore_sticky_posts' => 1,
    'posts_per_page'      => $product_count,
    'meta_key'            => 'total_sales',
    'orderby'             => 'meta_value_num',
    'meta_query'          => $meta_query,
    'no_found_rows'       => true
  );

  if ( $product_featured_cat ) {
    $args['tax_query'] = [
      [
        'taxonomy' => 'product_cat',
        'terms'    => $product_featured_cat,
      ],
    ];
  }

  // Cache this query
  $loop = get_transient( 'craft_best_selling_products_query_' . get_row_index() );
  if ( false === $loop ) {
    $loop = new WP_Query( $args );
    set_transient( 'craft_best_selling_products_query_' . get_row_index(), $loop, 10 * MINUTE_IN_SECONDS );
  }
  if ( $loop->have_posts() ) { ?>
    <!-- Best Sellers -->
    <div class="best_sellers columns-<?php echo $products_per_row ?>">
      <?php if ( $product_list_title ) : ?>
        <div class="section__title">
          <h2 class="section__title--heading" data-type="p-l-t"><?php echo $product_list_title; ?></h2>
          <?php if ( $product_list_link && $product_list_link_text ) : ?>
            <a class="section__title--link" href="<?php echo $product_list_link; ?>"><?php echo $product_list_link_text; ?></a>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <ul class="products">
        <?php
        do_action( 'craft_before_product_list_loop' );
        while ( $loop->have_posts() ) : $loop->the_post();
          wc_get_template_part( 'content', 'product' );
        endwhile;
        do_action( 'craft_after_product_list_loop' );
        ?>
      </ul>
    </div>
    <!-- End x Best Sellers -->
  <?php }

  wp_reset_postdata();
  wp_reset_query();

  // Finally, if we're just the latest
} elseif ( 'new' === $type ) {

  $args = array(
    'post_type'           => 'product',
    'post_status'         => 'publish',
    'ignore_sticky_posts' => 1,
    'posts_per_page'      => $product_count,
    'orderby'             => 'date',
    'order'               => 'desc',
    'meta_query'          => $meta_query,
    'no_found_rows'       => true
  );

  if ( $product_featured_cat ) {
    $args['tax_query'] = [
      [
        'taxonomy' => 'product_cat',
        'terms'    => $product_featured_cat,
      ],
    ];
  }

  // Cache this query
  $loop = get_transient( 'craft_new_products_query_' . get_row_index() );
  if ( false === $loop ) {
    $loop = new WP_Query( $args );
    set_transient( 'craft_new_products_query_' . get_row_index(), $loop, 10 * MINUTE_IN_SECONDS );
  }
  if ( $loop->have_posts() ) { ?>
    <!-- Best Sellers -->
    <div class="recent_products columns-<?php echo $products_per_row ?>">
      <?php if ( $product_list_title ) : ?>
        <div class="section__title">
          <h2 class="section__title--heading" data-type="p-l-t"><?php echo $product_list_title; ?></h2>
          <?php if ( $product_list_link && $product_list_link_text ) : ?>
            <a class="section__title--link" href="<?php echo $product_list_link; ?>"><?php echo $product_list_link_text; ?></a>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <ul class="products">
        <?php
        do_action( 'craft_before_product_list_loop' );
        while ( $loop->have_posts() ) : $loop->the_post();
          wc_get_template_part( 'content', 'product' );
        endwhile;
        do_action( 'craft_after_product_list_loop' );
        ?>
      </ul>
    </div>
    <!-- End x Best Sellers -->
  <?php }
  wp_reset_postdata();
  wp_reset_query();

} elseif ( 'selected' === $type ) {
  /* Selected products rather than generated via a selecton */
  if ( $selected_products ) {

    /**
     * Capture the entire segment as HTML and save this as a transient
     * Return this transient on the front page. This is micro caching.
     * This stops the page from needing to laod all of the data that comes
     * with the products in the selected product sections.
     */

    // This transient will take the array, convert it to a string & use this as the transient name to ensure it's unique
    $transient       = 'craft_product_section_' . implode( $selected_products, '' ) . '_' . get_row_index();
    $product_section = get_transient( $transient );

    if ( false === $product_section ) {
      // Capture the HMTL of this entire section
      ob_start();
      ?>

      <div class="selected columns-<?php echo $products_per_row ?>">
        <?php if ( $product_list_title ) : ?>
          <div class="section__title">
            <h2 class="section__title--heading" data-type="p-l-t"><?php echo $product_list_title; ?></h2>
            <?php if ( $product_list_link && $product_list_link_text ) : ?>
              <a class="section__title--link" href="<?php echo $product_list_link; ?>"><?php echo $product_list_link_text; ?></a>
            <?php endif; ?>
          </div>
        <?php endif; ?>

        <ul class="products">
          <?php
          do_action( 'craft_before_product_list_loop' );

          $selected_post_args = array(
            'post__in'       => $selected_products, // Query for POSTS__IN to speed up results
            'orderby'        => 'post__in', // Order by how the client wanted it ordered
            'post_type'      => 'product',
            'post_status'    => 'publish',
            'posts_per_page' => count($selected_products), // Limit to how many selected
            'no_found_rows'  => true // This will stop the query from accounting for pagination
          );

          $loop = new WP_Query( $selected_post_args );
          while ( $loop->have_posts() ) : $loop->the_post();
            // require( get_template_directory() . '/acf-fields/blank-list-product.php' ); // Load in blank product
            wc_get_template_part( 'content', 'product' );
          endwhile;

          do_action( 'craft_after_product_list_loop' );
          ?>
        </ul>
      </div>

      <?php
      $product_section     = ob_get_clean();
      $set_product_section = set_transient( $transient, $product_section, 0.3 * DAY_IN_SECONDS );
    }

    echo $product_section;

    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly

  }
}

?>

</div>
