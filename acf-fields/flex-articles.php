<?php

/**
 * List articles on the page
 */

$articles        = get_sub_field( 'articles' );
$show_thumbnails = get_sub_field( 'display_thumbnails' );
$show_excerpt    = get_sub_field( 'display_excerpts' );

// If there are no pages
if ( empty( $articles ) ) {
  return;
}
?>


<div class="parent-articles cf">

  <ul class="parent-articles--container w cf">

    <?php foreach ( $articles as $article ) { ?>
      <li class="parent-article cf">
        <a class="parent-article--link" href="<?php echo get_permalink( $article ); ?>">
          <?php
          /* If we can show thumbnails */
          if ( $show_thumbnails ) {
            echo get_the_post_thumbnail( $article, 'medium' );
          } ?>

          <h4 class="parent-article--title"><?php echo get_the_title( $article );?></h4>

          <?php

          if ( $show_excerpt ) {
            /* Get the paragraph field on the child
            page, strip it down to 20 words */
            $limit = 20;
            $article = get_post( $article );
            if (  $article->post_content ) {
              echo '<p class="parent-article--content">';
              echo limit_word_count( strip_tags( apply_filters( 'the_content', $article->post_content ) ), $limit );
              echo '</p>';
            }
          } // End show excerpt ?>
        </a>
      </li>
    <?php } ?>
  </ul>

  <?php /* Optional content after pages. Perhaps a button? */
  do_action( 'drivkraft_flex_articles_after' ); ?>

</div>

