<?php

// A simple image that can become full width
$image = get_sub_field( 'image' );
$edge  = get_sub_field( 'edge_to_edge' );

// if $edge is true, class removes W and places edge in place
$class = ( $edge ) ? 'edge' : 'w';

if( $image ) { ?>

  <picture class="standalone--image <?php echo $class; ?>">

    <!--[if IE 9]><video style="display: none;"><![endif]-->

    <?php
        // Large image version
        $media__large = wp_get_attachment_image_src( $image, 'large' );
        $media__alt   = get_post_meta( $image, '_wp_attachment_image_alt', true );
        $media__title = get_the_title( $image );

       if ( $media__large ) {
          echo '<source alt="' . $media__alt . '" title="' . $media__title  . '" srcset="' . $media__large[0] . '" media="(min-width: 40em)">';
        }

        // Scale it down for mobile
        $media__mobile  = wp_get_attachment_image_src( $image, 'medium' );
        if ( $media__mobile ) {
          echo '<source alt="' . $media__alt . '" title="' . $media__title  . '" srcset="' . $media__mobile[0] . '" media="(min-width: 1em)" alt="">';
        }

        if ( $media__large ) {
          echo '<img alt="' . $media__alt . '" title="' . $media__title  . '" srcset="' . $media__large[0] . '" media="(min-width: 58em)">';
        }
    ?>

    <!--[if IE 9]></video><![endif]-->

  </picture>
<?php
}