<?php

/**
 * USP Flex layout: Returns a list of
 * titles and accompanying test
 */

if ( have_rows( 'usp' ) ) : ?>

<div class="usp__list cf">

  <ul class="usp__list--container w cf">

  <?php while ( have_rows( 'usp' ) ) : the_row();

    $title = get_sub_field( 'title' );
    $content = get_sub_field( 'content' ); ?>

    <li class="usp__list--item">
      <strong class="usp__list--item--title"><?php echo $title ?></strong>
      <span class="usp__list--item--content"><?php echo $content ?></span>
    </li>

  <?php endwhile; ?>

  </ul>

</div>

<?php endif;
