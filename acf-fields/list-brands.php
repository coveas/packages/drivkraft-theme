  <div class="sub-menu--column">

    <strong><?php _e( 'Brands', 'drivkraft-theme' ); ?></strong>

    <ul class="sub-menu--list">
      <?php

        $link_string    = '';
        $transient_name = 'query_' . md5( $category->slug . $category->taxonomy );
        if ( false === ( $link_string = get_transient( $transient_name ) ) ) {
            $args = [
                'post_type' => 'product',
                'show_posts'=> 10,
                'fields'    => 'ids', // Only query the post ID's, not complete post objects
                'tax_query' => [
                    [
                        'taxonomy'  => $category->taxonomy,
                        'field'     => 'slug',
                        'terms'     => $category->slug
                    ]
                ]
            ];
            $ids = get_posts( $args );
            $links = [];
            // Make sure we have ID'saves
            if ( $ids ) {
                /**
                 * Because we only query post ID's, the post caches are not updated which is
                 * good and bad
                 *
                 * GOOD -> It saves on resources because we do not need post data or post meta data
                 * BAD -> We loose the vital term cache, which will result in even more db calls
                 *
                 * To solve that, we manually update the term cache with update_object_term_cache
                 */
                update_object_term_cache( $ids, 'product' );

                $term_names = [];

                foreach ( $ids as $id ) {
                    $terms = get_object_term_cache( $id, 'brand' );
                    foreach ( $terms as $term ) {
                        if ( in_array( $term->name, $term_names ) )
                            continue;

                        $term_names[] = $term->name;

                        $links[$term->name] = '<li><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></li>';
                    }
                }
            }


            if ( $links ) {
                ksort( $links );
                $limitation  = array_slice($links, 0, 10);
                $link_string = implode( "\n\t" , $limitation );
            } else {
                $link_string = '';
            }

            set_transient( $transient_name, $link_string, 7 * DAY_IN_SECONDS );
        }

        echo $link_string;

      } else {
        $i=0;
        foreach ($manually_pick_brands as $term) {
          // Count to 10, then break
          if($i==10) break;
          $term = get_term( $term );
          echo '<li><a href="' . get_term_link( $term->term_id, $term->taxonomy ) . '">' . $term->name . '</a></li>';
          $i++;
      } ?>
    </ul>
  </div>

