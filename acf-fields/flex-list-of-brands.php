<?php
/**
 * list_of_brands
 */
$section_title  = get_sub_field( 'section_title' ); // radio
$list_of_brands = get_sub_field( 'selected_brands' ); // post objects
$brand_link     = get_sub_field( 'link_to_brands_page' ); // post objects

// If no selected terms
if ( empty( $list_of_brands ) ) {
  return;
}
?>

<div class="selected-brands cf">

  <div class="selected-brands--container w cf">

    <?php if ( $section_title ) { ?>
    <div class="section__title">
      <h2 class="section__title--heading" data-type="p-l-t"><?php echo $section_title; ?></h2>
    </div>
    <?php } ?>

      <ul class="selected-brands--items cf">
      <?php
      /**
       * Loop through the selected brands and get their logos
       */
      foreach ( $list_of_brands as $term ) {

        $term = get_term( $term );

        // If no term, try the next one
        if ( ! $term ) {
          continue;
        }

        /**
         * Breakdown of available information
         */
        $name    = $term->name;
        $link    = get_term_link( $term->term_id, $term->taxonomy );
        $logo_id = get_field( 'logo', $term->taxonomy . '_' .$term->term_id );
        $svg_id  = get_field( 'logo', $term->taxonomy . '_' .$term->term_id );

        /**
         * List each brand with image and link
         */
        echo '<li class="selected-brands--item">'; // <li>
        echo '<a href="' . $link . '">';
        // If SVG logo is available
        if ( $svg_id ) {
          $svg_img = wp_get_attachment_image_src( $svg_id );
          echo "<img src='$svg_img[0]' alt='$name' width='150' />";
          // If there's no SVG, try for an image
        } elseif ( $logo_id ) {
          $logo_img = wp_get_attachment_image_src( $logo_id );
          echo "<img src='$logo_img[0]' alt='$name' width='150' />";
          // Still no image? Rollback to Text based name
        } else {
          echo $name;
        }
        echo '</a>';
        echo '</li>';
      }
      ?>
    </ul>

    <?php if ( $brand_link ) { ?>
      <a href="<?php echo $brand_link; ?>" class="button button-brands"><?php _e( 'View all brands', 'drivkraft-theme' );?></a>
    <?php } ?>

  </div>

</div>
