<?php

if ( ! class_exists( 'acf' ) ) {
  do_action( 'drivkraft_before_flex_failed_layout' );
  echo '<div class="no-flex-break w cf">';
  the_content();
  echo '</div>';
  do_action( 'drivkraft_after_flex_failed_layout' );
  return;
}

if ( ! isset( $post_id ) ) { $post_id = ''; }

do_action( 'drivkraft_before_flex_layout' );

/* If we have the ACF group 'flex_layout'...*/
if ( have_rows( 'flex_layout', $post_id ) ) :

  /* ...we should run through it... */
  while ( have_rows( 'flex_layout', $post_id ) ) : the_row();

    /** Insert something into the flex layout
    do a row count to find out where you are... */
    do_action( 'drivkraft_flex_layout_insert', $post_id );

    /* We choose to show a list of products */
    if ( 'product_list' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'product-lists' );

    /* A simple paragraph wysiwyg editor */
    elseif ( 'paragraph' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'paragraph' );

    /* A simple image */
    elseif ( 'image' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'image' );

    /* Banners – This is complex */
    elseif ( 'banner' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'banners' );

    /* Banner groups – This is even more complex */
    elseif ( 'banner_group' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'banner-groups' );

    /* List selected brand logos */
    elseif ( 'list_of_brands' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'list-of-brands' );

    /* Text based USP list */
    elseif ( 'usp_block' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'usp' );

    /* Children of the current page */
    elseif ( 'sub_page_list' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'subpages' );

    /* Display Articles / posts */
    elseif ( 'show_selected_articles' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'articles' );

    /* Get a 'Block' and run through this again */
    elseif ( 'block' === get_row_layout() ) :
      $this_post_id = $post_id;
      $post_id = get_sub_field( 'selected_block' );
      if ( ! $post_id ) { echo '<hr />'; }
      else { require __FILE__; }
      $post_id = $this_post_id;

    /* Output a list of popular categories */
    elseif ( 'popular_categories' === get_row_layout() ) :
      get_template_part( 'acf-fields/flex', 'popular-categories' );

    endif;

    endwhile;

  /* If no flex detected, default to the_content */

  else :
    do_action( 'drivkraft_before_flex_failed_layout' );
    echo '<div class="no-flex-break w cf">';
    the_content();
    echo '</div>';
    do_action( 'drivkraft_after_flex_failed_layout' );
  endif;

  do_action( 'drivkraft_after_flex_layout' );
