<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]><script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script><![endif]-->
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope="" itemtype="http://schema.org/WebPage">

  <?php do_action( 'drivkraft_after_body_tag' ); ?>

  <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'drivkraft-theme' ); ?></a>

  <div class="container" role="document">

    <?php
    /*
      Before the header has been created
     */
    do_action( 'drivcraft_before_header' );

    /*
      Header location: includes/header-ident-top
      Default: USP, Logo, Account menu
     */
    get_template_part( 'includes/header', 'ident-top' );

    // Inbetween headers
    do_action( 'drivcraft_between_header' );

    /*
      Header location: includes/header-ident-bottom
      Default: search, navigation, cart
     */
    get_template_part( 'includes/header', 'ident-bottom' );

    /*
      After the header has been created
     */
    do_action( 'drivcraft_after_header' );

    /*
      After the header, after the breadcrumbs, before the content
     */
    do_action( 'drivcraft_before_content_begins' );
