<?php

// Import the theme setup class
require_once( 'classes/class-drivkraft-required.php' );
require_once( 'classes/class-drivkraft-theme-setup.php' );
drivkraft_theme_setup::setup();

/**
 * Menu fallback for no menu available
 * @return [type] [description]
 */
function drivkraft_no_menu_available() {
  if ( ! current_user_can( 'manage_options' ) ) {
    $message  = '<p class="no-menu-yet"></p>';
  } else {
    $message  = '<p><small>';
    $message .= __( 'Enable menu', 'drivkraft-theme' );
    $message .= ': ';
    $message .= __( 'Dashboard > Theme settings > Main menu', 'drivkraft-theme' );
    $message .= '</small></p>';
  }
  echo $message;
}

/**
 * Limit the word count of something
 */
if ( ! function_exists( 'limit_word_count' ) ) {
  function limit_word_count( $text, $limit ) {
    if ( str_word_count( $text, 0 ) > $limit ) {
        $words = str_word_count( $text, 2 );
        $pos   = array_keys( $words );
        $text  = substr( $text, 0, $pos[ $limit ] ) . '...';
    }
    return $text;
  }
}

/**
 * Backwards compatibility
 */
if ( ! function_exists( 'wc_get_page_id' ) && class_exists( 'woocommerce' ) ) {
  function wc_get_page_id( $thing ) {
    woocommerce_get_page_id( $thing );
  }
}

$GLOBALS['drivkraft-theme'] = new drivkraft_theme_setup();
