<?php /* Template name: Simple content */
get_header(); ?>

<div id="content" class="simple--page w cf">

  <?php do_action( 'drivkraft-simple-page-before' );

  if ( have_posts() ) :

    while ( have_posts() ) : the_post();

       the_content();

     endwhile;

   endif;

   do_action( 'drivkraft-simple-page-after' ); ?>

</div>

<?php get_footer(); ?>
