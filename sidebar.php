<?php if ( is_active_sidebar( 'aside' ) ) : ?>
  <div id="sidebar" class="content--sidebar" role="complementary">
    <?php dynamic_sidebar( 'aside' ); ?>
  </div>
<?php endif; ?>
