    </div><!-- End container -->

    <?php
    /* No ACF? Lets return only the footer() */
    if ( ! class_exists( 'acf' ) ) {
      ?>
      <footer id="footer" class="footer widget-area cf" role="footer">
        <div class="widget-area--inner w">
          <div class="footer--widgets--container cf">
            <?php if ( is_active_sidebar( 'footer' ) ) : dynamic_sidebar( 'footer' ); endif; ?>
          </div>
        </div>
      </footer>
      <div class="post--credits">
        <div class="post--credits--wrapper w cf">
          <div class="post--copyright">
            <?php echo sprintf( __( '&copy; %1$s %2$s', 'drivkraft-theme' ), get_bloginfo( 'name' ), get_the_time( 'Y' ) ); ?>
          </div>
        </div>
      </div>
      <?php
      do_action( 'drivkraft_before_body_tag_close' );
      wp_footer();
      return;
    } ?>

    <?php
    // If we have ACF, then lets move onwards with custom things.

    // We might want to put something before the footer, so there's
    // an action here just incase we want to hook into it at a later
    // date.
    do_action( 'drivkraft_before_footer' );

    // Insert content in Footer widget in WP-admin
    // in order to make it visible, the footer is designed
    // to hold four columns of equal length. But not limited to

    if ( is_active_sidebar( 'footer' ) ) { ?>
      <footer id="footer" class="footer widget-area cf" role="footer">
        <div class="widget-area--inner w">

          <div class="footer--widgets--container cf">
            <?php dynamic_sidebar( 'footer' ); ?>
          </div>

          <?php do_action( 'drivkraft_after_footer_widgets' ); ?>

        </div>
      </footer>
    <?php } ?>

    <?php do_action( 'drivkraft_after_footer' ); ?>

    </div>

    <?php do_action( 'drivkraft_before_body_tag_close' );
    wp_footer(); ?>

  </body>
</html>
