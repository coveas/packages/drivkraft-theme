  <?php if ( is_active_sidebar( 'filter' ) ) : ?>

  <div class="extended--filter">

    <div class="extended--filter--toggle">
      <?php _e( 'Filter products', 'drikraft_theme' );?>
      <svg width="11" height="12" viewBox="218 471 11 12" xmlns="http://www.w3.org/2000/svg"><path d="M227 476h-1v2h1v5h1v-5h1v-2h-1v-5h-1v5zm-4-3h-1v2h1v8h1v-8h1v-2h-1v-2h-1v2zm-3 6v-8h-1v8h-1v2h1v2h1v-2h1v-2h-1z" fill="#FFF" fill-rule="evenodd"/></svg>
    </div>

    <div id="filters" class="extended--filter--list">

      <?php dynamic_sidebar( 'filter' ); ?>

    </div>

  </div>

  <?php endif; ?>