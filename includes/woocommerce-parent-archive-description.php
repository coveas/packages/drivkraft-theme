<?php if ( is_product_category() ) :
  /* Get the category Image if available */
  $image = drivkraft__Woocommerce::woocommerce_category_image(); ?>

<div class="parent--archive--description cf" style="background-image: url( '<?php echo $image; ?>' )">

  <div class="parent--archive--description--container cf w">

    <?php do_action( 'drivkraft_before_inside_archive_header' ); ?>

    <div class="parent--archive--description--content">
      <h1><?php woocommerce_page_title(); ?></h1>

      <?php /* Descriptions */

        if ( is_product_taxonomy() ) {
          $term = get_queried_object();

          if ( $term && ! empty( $term->description ) ) {
            echo '<div class="term-description">' . wc_format_content( $term->description ) . '</div>'; // WPCS: XSS ok.
          }
        }

        // woocommerce_taxonomy_archive_description(); // Archive desc
        // woocommerce_product_archive_description(); // Product desc
      ?>

      <ul class="billboard__subcategories">
        <?php
        foreach ( $children as $child ) {
          $link = get_term_link( $child->slug, $child->taxonomy );
          echo '<li class="billboard__subcategories--item">';
          echo '<a class="billboard__subcategories--link" href="'. $link .'">'.$child->name.'</a>';
          echo '</li>';
        } ?>
      </ul>
    </div>

    <?php do_action( 'drivkraft_after_inside_archive_header' ); ?>

  </div>

</div>

<?php endif; ?>

<style>.breadcrumbs__parent{display: none;}</style>
