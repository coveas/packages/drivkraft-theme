<?php if ( is_product_category() ) : ?>
<div class="archive--description cf">
  <div class="archive--description--container cf w">

    <?php do_action( 'drivkraft_before_inside_archive_header' ); ?>

    <?php /* Category Image */
    $image = drivkraft__Woocommerce::woocommerce_category_image() ?>
    <img class="term-description--image" src="<?php echo $image; ?>" alt="" />

    <div class="archive--description--content">

      <h1><?php woocommerce_page_title(); ?></h1>

      <?php /* Descriptions */

        if ( is_product_taxonomy() ) {
          $term = get_queried_object();

          if ( $term && ! empty( $term->description ) ) {
            echo '<div class="term-description">' . wc_format_content( $term->description ) . '</div>'; // WPCS: XSS ok.
          }
        }

        // woocommerce_taxonomy_archive_description(); // Archive desc
        // woocommerce_product_archive_description(); // Product desc
      ?>
    </div>

    <?php do_action( 'drivkraft_after_inside_archive_header' ); ?>

  </div>
</div>
<?php endif; ?>
