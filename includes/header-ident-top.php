<header id="header__peak" class="header">

  <div class="header--wrap w cf">

    <!-- Washing Line -->
    <div class="header--line">

      <div class="<?php echo apply_filters( 'craft_header_top_left_classes', 'header--top--left header--left' ) ?>">
        <?php do_action( 'drivkraft_header_top_left' ); ?>
      </div>

      <div class="<?php echo apply_filters( 'craft_header_top_center_classes', 'header--top--center header--logo' ) ?>">
        <?php do_action( 'drivkraft_header_top_center' ); ?>
      </div>

      <div class="<?php echo apply_filters( 'craft_header_top_right_classes', 'header--top--right header--account' ) ?>">
        <?php do_action( 'drivkraft_header_top_right' ); ?>
      </div>

    </div>
  </div>
</header>
