<?php

/*
  When the cart is empty, these will display.
  You can edit this message in the header class
 */

do_action( 'drivkraft_before_empty_cart' );

do_action( 'drivkraft_empty_cart_message' );

do_action( 'drivkraft_after_empty_cart' );

?>
