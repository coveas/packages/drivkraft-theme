<?php if( is_product_category() ) : ?>

  <div class="filter--bar cf">

    <div class="filter--bar--container w cf">

      <?php
      /**
       * Deprecated drivkraft__extended-filter filter
       * will be moved into a function on next version
       */
      do_action( 'drivkraft__extended-filter' );
      do_action( 'drivkraft-before-filter' ); ?>

      <div class="filter--bar--ordering">
        <?php /* woocommerce_result_count(); */
          woocommerce_catalog_ordering(); ?>
      </div>

      <?php do_action( 'drivkraft-after-filter' ); ?>

    </div>

  </div>

<?php endif; ?>