    <header id="header__base" class="header">

      <div class="header--wrap w cf">

        <!-- Lower level header -->
        <div class="header--base">

          <!-- Search contained within the header -->
          <div class="<?php echo apply_filters( 'craft_header_bottom_left_classes', 'header--bottom--left header--search' ); ?>">
            <?php do_action( 'drivkraft_header_bottom_left' ); ?>
          </div>

          <!-- Logo: Our brand can be summed up with this icon -->
          <div class="header--logo--mobile">
            <?php do_action( 'drivkraft_header_logo' ); ?>
          </div>

          <!-- The main navigation -->
          <?php do_action( 'drivkraft_header_bottom_center' ); ?>

          <!-- The cart, both tiny and mighty -->
          <div class="<?php echo apply_filters( 'craft_header_bottom_right_classes', 'header--bottom--right header--cart' ); ?>">
            <?php do_action( 'drivkraft_header_bottom_right' ); ?>
          </div>
        </div>
      </div>
    </header>