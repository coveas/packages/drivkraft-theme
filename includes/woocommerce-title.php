<!-- Lets take a look at this '<?php echo get_the_title(); ?>' product shall we-->
<div class="single-product__header cf">

  <div class="single-product__header--title">

    <?php do_action( 'drivkraft_single_product_before_title' ); ?>

    <?php woocommerce_template_single_title(); ?>

    <?php do_action( 'drivkraft_single_product_after_title' );  ?>

    <div class="single-product__header--rating" >

      <?php woocommerce_template_single_rating() ?>

    </div>

  </div>

  <div class="single-product__header--price">

    <?php do_action( 'drivkraft_single_product_before_title_price' ); ?>

    <?php woocommerce_template_single_price() ?>

    <?php drivkraft__Woocommerce::drivkraft_payment_information() ?>

    <?php do_action( 'drivkraft_single_product_after_title_price' ); ?>

  </div>

</div>
