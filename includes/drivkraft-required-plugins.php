<?php

require_once dirname( __FILE__ ) . '/../classes/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'drivkraft_theme_register_required_plugins' );

function drivkraft_theme_register_required_plugins() {
  /*
   * Array of plugin arrays. Required keys are name and slug.
   * If the source is NOT from the .org repo, then source is also required.
   */
  $plugins = array(

    // Include ACF-Pro with Drivkraft
    array(
      'name'               => 'Advanced Custom Fields Pro', // The plugin name.
      'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
      'source'             => dirname( __FILE__ ) . '/plugins/advanced-custom-fields-pro.zip', // The plugin source.
      'required'           => true, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => 'acf_init', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ),

    // Woocommerce Waitlist
    array(
      'name'               => 'Woocommerce waitlist', // The plugin name.
      'slug'               => 'woocommerce-waitlist', // The plugin slug (typically the folder name).
      'source'             => dirname( __FILE__ ) . '/plugins/woocommerce-waitlist.zip', // The plugin source.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
    ),

    // Woocommerce Dynamic Pricing
    array(
      'name'               => 'Woocommerce Dynamic Pricing', // The plugin name.
      'slug'               => 'woocommerce-dynamic-pricing', // The plugin slug (typically the folder name).
      'source'             => dirname( __FILE__ ) . '/plugins/woocommerce-dynamic-pricing.zip', // The plugin source.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
    ),

    array(
      'name'               => 'Woocommerce Live Search', // The plugin name.
      'slug'               => 'searchwp-live-ajax-search', // The plugin slug (typically the folder name).
      'source'             => dirname( __FILE__ ) . '/plugins/searchwp-live-ajax-search.1.1.7.zip', // The plugin source.
      'required'           => false, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
    ),

    /* Improving the search of woocommerce with this plugin. The pro version is
    available, but is something that we need to charge for, as it's not free. */
    array(
      'name'        => 'Relevanssi',
      'slug'        => 'relevanssi',
      'required'    => false,
      'is_callable' => 'relevanssi_init',
    ),

    // We need columns for ACF. Otherwise there's
    // going to be a bunch of random words littered
    // thoughout the site.
    array(
      'name'      => 'ACF Column fields',
      'slug'      => 'acf-column-field',
      'required'  => true,
    ),
  );

  $config = array(
    'id'           => 'drivkraft-theme',        // Unique ID for hashing notices for multiple instances of TGMPA.
    'default_path' => '',                        // Default absolute path to bundled plugins.
    'menu'         => 'tgmpa-install-plugins',   // Menu slug.
    'parent_slug'  => 'plugins.php',            // Parent menu slug.
    'capability'   => 'manage_options',          // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
    'has_notices'  => true,                      // Show admin notices or not.
    'dismissable'  => false,                      // If false, a user cannot dismiss the nag message.
    'dismiss_msg'  => 'Drivkraft is powered by ACF – If it isn\'t installed, you\'re going to run into a few issues with content management',                        // If 'dismissable' is false, this message will be output at top of nag.
    'is_automatic' => false,                     // Automatically activate plugins after installation or not.
    'message'      => '',                        // Message to output right before the plugins table.

  );

  tgmpa( $plugins, $config );
}
