<?php

// We nee ACF to run this, so if it doesn't exist,
// we're going to place the default menu in place.
if ( ! class_exists( 'acf' ) ) {
  wp_nav_menu(
    [
      'container'      => false,
      'menu_class'     => 'nav-menu-primary nav-menu',
      'theme_location' => 'primary',
      'fallback_cb'    => 'drivkraft_no_menu_available',
    ]
  );
  // No ACF? End this now.
  return;
}


/* Because this file gets complex,
classes can be assigned here: */

$menu_classes                           = ' mega-menu';
$menu_item_classes                      = ' mega-menu--item menu-item';
$menu_item_sale_classes                 = ' mega-menu--item--sale';
$menu_item_link_classes                 = ' mega-menu--item--custom--link';
$menu_item_custom_category_list_classes = ' mega-menu--item--custom--list';

/* This is a custom menu that gets saved to
a transient when it's all done. This way, there
are no requests too large that we can't fetch once
and cache for the rest of the day. */

$load_main_menu = get_transient( 'craft_mega_menu' );

/**
 * Create a global array of all the sub menu
 * items that can be assigned to each menu item
 */
do_action( 'mega_menu_sub_menu_compile' );

if ( false === ( $load_main_menu ) ) {

  /* START BUILDING THIS TRANSIENT! */ ob_start();

  if ( have_rows( 'menu_item', 'option' ) ) : ?>

    <ul id="nav" class="<?php echo apply_filters( 'mega_menu_classes', $menu_classes ); ?> accordion">

    <?php while ( have_rows( 'menu_item', 'option' ) ) : the_row();

      /* Product category */
      if (  get_row_layout() == 'product_category' ) :

        /* Pull in the fields! */
        $acf_fields = [
          'category',
          'menu_item_name',
          'list_sub_categories',
          'manually_pick_sub_categories',
        ];

        // Assign them to a variable
        foreach ( $acf_fields as $key ) {
          @$$key = get_sub_field( $key );
        }

        // If category is no null
        if ( $category ) :

          // Overwrites title + sub-cats + brands
          if ( empty( $menu_item_name ) ) {
            $menu_item_name = $category->name;
          }

          $subcat_terms = get_term_children( $category->term_id, $category->taxonomy );

          // No terms? Skip this item
          if ( empty( $subcat_terms ) ) {
            continue;
          }

          $all_possible_terms = get_terms( [
            'include' => $subcat_terms,
            'orderby' => 'count',
            'order'   => 'desc'
          ]);
          ?>
          <li class="<?php echo apply_filters( 'mega_menu_item_classes', $menu_item_classes, $category ); if ( ! empty( $subcat_terms ) ) { echo ' mega-menu--item-has-children'; } ?>">
            <a href="<?php echo get_term_link( $category->slug,  $category->taxonomy ); ?>" class="opener"><?php echo $menu_item_name; ?></a>
            <?php $i = 0;
            if ( ! empty( $all_possible_terms ) ) { ?>
              <div class="sub-menu">
                <div class="sub-menu--container">
                  <div class="sub-menu--column">
                    <ul class="sub-menu--list">
                    <?php
                    foreach ( $all_possible_terms as $term ) {
                      // Count to 8, then stop
                      if ( 7 == $i % 8 ) { ?>
                        </ul>
                      </div>
                      <div class="sub-menu--column">
                        <ul class="sub-menu--list">
                      <?php }
                      echo '<li><a href="' . get_term_link( $term->term_id, $category->taxonomy ) . '">' . $term->name . '</a></li>';
                      $i++;
                    } ?>
                    </ul>
                </div>
              </div>
              <?php do_action( 'mega_menu_after_sub_menu', $menu_item_name ); ?>
            </div>
            <?php } ?>

          </li>

        <?php endif;

        // Product category

    elseif ( get_row_layout() == 'sale_items' ) :

      // Sale category
      // Collect all options
      $sale_title        = get_sub_field( 'sale_name' );
      $sale_category     = get_sub_field( 'sale_categories' );
      ?>
      <li class="<?php echo apply_filters( 'mega_menu_item_classes', $menu_item_classes, $sale_category ) . ' ' . $menu_item_sale_classes; ?> menu--item--sale <?php echo ( ! empty( $sale_category ) ) ? 'mega-menu--item-has-children' : '' ?>">
        <a class="opener" href="/?orderby=on_sale&post_type=product" ><?php echo $sale_title; ?></a>
        <?php if ( ! empty( $sale_category ) ) : ?>
        <div class="sub-menu">
          <div class="sub-menu--container">
            <div class="sub-menu--column">
              <ul class="sub-menu--list">
                  <?php $i = 0;
                  foreach ( $sale_category as $sale_cat ) {
                    // Count to 10, then stop
                    if ( 7 == $i % 8 ) { ?>
                      </ul>
                    </div>
                    <div class="sub-menu--column">
                      <ul class="sub-menu--list">
                    <?php }
                    echo '<li><a href="' . get_term_link( $sale_cat->term_id, $sale_cat->taxonomy ) . '?sale=show">' . $sale_cat->name . '</a></li>';
                    $i++;
                  } ?>
              </ul>
            </div>
          </div>
          <?php do_action( 'mega_menu_after_sub_menu', $sale_title ); ?>
        </div>
      <?php endif; ?>
      </li>


    <?php elseif ( get_row_layout() == 'page' ) :

        /* Pull in the fields! */
        $acf_fields = [
          'page_name',
          'page',
          'sub_pages', // Sub pages is new!
        ];
        // Assign them to a variable
        foreach ( $acf_fields as $key ) {
          @$$key = get_sub_field( $key );
        }
    ?>

      <li class="<?php echo apply_filters( 'mega_menu_item_classes', $menu_item_classes, $page ) . ' ' . $menu_item_link_classes; if ( $sub_pages ) : echo ' mega-menu--item-has-children'; endif; ?> menu--item--link <?php do_action( 'mega_menu_page_item_class', $item_name = ( $page_name ) ? $page_name : get_the_title( $page ) ); ?>">
        <a href="<?php echo get_the_permalink( $page ); ?>" class="opener"><?php echo ( $page_name ) ? $page_name : get_the_title( $page ); ?></a>
        <?php if ( $sub_pages ) {
          /* If there are pages available, lets make ourselves a tidy sub menu */
            $i = 0; ?>
            <div class="sub-menu">
              <div class="sub-menu--container">
                <div class="sub-menu--column">
                  <ul class="sub-menu--list">
                  <?php
                  foreach ( $sub_pages as $page_id ) {
                    if ( 7 == $i % 8 ) { ?>
                  </ul>
                </div>
                <div class="sub-menu--column">
                  <ul class="sub-menu--list">
                    <?php }
                    echo '<li><a href="' . get_the_permalink( $page_id ) . '">' . get_the_title( $page_id ) . '</a></li>';
                    $i++;
                  } ?>
                  </ul>
              </div>
            </div>
            <?php do_action( 'mega_menu_after_sub_menu', $page_name ); ?>
          </div>
        <?php }
        do_action( 'mega_menu_page_item_children', $item_name = ( $page_name ) ? $page_name : get_the_title( $page ) ); ?>
      </li>

   <?php elseif ( get_row_layout() == 'external_link' ) :
      /* External link */
      $link_array = get_sub_field( 'link' );
      $title      = $link_array['title'];
      $url        = $link_array['url'];
      $target     = $link_array['target'];
    ?>
      <li class="<?php echo apply_filters( 'mega_menu_item_classes', 'mega-menu--item menu-item external_link_li' ) . ' ' . $menu_item_link_classes; ?> menu--item--link">
        <a class="external_link" href="<?php echo $url; ?>" target="<?php echo $target; ?>"><?php echo $title; ?></a>
      </li>
      <style>a.external_link:hover{cursor:pointer;}</style>
    <?php elseif ( get_row_layout() == 'organised_menu' ) :

      /* Pull in the fields! */
      $acf_fields = [
        'menu_title',
        'sub_items',
        //sub
        'sub_title', // text
        'sub_title_link', // tax
        'categories', // tax
      ];
      // Assign them to a variable
      foreach ( $acf_fields as $key ) {
        @$$key = get_sub_field( $key );
      }


      $class = $menu_item_classes . ' ' . $menu_item_link_classes;
      if ( $sub_items ) {
        $class .= ' mega-menu--item-has-children';
      }
      ?>

      <li class="<?php echo apply_filters( 'mega_menu_item_classes', $class, $categories ); ?> menu--item--link extended--sub-menu">

        <a href="#" class="opener"><?php echo $menu_title; ?></a>

        <?php if ( $sub_items ) { ?>

          <div class="sub-menu sub-menu--extended">
            <div class="sub-menu--wrapper w cf">

              <div class="sub-menu--container">
                <?php foreach ( $sub_items as $item ) { ?>
                  <div class="sub-menu--column">
                    <ul class="sub-menu--list">
                    <?php
                    $column_title = $item['sub_title'];
                    $column_link  = $item['sub_title_link'];

                    // no link? No problem
                    if ( ! term_exists( $column_link ) ) {
                      $column_link = '#';
                    } else {
                      $column_link = get_term_link( $column_link, 'product_cat' );
                    }

                    // No title? No troblem
                    if ( $column_title ) {
                      echo '<li class="sub-menu--list--title"><a href="' . $column_link . '"><strong>' . $column_title . '</strong></a></li>';
                    }

                    $categories = $item['categories'];

                    // Sub categories? No Cobblem... (cough.)
                    if ( ! empty( $categories ) ) {
                      // If the categories array returns something, loop through that
                      foreach ( $categories as $category_id ) {

                        // Failsafe, if there isn't an ID, move on
                        if ( empty( $category_id ) ) {
                          continue;
                        }

                        //  Check if theTerm exists.
                        if ( ! term_exists( $category_id, 'product_cat' ) ) {
                          continue;
                        }

                        $cat_details = get_term( $category_id, 'product_cat' );

                        // Check get_term didn't retrun null
                        if ( ! $cat_details ) {
                          continue;
                        }

                        $cat_title   = $cat_details->name;
                        $cat_link    = get_term_link( $category_id, 'product_cat' );
                        echo '<li><a class="menu--item--sub--child" href="' . $cat_link . '">' . $cat_title . '</a></li>';
                      } ?>
              <?php } ?>
                    </ul>
                  </div>
          <?php } ?>
              </div>
              <?php do_action( 'mega_menu_after_sub_menu', $menu_title ); ?>
            </div>
          </div>
        <?php } ?>
      </li>


    <?php endif;
    endwhile; ?>

    <?php do_action( 'mega_menu_additional_items' ); ?>

    </ul>


  <?php else :

    // So, if the ACF thing fails, we're going to fall back onto the standard menu.
    // Might not looks as good, and won't have all the bells and whistles as my fancy menu
    // but what else can we do?

    $menu = wp_nav_menu( array(
      'container'      => false,
      'menu_class'     => 'nav-menu-primary nav-menu',
      'theme_location' => 'primary',
      'fallback_cb'    => 'drivkraft_no_menu_available',
    ) );

  endif;  ?>

  <?php

  $load_main_menu = ob_get_clean();

  // Set the transient to die after 10 minutes
  set_transient( 'craft_mega_menu' , $load_main_menu, 10 * MINUTE_IN_SECONDS );

} // End of transient.

echo $load_main_menu;
