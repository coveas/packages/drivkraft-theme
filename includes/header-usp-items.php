<?php
/* Options fields in Header */
// Since the layout depends on the
// structure of this section, we
// will print the div without content
// if we need to.
//
// usp icon list as transient
$usp_list = get_transient( 'craft_header_usps' );
// If there isn't a transient, capture the html
if ( ! $usp_list ) {

  ob_start();

    echo '<ul class="header--usp">';

    if ( have_rows( 'usps', 'option' ) ) :

      while ( have_rows('usps', 'option') ) : the_row();

        $statment = get_sub_field( 'statement' );
        $link     = get_sub_field( 'optional_link' );
        echo '<li class="header--usp--item">';
        echo ( $link ) ? '<a href="' . $link . '">' : '';
        echo  $statment;
        echo ( $link ) ? '</a>' : '';
        echo '</li>';

      endwhile;

    endif;

    echo '</ul>';

  $usp_list = ob_get_clean();

  // Set the transient for next load
  set_transient( 'craft_header_usps' , $usp_list, 12 * MONTH_IN_SECONDS );
}

// Always output a list
echo $usp_list;