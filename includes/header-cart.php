<?php

/* If woocommerce isn't active, stop. */
if ( ! class_exists( 'woocommerce' ) ) {
  return;
}

/* Include Woocommerce in this cart */ ?>
<div class="header--cart--link">
  <?php

  do_action( 'craft_before_cart_message' );

  $cart_url = wc_get_cart_url();
  $size     = sizeof( WC()->cart->cart_contents );
  $aria     = __( 'Your shopping cart contains %d items', 'drivkraft-theme' );
  echo '<a aria-label="' . sprintf( $aria, $size ) . '" href="' . $cart_url . '">';
    echo '<span class="counted"></span>';
    echo apply_filters( 'drivkraft_header_cart_button_text', '<span class="header--cart--text">' . __( 'Cart', 'drivkraft-theme' ) . '</span>' );
  echo '</a>'; ?>

</div>
<?php
/**
 * Display the hover for the cart. However, enable the filter to disable the cart.
 */
if ( apply_filters( 'drivkraft_display_cart_hover', false ) === false ) {
  ?>
  <div class="header--cart--container">
  <?php get_template_part( 'includes/header-cart--contents' ); ?>
  </div>
  <?php
}
