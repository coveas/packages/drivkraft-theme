<div class="header--cart--contents">
<?php if ( sizeof( WC()->cart->cart_contents ) > 0 ) : ?>
  <div class="header--cart--title">
    <h3><?php echo __( 'Cart', 'drivkraft-theme' ); ?></h3>
  </div>
<?php endif; ?>

<div class="header--cart--scroll">
  <table class="header--cart--list">
    <?php
      /*
      Show a miniture cart. This is going to be written as a table.
      I tried using normal markup, but it requires too many styles
      to get it working nicely. If we use a table, the layout is solved
      without the need of styles that need overwritting.
      */

    $visible_item_count = 0;

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
      $visible_item_count++;
      $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
      $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
      if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) { ?>

        <tr class="header--cart--item">


          <td class="header--cart--item--img">
            <?php // Product Thumbail
            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
            if ( ! $_product->is_visible() ) { echo $thumbnail; }
            else { printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail ); }
            ?>
          </td>

          <td class="header--cart--item--details">
            <!-- <a href="<?php echo esc_url( $_product->get_permalink( $cart_item ) ); ?>" class="header--cart--item--name"> -->
              <strong class="header--cart--item--title"><?php if ( ! $_product->is_visible() ) {
                echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
              } else {
                echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_name() ), $cart_item, $cart_item_key );
              } ?></strong>
              <?php /* Meta data */
              if ( wc_get_formatted_cart_item_data( $cart_item ) ) {
                echo apply_filters( 'woocommerce_cart_item_meta_data' , '<span class="header--cart--item--meta">' . wc_get_formatted_cart_item_data( $cart_item ) . '</span>', $cart_item, $cart_item_key );
              } ?>
            <!-- </a> -->
          </td>

          <td class="header--cart--item--cost">
            <div class="header--cart--item--price">
              <?php echo '<div class="item--quantity">&times ' . $cart_item['quantity'] . '</div>'; ?>
              <?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>
            </div>

              <?php
                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                  '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="&times; %s">%s</a>',
                  esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                  __( 'Remove this item', 'woocommerce' ),
                  esc_attr( $product_id ),
                  esc_attr( $_product->get_sku() ),
                  '<div class="remove-text">' . __( 'Remove', 'drivkraft-theme' ) . '</div>'
                ), $cart_item_key );
              ?>

          </td>

        </tr>

      <?php  }
  }
    /* No products? No problem, show an empty cart */
  if ( 0 === $visible_item_count ) {
    get_template_part( 'includes/empty', 'cart-message' );
  } ?>
  </table>
</div>

  <?php if ( sizeof( WC()->cart->cart_contents ) > 0 ) : ?>

    <div class="header--cart--subtotal">
      <span class="header--cart--sub"><?php _e( 'Subtotal', 'drivkraft-theme' ); ?></span>
      <span class="header--cart--sub"><?php echo WC()->cart->get_cart_total(); ?></span>
    </div>

    <div class="header--cart--checkout"><?php
      do_action( 'drivkraft_header_cart_buttons' );
      echo apply_filters( 'drivkraft_header_cart_button_cart', sprintf( '<a class="header--cart--btn btn btn-primary btn--cart" href="%1$s" title="%2$s">%2$s</a>', wc_get_cart_url(), __( 'Cart', 'drivkraft-theme' ) ) );
      echo apply_filters( 'drivkraft_header_cart_button_checkout', sprintf( '<a class="header--cart--btn btn btn-secondary btn--checkout" href="%1$s" title="%2$s">%2$s</a>', wc_get_checkout_url(), __( 'Checkout', 'drivkraft-theme' ) ) );
    ?></div>
  <?php endif; ?>
</div>
