<?php
$placeholder = apply_filters( 'craft_search_placeholder', _x( 'Search', 'The placeholder of the search', 'drivkraft-theme' ) ); ?>
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="searchform--inner cf">
      <label for="s" class="screen-reader-text"><?php _e( 'Search for:', 'drivkraft-theme' ); ?></label>
      <input type="search" id="s" name="s" value="" placeholder="<?php echo $placeholder; ?>" />
      <input type="hidden" name="post_types" value="product" />
      <input type="submit" id="searchsubmit" value="<?php _e( 'Search', 'drivkraft-theme' ); ?>">
    </div>
</form>

<?php /* if( is_search() ) { ?>

  <script type="text/javascript">
    jQuery(document).ready(function($){
      // Bind the submit event for your form
      $('.searchform').submit(function( e ){
          // Stop the form from submitting
        e.preventDefault();

          // Get the search term
        var term = $('#s').val();

          // Get post type
        var pt = $('.post_type').val();

          // Make sure the user searched for something
        if ( term ){
          $.get( '/', { s: term, post_type: pt }, function( data ) {
                  // Place the fetched results inside the #content element
            var $what = '.products';
            var $where = '.content--woo';
            console.log(data);
            $( $where ).html( $(data).find( $what ) );
          });
        }
      });
    });
  </script>
<?php } */ ?>