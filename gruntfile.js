module.exports = function( grunt ) {
  var path = require('path');
  const sass = require('node-sass');

  // Statistics about tasks
  // require('time-grunt')(grunt);

  // Loading everything into grunt
  require('load-grunt-config')(grunt, {
    configPath: path.join(process.cwd(), 'grunt'),

    // Loading tasks
    jitGrunt: {
      customTasksDir: 'grunt',
      staticMappings: {
        browserSync  : 'grunt-browser-sync',
        concat       : 'grunt-contrib-concat',
        concurrent   : 'grunt-concurrent',
        imagemin     : 'grunt-contrib-imagemin',
        sass         : 'grunt-sass',
        uglify       : 'grunt-contrib-uglify',
        watch        : 'grunt-contrib-watch',
        csso         : 'grunt-csso',
        newer        : 'grunt-newer',
        postcss      : 'grunt-postcss',
        prompt       : 'grunt-prompt',
        sass_globbing: 'grunt-sass-globbing',
        stripmq      : 'grunt-stripmq',
        }
    },

    // Custom directories
    data: {
      proxy            : 'drivkraft.dev:8080',
      scssPartials     : 'assets/scss/partials/',
      scssMainFileName : 'style',
      cssMainFileName  : 'style',
      scssLoginFileName: 'login',
      cssLoginFileName : 'login',
      scssEmailFileName: 'email',
      cssEmailFileName : 'email',
      scssDir          : 'assets/scss/',
      cssDir           : 'assets/css/',
      jsDir            : 'assets/js/',
      imgDir           : 'assets/images/'
    }

  });

};