<?php get_header(); ?>


<div id="content" class="cf w">

  <main id="main" class="columns-4 cf search--archive" role="main">

    <h2 class="search--title"><?php _e( 'Search Results for', 'drivkraft-theme' ); ?> '<?php echo get_search_query(); ?>'</h2>

      <?php do_action( 'drivkraft_before_search_results' ); ?>

      <?php
      // Store the post type from the URL string
      $post_type = @$_GET['post_type'];
      if ( ! $post_type ) {
        $post_type = @$_GET['post_types'];
      }

      // Check to see if there was a post type in the
      // URL string and if a results template for that
      // post type actually exists
      if ( isset( $post_type ) && locate_template( 'search-' . $post_type . '.php' ) ) {
        // if so, load that template
        get_template_part( 'search', $post_type );
        // and then exit out
        exit;
      }

      $last_type = '';
      $typecount = 0;
      if ( have_posts() ) :

        if ( isset( $post_type ) && $post_type == 'product' ) {
          echo '<ul class="products product-category search--archive--list">';
        }

        while ( have_posts() ) : the_post();


          if ( isset( $post_type ) && $post_type == 'product' ) {
            wc_get_template_part( 'content', 'product' );
          } else { ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class( 'article' ); ?>>

            <header class="article--header">

              <?php do_action( 'drivkraft_before_article_title' ); ?>

              <h1 class="article--title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

              <?php do_action( 'drivkraft_after_article_title' ); ?>

            </header>

            <section class="article--content cf">

              <?php the_excerpt(); ?>

              <?php do_action( 'drivkraft_after_article_excerpt' ); ?>

            </section>

          </article>

        <?php }

        endwhile;

        if ( get_post_type() == 'product' ) {
          echo '</ul>';
        }

        /* If woocommerce is active, use the pagination. */
        if ( class_exists( 'woocommerce' ) ) {
          woocommerce_pagination();
        } else {
          the_posts_pagination();
        }

        else :

          /* No search results? Why show an empty page that says nothing.
          Lets import the best sellers, the top rated, the best of the
          best we have. Sales are sales, and you can't sell a blank page. */
          echo '<h1 class="no-results-title">' . __( 'Sorry, nothing matches your search', 'drivkraft-theme' ) . '</h1>';
          if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
            echo do_shortcode( '[best_selling_products per_page="8"]' );
          }

        endif; ?>

  </main>

</div>

<?php get_footer(); ?>
