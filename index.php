<?php get_header(); ?>

<div id="content" class="cf w">

  <main id="main" class="inx content--inx">

    <?php do_action( 'drivkraft_archive_before' ); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class( 'article' ); ?>>

        <header class="article--header">

          <?php do_action( 'drivkraft_before_article_title' ); ?>

          <h1 class="article--title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

          <?php do_action( 'drivkraft_after_article_title' ); ?>

        </header>

        <section class="article--content cf">

          <?php the_excerpt(); ?>

          <?php do_action( 'drivkraft_after_article_excerpt' ); ?>

        </section>

      </article>

    <?php endwhile;

    do_action( 'drivkraft_maybe_pagination' );

    endif; ?>

    <?php do_action( 'drivkraft_archive_after' ); ?>

  </main>

  <?php do_action( 'drivkraft_archive_sidebar' ); ?>

</div>

<?php get_footer(); ?>
