<?php get_header(); ?>


<div id="content" class="cf w">

  <div class="brand-description--header cf">
    <h2 class="brand--archive--title search--title"><?php
    global $wp_query;
    $brand = $wp_query->get_queried_object();
    $name = $brand->name;

    $logo_id = get_field( 'logo', $brand->taxonomy . '_' .$brand->term_id );
    $svg_id = get_field( 'svg_logo', $brand->taxonomy . '_' .$brand->term_id );
    // If there's an SVG, we're going to output that
    if ( $svg_id ) {
      $svg_img = wp_get_attachment_image_src( $svg_id, 'full' );
      echo "<img src='$svg_img[0]' alt='$name' width='250' />";
    } elseif ( $logo_id ) {
      // If there's no SVG, try for an image
      $logo_img = wp_get_attachment_image_src( $logo_id, 'medium' );
      echo "<img src='$logo_img[0]' alt='$name' width='250' />";
    } else {
      // Still no image? Rollback to Text based name
      echo $name;
    }
    ?></h2>

    <?php
    // Echo the description
    if ( $brand->description ) {
      echo '<p class="brands--related--description">' . $brand->description . '</p>';
    }
    ?>
  </div>
  <main id="main" class="content--woo brand--archive search--archive cf">

  <?php if ( have_posts() ) : ?>

      <?php
        /**
         * woocommerce_before_shop_loop hook.
         *
         * @hooked woocommerce_result_count - 20
         * @hooked woocommerce_catalog_ordering - 30
         */
        do_action( 'woocommerce_before_shop_loop' );
      ?>

      <?php woocommerce_product_loop_start(); ?>

        <?php woocommerce_product_subcategories(); ?>

        <?php while ( have_posts() ) : the_post(); ?>

          <?php wc_get_template_part( 'content', 'product' ); ?>

        <?php endwhile; // end of the loop. ?>

      <?php woocommerce_product_loop_end(); ?>

      <?php
        /**
         * woocommerce_after_shop_loop hook.
         *
         * @hooked woocommerce_pagination - 10
         */
        do_action( 'woocommerce_after_shop_loop' );
      ?>

    <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

      <?php wc_get_template( 'loop/no-products-found.php' ); ?>

    <?php endif; ?>

  </main>

  <?php
    /**
     * Craft hook for possible woocommerce_sidebar hook.
     *
     * @hooked woocommerce_get_sidebar - 10
     */
    do_action( 'craft_taxonomy_sidebar' );
  ?>

</div>

<?php get_footer(); ?>
