jQuery( function( $ ) {
  function restyle_kco_elements() {
    if ( $( '.enter-postcode' ).hasClass( 'checkout-box' ) ) {
      // This function has been run already
      // Usually happens when coupons are added
      return;
    }
    // style elements
    $( '.enter-postcode' ).addClass( 'checkout-box' );

    var coupons = $( '.wrapped--coupon' );
    if ( 2 <= coupons.length ) {
      $( '.wrapped--coupon:eq(0)' ).remove();
    }

    $( 'input.shipping_method' ).attr( 'style', '' );
    $( '.fraktguiden-pickup-point' ).hide();
    $( '.select-shipping-method' ).addClass( 'checkout-box' );
    // Add classes to parent divs
    $( '#klarna-checkout-cart' ).parent().addClass( 'kco-product-list' );
    $( '#kco-totals' ).parent().addClass( 'kco-totals' );
    $( '#klarna-checkout-order-note' ).parent().parent().addClass( 'kco-order-note' );

    // Remove the style on Klarna P
    $( '#kco-page-shipping td p' ).attr( 'style', '' );

    // If there isn't a coupon title...
    if ( ! $( '.wrapped--coupon-title' ).length ) {
      $( '.wrapped--coupon' ).before( '<h5 class="kco-title wrapped--coupon-title">Rabattkoder</h5>' );
    }
    // If there isn't a product list title...
    if ( ! $( '.kco-product-list-title' ).length ) {
      $( '.kco-product-list' ).before( '<h5 class="kco-title kco-product-list-title">Din handlekurv</h5>' );
    }
    // If there isn't a post code title
    if ( ! $( '.bring-enter-postcode-title' ).length ) {
      $( '.bring-enter-postcode' ).before( '<h4 data-order="1" class="kco-title bring-enter-postcode-title">Din informasjon</h4>' );
    }
    // If there isn't a shipping title
    if ( ! $( '.select-shipping-method-title' ).length ) {
      // $( '.select-shipping-method' ).before( '<h4 data-order="2" class="kco-title select-shipping-method-title">Hvordan vil du ha varene levert?</h4>' );
      $ ('.bring-select-shipping h3' ).addClass( 'kco-title select-shipping-method-title' ).attr( 'data-order', '2' );
      $ ('.bring-select-shipping--options' ).addClass( 'checkout-box select-shipping-method' );
    }
    // Show betalling only once
    if ( ! $( '.klarna_checkout-title' ).length ) {
      $( '.klarna_checkout ' ).prepend( '<h4 data-order="3" class="kco-title klarna_checkout-title">Betaling</h4>' );
    }

    var postcode_input = $( '.bring-enter-postcode .input-text' );
    if ( ! postcode_input.val() ) {
      $( '.kco-title:nth-of-type( 1n + 2 ), .select-shipping-method, .wrapped--coupon, .klarna_checkout ' ).hide();
    }
    var coupon_result = $( '#klarna_checkout_coupon_result' );
    if ( coupon_result.length && coupon_result.html() ) {
      $( 'html, body' ).animate( {
          scrollTop: coupon_result.offset().top - 100
      }, 200 );
    }

    var list_container = $( '#shipping_method_clone' ).parent();
    var pickup_list = $( '<div>' ).append( '<strong>Hent varene selv</strong><ul></ul>' );
    var ship_list   = $( '<div>' ).append( '<strong>Få varene levert</strong>', $( '#shipping_method_clone' ) );
    list_container.append( ship_list );
    ship_list.find( 'ul' ).children().each( function() {
      var input = $( this ).find( 'input' );
      if ( ! input.length ) {
        return;
      }
      if ( -1 == input.val().indexOf( ':servicepakke-' ) ) {
        return;
      }
      pickup_list.find( 'ul' ).append( $( this ) );
    } );
    if ( pickup_list.find( 'ul' ).children().length ) {
      list_container.prepend( pickup_list );
    }

    /**
     * Reshuffling around the content of the checkout into sections that allow us to make us of css grids
     */

    // // If Grid is enabled, we're going to Grid this layout
    // if ( Modernizr.cssgrid ) {

    //   $( 'body' ).addClass( 'klarna-grid-enabled' );

    //   // Coupon segment
    //   var coupon_checkout   = $( '.wrapped--coupon-title, .wrapped--coupon' );
    //   $( coupon_checkout ).wrapAll( '<div class="klarna-coupons">');

    //   // List segment
    //   var product_list_checkout   = $( '.kco-product-list-title, .kco-product-list, .kco-totals, .kco-order-note' );
    //   $( product_list_checkout ).wrapAll( '<div class="klarna-floating-products-list">');

    //   // Bring segment
    //   var bring_checkout   = $( '.bring-enter-postcode-title, .bring-enter-postcode, .bring-select-shipping' );
    //   $( bring_checkout ).wrapAll( '<div class="klarna-bring">');
    // }

  } // - restyle_kco_elements

  // The bring module triggers the "updated_checkout" event allowing us to update the layout
  $( 'body' ).on( 'updated_checkout', restyle_kco_elements );
  $( 'body' ).on( 'updated_checkout', adjust_mini_cart );

  restyle_kco_elements();

  function no_shipping_methods() {
    $( '.bring-enter-postcode' ).after( '<div class="select-shipping-method"><p class="warning">Det er ingen tilgjengelige fraktvalg for valgt addresse</p></div>' );
  }

  var cloned_select;
  var move_to_step2 = false;

  // Support for updating mini-cart when using klarna checkout
  var total_quantity = 0;
  function adjust_mini_cart() {
    // Make sure both elements exists before continuing
    if ( ! $( '.header--cart' ).length || ! $( '#klarna-checkout-cart' ).length ) {
      return;
    }
    total_quantity = 0;
    $( '#klarna-checkout-cart .product-name a' ).each( copy_quantities_from_cart_to_mini_cart );
    $( '.header--cart--link .counted' ).text( total_quantity );
    if ( ! total_quantity ) {
      window.location.reload( false );
    }
  }

  function copy_quantities_from_cart_to_mini_cart() {
    var url = $( this ).attr( 'href' );
    var row = $( this ).parent().parent();
    var quantity = row.find( '.product-quantity input' ).val();
    var image_link = $( '.header--cart--item--img a[href="'+ url +'"]' );
    var mini_row = image_link.parent().parent();
    var item_quantity = mini_row.find( '.item--quantity' );
    item_quantity.text( '× '+ quantity);
    total_quantity += parseInt( quantity, 10 );
  }
} );
