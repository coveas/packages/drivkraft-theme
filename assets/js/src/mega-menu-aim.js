// Dependent on Menu-Aim.js in libs this function allows users to be sloppy
// with their navigation. This means they're allowed to hover over other
// items unintentionally and not be punished for it.
jQuery( function( $ ){
  var menu = $( "#nav" );
  menu.menuAim( {
      submenuDirection: "below",
      tolerance: 1,
      // When the menu is entered
      enter: function( a ) {
        $( '.submenu-previously-visible' ).addClass( "submenu-visible" );
      },
      // When user moved to a parent item
      activate: function( a ) {
        $( a ).addClass( "submenu-visible" );
        $( '.submenu-previously-visible' ).removeClass( 'submenu-previously-visible' );
      },
      // When user moved to another parent item
      deactivate: function( a ) {
        $( a ).removeClass( "submenu-visible" );
        $( '.submenu-previously-visible' ).removeClass( 'submenu-previously-visible' );
      },
      // When user leaves the menu entirely
      exitMenu: function( a ) {
        $( ".submenu-visible" ).addClass( 'submenu-previously-visible' ).removeClass( "submenu-visible" );
      },
  } );
});