jQuery( function( $ ) {

  var slideChild  = $( '#slide-list > li' ),
      nextLink    = $( '#slide-next' ),
      prevLink    = $( '#slide-next' ),
      numSlides   = slideChild.length,
      slideWidth  = slideChild.width(),
      currOffset  = 0,
      counter     = 0,
      tempCounter = 1;

  /* Next click */
  nextLink.click( function(e) {
    e.preventDefault();

    if (counter < (numSlides-1)) {
      tempCounter = 1;
      slideChild.each( function() {
        var strCurrLeftVal = $( this ).css('left');
        var intCurrLeftVal = parseInt(strCurrLeftVal.replace('px', ''));
        var newLeftVal     = (intCurrLeftVal - slideWidth) + 'px';
        $( this ).css('left', newLeftVal);
        tempCounter ++;
      });

      currOffset = currOffset + 100;
      tempCounter = 0;
      slideChild.each( function() {
        var slideOffset = (tempCounter - currOffset) + '%';
        $( this ).css('left', slideOffset);
        tempCounter +=100;
      });
      counter++;

    } else {

      tempCounter = 1;
      slideChild.each( function() {
        var strCurrLeftVal = $( this ).css('left');
        var intCurrLeftVal = parseInt(strCurrLeftVal.replace('px', ''));
        var newLeftVal = (intCurrLeftVal + (slideWidth * (numSlides-1))) + 'px';
        $( this ).css('left', newLeftVal);

        tempCounter ++;
      });

      currOffset = 0;
      tempCounter = 0;

      slideChild.each( function() {
        var slideOffset = (tempCounter - currOffset) + '%';
        $( this ).css('left', slideOffset);
        tempCounter +=100;
      });
      currOffset = 0;
      counter = 0;
    }
  });

  /* Previous click */
  prevLink.click( function(e) {
    e.preventDefault();
    if (counter > 0) {
      tempCounter = 1;
      slideChild.each( function() {
        var strCurrLeftVal = $( this ).css('left');
        var intCurrLeftVal = parseInt(strCurrLeftVal.replace('px', ''));
        var newLeftVal = (intCurrLeftVal + slideWidth) + 'px';
        $( this ).css('left', newLeftVal);
        tempCounter ++;
      });
      currOffset = currOffset - 100;
      tempCounter = 0;
      slideChild.each( function() {
        var slideOffset = (tempCounter - currOffset) + '%';
        $( this ).css('left', slideOffset);
        tempCounter +=100;
      });
      counter--;
    }
  });
});