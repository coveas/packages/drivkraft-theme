jQuery( function( $ ) {
  // When Images have loaded
  $( window ).on( 'load', function() {
    // Get all images
    var images = $('li.product img.wp-post-image');
    // Object fit all images
    images.each( objectFitImages );
    // Equalise all images
    // $images.equalise();
  });
});

// Do it once more for luck.
jQuery( function() {
  if ( ! Modernizr.objectfit) {
    objectFitImages()
  }
});