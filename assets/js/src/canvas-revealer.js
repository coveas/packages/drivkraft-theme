jQuery( function( $ ) {

 /* When we click on the cart in the header,
  we're going to slide a navigation item
  from the right side of the screen. */

  canvas_reveal = function ( container, link_element, body ) {
    var element_active = false;
    var toggle_element = function() {
      if ( element_active ) {
        container
          .addClass( 'closed' )
          .removeClass( 'open' );
        $( 'body' )
          .addClass( body + '__inactive' )
          .removeClass( body + '__active' );
        element_active = false;
      }
      else {
        container
          .addClass( 'open' )
          .removeClass( 'closed' );
        $( 'body' )
          .addClass( body + '__active' )
          .removeClass( body + '__inactive' );
        setTimeout( function() {
          element_active = true;
        }, 200 );
      }
    };
    // Open cart
    link_element.on( 'click touch', function( e ) {
      e.preventDefault();
      e.stopPropagation();
      toggle_element();
    } );

    // Close cart
    $( document ).on( 'click touch', function() {
      if ( true == element_active ) {
        toggle_element();
      }
    } );

    container.on( 'click touch', function( e ) {
      e.stopPropagation();
    } );
  }
} );
