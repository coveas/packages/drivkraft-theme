/* function - container, link_element, body */
jQuery( function( $ ) {
  canvas_reveal(
    $( '.header--navigation' ), // Off-canvas element
    $( '.button-menu' ),
    'nav' // Trigger
  );

  // Cart
  canvas_reveal(
    $( '.header--cart--container' ), // Off-canvas element
    $( '.header--cart--link a' ),
    'cart' // Trigger
  );

  // Filters
  canvas_reveal(
    $( '.extended--filter--list' ), // Off-canvas element
    $( '.extended--filter--toggle' ),
    'filter' // Trigger
  );

  // Filters
  canvas_reveal(
    $( '.super__menu--item.kids > .hidden__dragon' ),
    $( '.super__menu--item.kids > a.super__menu--item--link' ), // Off-canvas element
    'dragon_awake' // Trigger
  );

  // Mobile Search
  canvas_reveal(
    $( '.searchform' ), // Off-canvas element
    $( '.search--trigger' ),
    'search' // Trigger
  );

  // When search is triggered, focus
  $( ".search--trigger" ).click( function() {
    $( "#s" ).focus();
  });

  // Mobile Search
  $( '.footer__widget' ).each( function(){
    canvas_reveal(
      $( this ), // Off-canvas element
      $( this ).find( '.footer__widget--title' ),
      'footer--widget' // Trigger
    );
  });
} );
