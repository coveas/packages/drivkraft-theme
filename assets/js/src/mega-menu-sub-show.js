jQuery( function( $ ) {
  $menu = $('.mega-menu--item-has-children > a');
  $menu.click(function (event) {
    var sub = $(this).next();
    if ( event.which === 1 && sub.length && sub[0].nodeName.toLowerCase() === 'div' ) {
      if ( this.nodeName.toLowerCase() === 'a' || !sub.hasClass('show') ) {
        sub.toggleClass('show');
        sub.parent().toggleClass('is-active');
        sub.parent().siblings().toggleClass('is-inactive');
      return false;
      }
    }
  });

  // If Supermenu is enabled, repeat this, but change a few things
  if ( $( '.super__menu' ).length ) {
    $menu = $('.super__menu--item.kids > a');
    $menu.click( function ( event ) {
      var sub = $(this).next( '.hidden__dragon' );
      event.stopPropagation();
      sub.parent().toggleClass('submenu-visible submenu-tablet-visible');
      sub.parent().siblings().removeClass('submenu-visible');
      return false;
    } );
    // Remove the menu when clicking off
    $('body').click( function(){
      $('.super__menu--item.kids.submenu-visible.submenu-tablet-visible').hide();
    });
  }
});
