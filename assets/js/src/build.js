jQuery( function( $ ) {
  // When Images have loaded
  $( window ).on( 'load', function() {
    // Get all images
    var images = $('li.product img.wp-post-image');
    // Object fit all images
    images.each( objectFitImages );
    // Equalise all images
    // $images.equalise();
  });
});

// Do it once more for luck.
jQuery( function() {
  if ( ! Modernizr.objectfit) {
    objectFitImages()
  }
});;/* Debouncing. Performance gained. Swish. */
Function.prototype.debounce = function (threshold, execAsap) {
    var func = this, // reference to original function
        timeout; // handle to setTimeout async task (detection period)
    // return the new debounced function which executes the original function only once
    // until the detection period expires
    return function debounced () {
        var obj = this, // reference to original context object
            args = arguments; // arguments at execution time
        // this is the detection function. it will be executed if/when the threshold expires
        function delayed () {
            // if we're executing at the end of the detection period
            if (!execAsap)
                func.apply(obj, args); // execute now
            // clear timeout handle
            timeout = null;
        };
        // stop any current detection period
        if (timeout)
            clearTimeout(timeout);
        // otherwise, if we're not already waiting and we're executing at the beginning of the detection period
        else if (execAsap)
            func.apply(obj, args); // execute now
        // reset the detection period
        timeout = setTimeout(delayed, threshold || 100);
    };
};(function($, w){
  // A helper function to measure heights
  // If you're using borders or outlines
  var map_height = function (index, element) {
    return $(this).outerHeight();
  };

  // Equalise heights
  var equalise = function( element, group_size ) {
    // Reset element height
    element.height('');

    // Group results
    if (group_size === undefined) { group_size = 0; }

    if (group_size == 1) {
      return;
    }
    var groups = [];
    if (group_size) {
      // Not a clone, but a different object of with the same selection
      var clone = $(element);
      while (clone.length > 0) {
        groups.push($(clone.splice(0, group_size)));
      }
    }
    else {
      groups = [element];
    }

    // Measure the heights and then apply the highest measure to all
    var heights, height;
    for (var i in groups) {
      heights = groups[i].map(map_height).get();
      height = Math.max.apply(null, heights);
      groups[i].height(height);
    }
  };

  // Add jQuery plugin
  $.fn.equalise = function(size) {
    equalise(this, size);
    return this;
  };

  // Alterclass function to change classes rather than add/remove
  $.fn.alterClass = function ( removals, additions ) {

    var self = this;

    if ( removals.indexOf( '*' ) === -1 ) {
      // Use native jQuery methods if there is no wildcard matching
      self.removeClass( removals );
      return !additions ? self : self.addClass( additions );
    }

    var patt = new RegExp( '\\s' +
        removals.
          replace( /\*/g, '[A-Za-z0-9-_]+' ).
          split( ' ' ).
          join( '\\s|\\s' ) +
        '\\s', 'g' );

    self.each( function ( i, it ) {
      var cn = ' ' + it.className + ' ';
      while ( patt.test( cn ) ) {
        cn = cn.replace( patt, ' ' );
      }
      it.className = $.trim( cn );
    });

    return !additions ? self : self.addClass( additions );
  };

  // Determine if an url is external:
  var is_external = function () {
    var url = this;
    return (url.hostname && url.hostname.replace(/^www\./, '') !== location.hostname.replace(/^www\./, ''));
  };
  // Expose:
  w.is_external = is_external;


  // Min width detection
  var min_width;
  if ( Modernizr.mq('(min-width: 0px)') ) {
    // Browsers that support media queries
    min_width = function ( width ) {
      return Modernizr.mq( '(min-width: ' + width + 'em)' );
    };
  }
  else {
    // Fallback for browsers that does not support media queries
    min_width = function ( width ) {
      return jQuery(window).width() >= width * 16;
    };
  }
  // Expose:
  w.min_width = min_width;
} (jQuery, window) );
;/* When products vary in size, their images also
vary. which often causes issues with layout.
We're tackling this by equalising all images and
products when they load in.

The alternative is to use <table> layouts. Ouch. */
jQuery( function( $ ) {

  // Waiting until the end of your browser movement, you wierdo.
  var waitForFinalEvent = (function () {
    var timers = {};
    return function ( callback, ms, uniqueId ) {
      if ( ! uniqueId ) {
        uniqueId = "Don't call this twice without a uniqueId";
      }
      if ( timers[uniqueId] ) {
        clearTimeout (timers[uniqueId]);
      }
      timers[uniqueId] = setTimeout( callback, ms );
    };
  })();

  // Unique ID for window resizes
  function uniqId() {
    return Math.round(new Date().getTime() + (Math.random() * 100));
  }

  /* On Resize, load and init, we shall resize the elements */
  $( window ).on( 'load', resizeElements.debounce( 250, true ) );

  /* On resize... */
  $( window ).resize( function() {
    waitForFinalEvent( function(){
      // Get a resize event notification
      // console.log( 'resizing...' + uniqId() );
      resizeElements()
    }, 240, uniqId())
  });
});


// Equalise function
function resizeElements() {
  $ = jQuery;
  // Create a rows array
  var rows = [];
  // Cycle through all products and group them into rows
  $('html.nocssgrid ul.products li.product').each(function() {
    // Get the 'top'
    var top = $( this ).position().top;
    // Find items with the same top
    $( this ).prev().add( $( this ).next() )
      .filter( function(){ return $( this ).position().top == top } )
      .alterClass( 'product--row-*', 'product--row-' + Math.round($(this).position().top ) );
      // .addClass( 'product--row-' + Math.round($(this).position().top ) );
    // Add this group to an array
    rows.push( 'product--row-' + Math.round( $(this).position().top ) ) ;
  });

  // Filter out all repeating classes / tops
  var unique_rows = rows.filter( function( itm, i, a ) {
    return i == a.indexOf( itm );
  });

  // Check the classes being called
  // console.log( unique_rows );
  $.each( unique_rows, function( key, value ) {
    var $class = '.' + value;
    // Ensure classes are being targetted
    // console.log( $class );
    $( $class ).equalise();
  });

  // All lonely and last elemnents get equalised together
  $( 'html.nocssgrid ul.products li.product:not([class*="product--row"])' ).each( function() {
    $( this ).equalise();
  });

  // Send out a smoke signal for the child theme.
  $( 'html.nocssgrid body' ).trigger( 'craft_products_equalised' );

  // Equalise all widgets above tablet size
  if ( min_width( '47.9em' ) ) {
    $( '.footer__widget' ).equalise();
  }
}
;jQuery( function( $ ) {

 /* When we click on the cart in the header,
  we're going to slide a navigation item
  from the right side of the screen. */

  canvas_reveal = function ( container, link_element, body ) {
    var element_active = false;
    var toggle_element = function() {
      if ( element_active ) {
        container
          .addClass( 'closed' )
          .removeClass( 'open' );
        $( 'body' )
          .addClass( body + '__inactive' )
          .removeClass( body + '__active' );
        element_active = false;
      }
      else {
        container
          .addClass( 'open' )
          .removeClass( 'closed' );
        $( 'body' )
          .addClass( body + '__active' )
          .removeClass( body + '__inactive' );
        setTimeout( function() {
          element_active = true;
        }, 200 );
      }
    };
    // Open cart
    link_element.on( 'click touch', function( e ) {
      e.preventDefault();
      e.stopPropagation();
      toggle_element();
    } );

    // Close cart
    $( document ).on( 'click touch', function() {
      if ( true == element_active ) {
        toggle_element();
      }
    } );

    container.on( 'click touch', function( e ) {
      e.stopPropagation();
    } );
  }
} );
;/* function - container, link_element, body */
jQuery( function( $ ) {
  canvas_reveal(
    $( '.header--navigation' ), // Off-canvas element
    $( '.button-menu' ),
    'nav' // Trigger
  );

  // Cart
  canvas_reveal(
    $( '.header--cart--container' ), // Off-canvas element
    $( '.header--cart--link a' ),
    'cart' // Trigger
  );

  // Filters
  canvas_reveal(
    $( '.extended--filter--list' ), // Off-canvas element
    $( '.extended--filter--toggle' ),
    'filter' // Trigger
  );

  // Filters
  canvas_reveal(
    $( '.super__menu--item.kids > .hidden__dragon' ),
    $( '.super__menu--item.kids > a.super__menu--item--link' ), // Off-canvas element
    'dragon_awake' // Trigger
  );

  // Mobile Search
  canvas_reveal(
    $( '.searchform' ), // Off-canvas element
    $( '.search--trigger' ),
    'search' // Trigger
  );

  // When search is triggered, focus
  $( ".search--trigger" ).click( function() {
    $( "#s" ).focus();
  });

  // Mobile Search
  $( '.footer__widget' ).each( function(){
    canvas_reveal(
      $( this ), // Off-canvas element
      $( this ).find( '.footer__widget--title' ),
      'footer--widget' // Trigger
    );
  });
} );
;jQuery( function( $ ) {
  var parent = $( '.mega-menu--item-has-children' );
  parent.each( function( e ) {
    // Item with children
    var link  = $( this ).find( '> a' );
    var clone = link.clone();
    var text  = clone.text();
    clone.text( text );
    var item = $( '<li>' ).append( clone );
    item.addClass( 'all-link' );
    $( this ).find( '.sub-menu--column:first-child .sub-menu--list' ).prepend( item ) ;
  } );
});;// Dependent on Menu-Aim.js in libs this function allows users to be sloppy
// with their navigation. This means they're allowed to hover over other
// items unintentionally and not be punished for it.
jQuery( function( $ ){
  var menu = $( "#nav" );
  menu.menuAim( {
      submenuDirection: "below",
      tolerance: 1,
      // When the menu is entered
      enter: function( a ) {
        $( '.submenu-previously-visible' ).addClass( "submenu-visible" );
      },
      // When user moved to a parent item
      activate: function( a ) {
        $( a ).addClass( "submenu-visible" );
        $( '.submenu-previously-visible' ).removeClass( 'submenu-previously-visible' );
      },
      // When user moved to another parent item
      deactivate: function( a ) {
        $( a ).removeClass( "submenu-visible" );
        $( '.submenu-previously-visible' ).removeClass( 'submenu-previously-visible' );
      },
      // When user leaves the menu entirely
      exitMenu: function( a ) {
        $( ".submenu-visible" ).addClass( 'submenu-previously-visible' ).removeClass( "submenu-visible" );
      },
  } );
});;jQuery( function( $ ) {
  $menu = $('.mega-menu--item-has-children > a');
  $menu.click(function (event) {
    var sub = $(this).next();
    if ( event.which === 1 && sub.length && sub[0].nodeName.toLowerCase() === 'div' ) {
      if ( this.nodeName.toLowerCase() === 'a' || !sub.hasClass('show') ) {
        sub.toggleClass('show');
        sub.parent().toggleClass('is-active');
        sub.parent().siblings().toggleClass('is-inactive');
      return false;
      }
    }
  });

  // If Supermenu is enabled, repeat this, but change a few things
  if ( $( '.super__menu' ).length ) {
    $menu = $('.super__menu--item.kids > a');
    $menu.click( function ( event ) {
      var sub = $(this).next( '.hidden__dragon' );
      event.stopPropagation();
      sub.parent().toggleClass('submenu-visible submenu-tablet-visible');
      sub.parent().siblings().removeClass('submenu-visible');
      return false;
    } );
    // Remove the menu when clicking off
    $('body').click( function(){
      $('.super__menu--item.kids.submenu-visible.submenu-tablet-visible').hide();
    });
  }
});
