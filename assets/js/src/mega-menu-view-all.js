jQuery( function( $ ) {
  var parent = $( '.mega-menu--item-has-children' );
  parent.each( function( e ) {
    // Item with children
    var link  = $( this ).find( '> a' );
    var clone = link.clone();
    var text  = clone.text();
    clone.text( text );
    var item = $( '<li>' ).append( clone );
    item.addClass( 'all-link' );
    $( this ).find( '.sub-menu--column:first-child .sub-menu--list' ).prepend( item ) ;
  } );
});