<?php get_header(); ?>

<div id="content" class="cf w">

  <main id="main" class="single content--inx">

    <?php do_action( 'drivkraft_single_blog_before' ); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class( 'article' ); ?>>

        <div class="article--inner cf">

          <header class="article--header"><?php

            do_action( 'drivkraft_before_single_title' );

            ?><h2 class="article--title"><?php the_title(); ?></h2><?php

            do_action( 'drivkraft_after_single_title' );

          ?></header>

          <div class="article--content cf"><?php

          get_template_part( 'acf-fields/flex', 'layout' );

          ?></div>

        </div>

      </article>

    <?php endwhile;

    endif; ?>

    <?php do_action( 'drivkraft_single_blog_after' ); ?>

  </main>

  <?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>
